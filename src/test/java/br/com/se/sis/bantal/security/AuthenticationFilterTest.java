package br.com.se.sis.bantal.security;

import br.com.se.sis.bantal.controller.requests.LoginRequest;
import br.com.se.sis.bantal.modelo.WpPerfilUsuario;
import br.com.se.sis.bantal.repository.WpPerfilRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AuthenticationFilterTest {

    @Mock
    private AuthenticationFilter authenticationFilter;

    @Mock
    private LoginRequest loginRequest;

    @Mock
    private WpPerfilRepository profileRepository;


    @Test
    void shouldReturnUsernamePasswordAuthenticationTokenWhenEntityWasNull() {

        when(profileRepository.findByIdentificacao(eq("31"))).thenReturn(getProfileUser());

        UsernamePasswordAuthenticationToken auth = authenticationFilter.getOauthAuthentication(loginRequest, null);

        Assert.assertEquals(auth, auth);
    }

    private Optional<WpPerfilUsuario> getProfileUser() {
        return Optional.of(WpPerfilUsuario.builder()
                .withId("31")
                .withEmail("abc@gmail.com")
                .withFoto(null)
                .withDisplayName("ABC")
                .withIdentificacao("35.258.245/0001-06")
                .withPerfil("ADMIN")
                .withNome("BANTAL").build());
    }

}