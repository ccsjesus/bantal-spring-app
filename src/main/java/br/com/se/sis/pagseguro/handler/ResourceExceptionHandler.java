package br.com.se.sis.pagseguro.handler;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.se.sis.pagseguro.domain.vo.DetalheErro;
import br.com.se.sis.pagseguro.exception.TransacaoAbortadaException;
import br.com.se.sis.pagseguro.util.DetalheErroBuilder;

/**
 * Created by tawan on 11/08/18.
 */
@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(TransacaoAbortadaException.class)
    public ResponseEntity<DetalheErro> handlerTransacaoAbortadaException(
                    TransacaoAbortadaException exception, HttpServletRequest request) {

        DetalheErro erro = DetalheErroBuilder.getBuilder()
                .withTitulo("Erro ao realizar Transação")
                .withStatus(500L)
                .withData(new Date())
                .withMensagemDesenvolvedor(exception.getMessage() + " ##### " + request.getRequestURI())
                .withMensagemNegocio(exception.getMessage())
                .build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erro);
    }
}
