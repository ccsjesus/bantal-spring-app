package br.com.se.sis.pagseguro.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import br.com.uol.pagseguro.api.common.domain.enums.Currency;

public class PreAprovadoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4682399098505830831L;

	private String redirecionaURL;
	private String notificacaoURL;
	private Currency moeda;
	private BigDecimal precoExtra;
	private String referencia;
	private RemessaDTO remessa;
	private RemetenteDTO remetente;
	private RequisicaoPreAprovadoDTO requisicao;
	private Map<String, String> parametros = new HashMap<String, String>();
	
	public String getRedirecionaURL() {
		return redirecionaURL;
	}
	public void setRedirecionaURL(String redirecionaURL) {
		this.redirecionaURL = redirecionaURL;
	}
	public String getNotificacaoURL() {
		return notificacaoURL;
	}
	public void setNotificacaoRL(String notificacaoURL) {
		this.notificacaoURL = notificacaoURL;
	}
	public Currency getMoeda() {
		if(moeda == null) {
			return Currency.BRL;
		}
		return moeda;
	}
	public void setMoeda(Currency moeda) {
		this.moeda = moeda;
	}
	public BigDecimal getPrecoExtra() {
		return precoExtra;
	}
	public void setPrecoExtra(BigDecimal precoExtra) {
		this.precoExtra = precoExtra;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public RemessaDTO getRemessa() {
		return remessa;
	}
	public void setRemessa(RemessaDTO remessa) {
		this.remessa = remessa;
	}
	public RemetenteDTO getRemetente() {
		return remetente;
	}
	public void setRemetente(RemetenteDTO remetente) {
		this.remetente = remetente;
	}
	public RequisicaoPreAprovadoDTO getRequisicao() {
		return requisicao;
	}
	public void setRequisicao(RequisicaoPreAprovadoDTO requisicao) {
		this.requisicao = requisicao;
	}
	public Map<String, String> getParametros() {
		return parametros;
	}
	public void setParametros(Map<String, String> parametros) {
		this.parametros = parametros;
	}
	
	public String getReferenciaRandom2() {
	    int leftLimit = 97;
	    int rightLimit = 122;
	    int targetStringLength = 10;
	    Random random = new Random();
	 
	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();
	 
	    return generatedString;
	}
	
	public static String getReferenciaRandom() {
		UUID uuid = UUID.randomUUID();
	    return uuid.toString();    
	}
	
}
