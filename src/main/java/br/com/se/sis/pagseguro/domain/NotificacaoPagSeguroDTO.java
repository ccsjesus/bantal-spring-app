package br.com.se.sis.pagseguro.domain;

public class NotificacaoPagSeguroDTO {
	
	private String notificationType;
	
	private String notificationCode;

	public NotificacaoPagSeguroDTO(String notificationType, String notificationCode) {
		super();
		this.notificationType = notificationType;
		this.notificationCode = notificationCode;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getNotificationCode() {
		return notificationCode;
	}

	public void setNotificationCode(String notificationCode) {
		this.notificationCode = notificationCode;
	}

}
