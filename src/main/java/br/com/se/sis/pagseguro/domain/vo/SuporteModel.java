package br.com.se.sis.pagseguro.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuporteModel {

	public String nome;
	public String destinatario;
	public String telefone;
	public String assunto;
	public String mensagem;
}
