package br.com.se.sis.pagseguro.domain;

import java.util.Date;

import br.com.uol.pagseguro.api.PagSeguro;

public class RegistroPreAprovado {
	
	private String preApprovalCode;
	
	private Date date;
	
	private PagSeguro pagSeguro;
	
	private String codeReference;
	
	private String redirectURL;

	
	public String getPreApprovalCode() {
		return preApprovalCode;
	}

	public void setPreApprovalCode(String preApprovalCode) {
		this.preApprovalCode = preApprovalCode;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	public PagSeguro getPagSeguro() {
		return pagSeguro;
	}

	public void setPagSeguro(PagSeguro pagSeguro) {
		this.pagSeguro = pagSeguro;
	}

	public String getCodeReference() {
		return codeReference;
	}

	public void setCodeReference(String codeReference) {
		this.codeReference = codeReference;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

}
