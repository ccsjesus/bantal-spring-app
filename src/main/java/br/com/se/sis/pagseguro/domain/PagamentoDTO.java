package br.com.se.sis.pagseguro.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import br.com.uol.pagseguro.api.common.domain.enums.Currency;

public class PagamentoDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1396721669039004741L;

	private String id;
	
	private Currency moeda;
	
	private BigDecimal precoExtra;
	
	private String referencia;
	
	private RemetenteDTO remetente;
	
	private RemessaDTO remessa;
	
	private List<ProdutoDTO> produtos;
	
	private CartaoCreditoDTO cartaoCredito;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Currency getMoeda() {
		if(moeda == null) {
			return Currency.BRL;
		}
		return moeda;
	}

	public void setMoeda(Currency moeda) {
		this.moeda = moeda;
	}

	public BigDecimal getPrecoExtra() {
		return precoExtra;
	}

	public void setPrecoExtra(BigDecimal precoExtra) {
		this.precoExtra = precoExtra;
	}

	public String getReferencia() {
		if(referencia == null) {
			return getReferenciaRandom();
		}
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public RemetenteDTO getRemetente() {
		return remetente;
	}

	public void setRemetente(RemetenteDTO remetente) {
		this.remetente = remetente;
	}

	public RemessaDTO getRemessa() {
		return remessa;
	}

	public void setRemessa(RemessaDTO remessa) {
		this.remessa = remessa;
	}

	public List<ProdutoDTO> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<ProdutoDTO> produtos) {
		this.produtos = produtos;
	}

	public CartaoCreditoDTO getCartaoCredito() {
		return cartaoCredito;
	}

	public void setCartaoCredito(CartaoCreditoDTO cartaoCredito) {
		this.cartaoCredito = cartaoCredito;
	}
	
	public String getReferenciaRandom() {
	    int leftLimit = 97;
	    int rightLimit = 122;
	    int targetStringLength = 10;
	    Random random = new Random();
	 
	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();
	 
	    return generatedString;
	}
	
}
