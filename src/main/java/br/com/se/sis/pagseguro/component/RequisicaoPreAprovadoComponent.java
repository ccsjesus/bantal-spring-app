package br.com.se.sis.pagseguro.component;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import org.springframework.stereotype.Component;

import br.com.se.sis.pagseguro.domain.RequisicaoPreAprovadoDTO;
import br.com.uol.pagseguro.api.common.domain.builder.DateRangeBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PreApprovalRequestBuilder;
import br.com.uol.pagseguro.api.common.domain.enums.Charge;
import br.com.uol.pagseguro.api.common.domain.enums.Period;

@Component
public class RequisicaoPreAprovadoComponent {

	public PreApprovalRequestBuilder toPreApprovalRequestBuilder(RequisicaoPreAprovadoDTO requisicao) {
		return new PreApprovalRequestBuilder()
				.withCharge(Charge.AUTO)
				.withName(requisicao.getNome())
				.withDetails(requisicao.getDetalhes())
				.withAmountPerPayment(requisicao.getValorPagamentoMensal())
				.withPeriod(Period.MONTHLY);
	}
}
