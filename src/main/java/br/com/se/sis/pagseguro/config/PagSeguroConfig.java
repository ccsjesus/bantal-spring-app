package br.com.se.sis.pagseguro.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import br.com.uol.pagseguro.api.PagSeguro;
import br.com.uol.pagseguro.api.PagSeguroEnv;
import br.com.uol.pagseguro.api.credential.Credential;
import br.com.uol.pagseguro.api.http.JSEHttpClient;
import br.com.uol.pagseguro.api.utils.logging.SimpleLoggerFactory;

@Configuration
@EnableWebMvc
public class PagSeguroConfig {
	
	@Value(value = "${pagseguro.email}")
	private String emailPagSeguro;
	
	@Value(value = "${pagseguro.token}")
    private String tokenPagSeguro;
	
	@Bean
	public PagSeguro getPagSeguro() {
		return PagSeguro
                .instance(new SimpleLoggerFactory(), new JSEHttpClient(),
                    Credential.sellerCredential(emailPagSeguro, tokenPagSeguro), PagSeguroEnv.SANDBOX);
	}
}