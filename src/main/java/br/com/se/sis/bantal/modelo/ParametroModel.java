package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParametroModel {
	
	private String termId;
	
	private String name;
	
	private String slug;
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
		
	@Override
	public boolean equals (Object object) {
	    boolean result = false;
	    if (object == null || object.getClass() != getClass()) {
	        result = false;
	    } else {
	        ParametroModel employee = (ParametroModel) object;
	        if (this.termId.equals(employee.getName())) {
	            result = true;
	        }
	    }
	    return result;
	}
	

}
