package br.com.se.sis.bantal.dto;

import java.util.List;

import br.com.se.sis.bantal.modelo.EmpresasDTO;
import br.com.se.sis.bantal.modelo.WpPerfilUsuario;
import br.com.se.sis.bantal.services.EmpresaService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class WpPerfilDTO {

	@Autowired
	private EmpresaService empresaService;
	
	private String id;

	private String userId;

	private String nome;
	
	private String displayNome;
	
	private String email;

	private String perfil;

	private String identificacao;
	
	private byte[] foto;
	
	private List<WpPlanoPagamentoDTO> planoPagamento;

	private String password;
	
	public static WpPerfilDTO toDTO (WpPerfilUsuario wpPerfilUsuario) {
		return WpPerfilDTO.builder()
				.withIdentificacao(wpPerfilUsuario.getIdentificacao())
				.withDisplayNome(wpPerfilUsuario.getDisplayName())
				.withEmail(wpPerfilUsuario.getEmail())
				.withFoto(wpPerfilUsuario.getFoto())
				.withId(wpPerfilUsuario.getId())
				.withPerfil(wpPerfilUsuario.getPerfil())
				.withNome(wpPerfilUsuario.getNome())
				.withPassword(wpPerfilUsuario.getPassword())
//				.withPlanoPagamento(wpPerfilUsuario.getPlanoPagamento())
				.build();
	}
}
