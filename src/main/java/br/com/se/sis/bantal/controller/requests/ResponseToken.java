package br.com.se.sis.bantal.controller.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class ResponseToken {

    @JsonProperty(value = "accessToken")
    private String accessToken;

    @JsonProperty(value = "expiresIn")
    private Long expiresIn;

    @JsonProperty(value = "tokenType")
    private String tokenType;

    @JsonProperty(value = "refreshToken")
    private String refreshToken;

}
