package br.com.se.sis.bantal.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.se.sis.bantal.modelo.WpEmpresas;

@Repository

public interface EmpresaRepository extends JpaRepository<WpEmpresas, Long> {

    @Query(value="select * from wp_empresas where user_id = :idEmpresa", nativeQuery = true)
    WpEmpresas obterInformacoesEmpresaPorId(@Param("idEmpresa") Long idEmpresa);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update wp_empresas set objetivo= :#{#empresa.objetivo} where user_id = :#{#empresa.userId}", nativeQuery = true)
    void alterarSobreEmpresa(@Param("empresa") WpEmpresas empresa);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "update wp_empresas set sigla = :#{#empresa.sigla} where user_id = :#{#empresa.userId}", nativeQuery = true)
    void alterarSigla(@Param("empresa") WpEmpresas empresa);

}
