package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicacaoVaga {
	
	private String idVaga;

	private String postContent;

	private String titulo;

	private String localizacao;

	private String salario;

	private String CNPJ;
	
	private String idTipoVaga;
	
	private String idAreaAtuacao;

	private String dataExpiracao;
	
	private String nomeTipoVaga;
	
	private byte[] foto;
	
	private String status;
	
	private String idProfissao;


	public String getCNPJ() {
		if(CNPJ == null) {
			return " ";
		}
		return CNPJ;
	}

}
