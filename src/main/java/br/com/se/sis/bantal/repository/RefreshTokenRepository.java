package br.com.se.sis.bantal.repository;

import br.com.se.sis.bantal.modelo.RefreshToken;
import br.com.se.sis.bantal.modelo.WpUserAuthenticationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

    Optional<RefreshToken> findByToken(String token);

    @Modifying
    int deleteByUser(WpUserAuthenticationEntity user);
}
