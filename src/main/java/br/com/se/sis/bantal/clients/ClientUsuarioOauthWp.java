package br.com.se.sis.bantal.clients;

import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.se.sis.bantal.modelo.WpUser;

@Service
@Builder(setterPrefix = "with")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientUsuarioOauthWp {

	private String user_email;
	private String user_pass;
	
	@Value(value = "${url.bantal.site}")
	private String urlSiteBantal;

	public String getUser_email() {
		return user_email == null ? null : user_email.trim();
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getUser_pass() {
		return user_pass;
	}

	public void setUser_pass(String user_pass) {
		this.user_pass = user_pass;
	}

	public ClientUsuarioOauthWp obterUsuarioWp(WpUser pWpUser) {
		ClientUsuarioOauthWp outh = null;
		try {

			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
		                    .loadTrustMaterial(null, acceptingTrustStrategy)
		                    .build();

		    //SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
		    SSLConnectionSocketFactory scsf = new SSLConnectionSocketFactory(
		    		SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build(), 
		    		NoopHostnameVerifier.INSTANCE);

		    CloseableHttpClient httpClient = HttpClients.custom()
		                    .setSSLSocketFactory(scsf)
		                    .build();

		    HttpComponentsClientHttpRequestFactory requestFactory =
		                    new HttpComponentsClientHttpRequestFactory();

		    requestFactory.setHttpClient(httpClient);
		    RestTemplate restTemplate = new RestTemplate(requestFactory);
		
		outh = restTemplate.getForObject(urlSiteBantal.concat("/wp-json/api/usuario?usuario=")
				.concat(pWpUser.getUserEmail()).concat("&senha=").concat(pWpUser.getUserPass()),
				ClientUsuarioOauthWp.class);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND);
		}

		return outh;
	}
	
	public ClientUsuarioOauthWp atualizarSenhaWp(WpUser pWpUser) {
		ClientUsuarioOauthWp outh = null;
		try {

			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
		                    .loadTrustMaterial(null, acceptingTrustStrategy)
		                    .build();

		    SSLConnectionSocketFactory scsf = new SSLConnectionSocketFactory(
		    		SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build(), 
		    		NoopHostnameVerifier.INSTANCE);

		    CloseableHttpClient httpClient = HttpClients.custom()
		                    .setSSLSocketFactory(scsf)
		                    .build();

		    HttpComponentsClientHttpRequestFactory requestFactory =
		                    new HttpComponentsClientHttpRequestFactory();

		    requestFactory.setHttpClient(httpClient);
		    RestTemplate restTemplate = new RestTemplate(requestFactory);
		
		outh = restTemplate.getForObject(urlSiteBantal.concat("/wp-json/api/usuario/atualizar?id=")
				.concat(pWpUser.getId().toString()).concat("&senha=").concat(pWpUser.getUserPass()).concat("&newSenha=").concat(pWpUser.getNewSenha()),
				ClientUsuarioOauthWp.class);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return outh;
	}
}
