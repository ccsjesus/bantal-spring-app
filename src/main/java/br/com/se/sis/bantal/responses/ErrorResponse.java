package br.com.se.sis.bantal.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class ErrorResponse {
    int httpCode;
    String message;
    String internalCode;
    List<FieldErrorResponse> erros;
}
