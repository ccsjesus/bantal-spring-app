package br.com.se.sis.bantal.util;

public @interface TipoMediaImage {
	
	String PNG = "data:image/png;base64,";
	
	String JPG = "data:image/jpeg;base64,";
	
}

