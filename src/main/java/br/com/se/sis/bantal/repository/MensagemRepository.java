package br.com.se.sis.bantal.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import br.com.se.sis.bantal.modelo.WpMensagem;

public interface MensagemRepository extends JpaRepository<WpMensagem, String>, JpaSpecificationExecutor<WpMensagem> {
	
	@Query(value="SELECT * from wp_mensagem m where m.id_vaga = :idVaga", nativeQuery = true)
	List<WpMensagem> obterMensagensPorCodigoVaga(String idVaga);
	
	@Query(value="SELECT * from wp_mensagem m where m.id = :id", nativeQuery = true)
	WpMensagem obterMensagensPorId(String id);
	
	@Query(value="SELECT * from wp_mensagem m where m.id_vaga = :idVaga and m.id_autor = :idAutor", nativeQuery = true)
	List<WpMensagem> obterMensagensPorCodigoVagaCandidato(String idVaga, String idAutor);
	
	@Query(value="SELECT * from wp_mensagem m where m.id_vaga = :idVaga and m.id_referencia_mensagem = :idDestino", nativeQuery = true)
	List<WpMensagem> obterMensagensPorCodigoVagaDestino(String idVaga, String idDestino);
	
	@Query(value="SELECT * from wp_mensagem m where m.id_vaga = :idVaga and m.id_referencia_mensagem = :idDestino ORDER by m.id desc LIMIT 1", nativeQuery = true)
	WpMensagem obterMensagemRecente(String idVaga, String idDestino);
	
	@Query(value="SELECT * from wp_mensagem m where m.id_vaga = :idVaga and m.id_referencia_mensagem = :idDestino and m.id_autor = :idAutor and m.lida = 0", nativeQuery = true)
	List<WpMensagem> obterMsgNaoLidaUsuarios(String idVaga, BigInteger idDestino, BigInteger idAutor);
	
	@Query(value="SELECT * from wp_mensagem m where m.id_vaga = :idVaga and m.id_referencia_mensagem = :idDestino and m.id_autor = :idAutor", nativeQuery = true)
	Optional<List<WpMensagem>> obterMsgCandidatoEmpresa(String idVaga, String idDestino, String idAutor);

	@Query(value="SELECT * from wp_mensagem m where m.id_vaga = :idVaga and m.id_referencia_mensagem = :idCandidate and m.id_autor = :idEmployer", nativeQuery = true)
	Optional<List<WpMensagem>> getSendsMessagesToCandidateByEmployer(String idVaga, String idEmployer, String idCandidate);
	
	@Query(value="SELECT * from wp_mensagem m where m.id_vaga = :idVaga and m.id_referencia_mensagem = :idDestino and m.lida = :tipoMensagem", nativeQuery = true)
	List<WpMensagem>  obterMsgsPorIdTipoMensagem(String idVaga, String idDestino, int tipoMensagem);
	
}
