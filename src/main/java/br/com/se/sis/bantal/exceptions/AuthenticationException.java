package br.com.se.sis.bantal.exceptions;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import lombok.Data;

@Data
public class AuthenticationException extends RuntimeException {

    private ErrorsMessages code;

    public AuthenticationException(String message, ErrorsMessages code) {
        super(message);
        this.code = code;
    }
}
