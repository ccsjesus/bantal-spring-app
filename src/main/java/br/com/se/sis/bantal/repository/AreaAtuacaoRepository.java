package br.com.se.sis.bantal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpAreaAtuacao;

public interface AreaAtuacaoRepository extends JpaRepository<WpAreaAtuacao, String>, JpaSpecificationExecutor<WpAreaAtuacao> {
	
	@Query(value = "select * from wp_area_atuacao f where f.id_curriculo = :idCurriculo", nativeQuery = true)
	List<WpAreaAtuacao> obterAreaAtuacaoByIdLogado(@Param("idCurriculo") String idCurriculo);
	
	@Query(value = "select * from wp_area_atuacao f where f.id = :id", nativeQuery = true)
	WpAreaAtuacao obterAreaAtuacaoPorId(@Param("id") String id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM wp_area_atuacao WHERE wp_area_atuacao.id = :id",nativeQuery = true)
	void removerAreaAtuacaoById(@Param("id") String id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "INSERT INTO wp_area_atuacao (id_curriculo, id_area_atuacao)"
			+		" VALUES"
			+ 		" (:#{#atuacao.wpCurriculosAreaAtuacao.id}, :#{#atuacao.wpAreaAtuacaoParametro.id})",
			nativeQuery = true)
	void inserirAreaAtuacao(@Param("atuacao") WpAreaAtuacao atuacao);
	
}
