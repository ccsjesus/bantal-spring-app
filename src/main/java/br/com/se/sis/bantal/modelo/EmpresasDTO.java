package br.com.se.sis.bantal.modelo;

import lombok.Data;

@Data
public class EmpresasDTO {
	
	private String userId;
	private String nomeEmpresa;
	private String sigla;
	private byte[] foto;

	public EmpresasDTO(String userId, String nomeEmpresa, String sigla, byte[] foto) {
		this.userId = userId;
		this.nomeEmpresa = nomeEmpresa;
		this.sigla = sigla;
		this.foto = foto;
	}
	
}