package br.com.se.sis.bantal.util;

import java.util.Arrays;
import java.util.List;

public @interface MetaCandidato {

	String META_CADIDATO_CARGO = "_candidate_cargo";

	String META_CADIDATO_CELULAR = "_candidate_celular";

	String META_CADIDATO_CIVIL = "_candidate_civil";

	String META_CADIDATO_CNH = "_candidate_cnh";

	String META_CADIDATO_CPF = "_candidate_cpf";

	String META_CADIDATO_CURSOS = "_candidate_cursos";

	String META_CADIDATO_EDUCATION = "_candidate_education";

	String META_CADIDATO_EMAIL = "_candidate_email";

	String META_CADIDATO_EXPERIENCE = "_candidate_experience";

	String META_CADIDATO_FILHOS = "_candidate_filhos";

	String META_CADIDATO_FORMACAO = "_candidate_formacao";

	String META_CADIDATO_FUNCAO = "_candidate_funcao";

	String META_CADIDATO_HABILIDADES = "_candidate_habilidades";

	String META_CADIDATO_IDADE = "_candidate_idade";

	String META_CADIDATO_IDIOMAS = "_candidate_idiomas";

	String META_CADIDATO_LINKEDIN = "_candidate_linkedin";

	String META_CADIDATO_LOCATION = "_candidate_location";

	String META_CADIDATO_MAIL = "_candidate_mail";

	String META_CADIDATO_NACIONALIDADE = "_candidate_nacionalidade";

	String META_CADIDATO_NAME = "_candidate_name";

	String META_CADIDATO_NIVEL = "_candidate_nivel";

	String META_CADIDATO_OBJETIVOS = "_candidate_objetivos";

	String META_CADIDATO_PHONE = "_candidate_phone";

	String META_CADIDATO_PHOTO = "_candidate_photo";
	
	String META_PERFIL_PHOTO = "_perfil_photo_informacoes";

	String META_CADIDATO_RECADOS = "_candidate_recados";

	String META_CADIDATO_RG = "_candidate_rg";

	String META_CADIDATO_SALARIO = "_candidate_salario";

	String META_CADIDATO_SEM_EXP = "_candidato_sem_exp";

	String META_CADIDATO_APRESENTACAO = "_new_resume_content";

	String META_CADIDATO_USER_ID = "_candidate_user_id";
	
	String META_CADIDATO_INFORMATICA = "_candidate_informatica";
	
	List<String> listaMetaCandidatoObrigatorio = Arrays.asList(
			META_CADIDATO_CPF,
			
			META_CADIDATO_NAME
			);

	List<String> listaMetaCandidatoOpcional = Arrays.asList(
			META_CADIDATO_CPF,
			
			META_CADIDATO_NAME,
			
			META_CADIDATO_CARGO,

			META_CADIDATO_CELULAR,

			META_CADIDATO_CIVIL,

			META_CADIDATO_CNH,

			META_CADIDATO_CURSOS,

			META_CADIDATO_EDUCATION,

			META_CADIDATO_EMAIL,

			META_CADIDATO_EXPERIENCE,

			META_CADIDATO_FILHOS,

			META_CADIDATO_FORMACAO,

			META_CADIDATO_FUNCAO,

			META_CADIDATO_HABILIDADES,

			META_CADIDATO_IDADE,

			META_CADIDATO_IDIOMAS,

			META_CADIDATO_LINKEDIN,

			META_CADIDATO_LOCATION,

			META_CADIDATO_MAIL,

			META_CADIDATO_NACIONALIDADE,

			META_CADIDATO_NIVEL,

			META_CADIDATO_OBJETIVOS,

			META_CADIDATO_PHONE,

			META_CADIDATO_PHOTO,

			META_CADIDATO_RECADOS,

			META_CADIDATO_RG,

			META_CADIDATO_SALARIO,

			META_CADIDATO_SEM_EXP,

			META_CADIDATO_APRESENTACAO,

			META_CADIDATO_USER_ID,
			
			META_CADIDATO_INFORMATICA,
			
			META_PERFIL_PHOTO);

}
