package br.com.se.sis.bantal.services;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.exceptions.NotFoundException;
import br.com.se.sis.bantal.modelo.Usuario;
import br.com.se.sis.bantal.modelo.WpEmpresas;
import br.com.se.sis.bantal.repository.EmpresaRepository;

@Service
public class EmpresaService {

     @Autowired
    private EmpresaRepository empresaRepository;


    public WpEmpresas obterInformacoesEmpresaPorId(Long idEmpresa) {
        WpEmpresas empresa = empresaRepository
                .obterInformacoesEmpresaPorId(idEmpresa);
        
        return empresa;
    }

    public void atualizarempresaPorUsuario(String idUsuario, Usuario usuario) {

        WpEmpresas empresa = empresaRepository.obterInformacoesEmpresaPorId(Long.valueOf(idUsuario));

        if (Objects.isNull(empresa)) {
            empresa = new WpEmpresas();
            
            empresa.setUserId(idUsuario);
            empresa.setNome(usuario.getFirst_name());
        }

        if (!Objects.isNull(usuario.getSobreEmpresa()))
            empresa.setObjetivo(usuario.getSobreEmpresa().trim());

        if (!Objects.isNull(usuario.getSigla()))
            empresa.setSigla(usuario.getSigla());

        empresaRepository.save(empresa);
    }


    public void atualizarSobreEmpresa(Long idEmpresa, String objetivo) {
        if (Objects.isNull(objetivo))
            return;
        WpEmpresas empresa = empresaRepository.obterInformacoesEmpresaPorId(idEmpresa);
        if (Objects.isNull(empresa))
            new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND);
        empresa.setObjetivo(objetivo);
        empresaRepository.save(empresa);
    }

    public void atualizarSigla(Long idEmpresa, String sigla) {
        if (Objects.isNull(sigla))
            return;
        WpEmpresas empresa = empresaRepository.obterInformacoesEmpresaPorId(idEmpresa);
        if (Objects.isNull(empresa))
            new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND);
        empresa.setSigla(sigla);
        empresaRepository.save(empresa);
    }
}



