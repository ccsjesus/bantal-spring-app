package br.com.se.sis.bantal.modelo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WpCandidatoModel {

	private int codigoVaga;
	
	private String cpf;
	
	private String candidato;
	
	private String idUsuario;
	
	public WpCandidatoModel(String idUsuario) {
		super();
		this.idUsuario = idUsuario;
	}
	

	public static List<WpCandidatoModel> retornarWpCandidatoModel(List<Object[]> pArray){
		List<WpCandidatoModel> lista = new ArrayList<>();
		WpCandidatoModel wpResult = null;
		for (Object[] obj : pArray) {
			wpResult = new WpCandidatoModel();
			wpResult.setCodigoVaga(new BigDecimal(obj[0].toString()).toBigInteger().intValue());
			wpResult.setCpf(obj[1].toString());
			wpResult.setCandidato(obj[2].toString());
			wpResult.setIdUsuario(obj[3].toString());
			lista.add(wpResult);
		}
		
		return lista;
	}
	
	public static WpCandidatoModel convetToWpCandidatoModel(Object[] pArray){
		WpCandidatoModel wpResult = null;
		wpResult = new WpCandidatoModel();
		wpResult.setCodigoVaga(new BigDecimal(pArray[0].toString()).toBigInteger().intValue());
		wpResult.setCpf(pArray[1].toString());
		wpResult.setCandidato(pArray[2].toString());
		wpResult.setIdUsuario(pArray[3].toString());
		return wpResult;
	}

	
}
