package br.com.se.sis.bantal.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import br.com.se.sis.bantal.annotations.UserAdminCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.annotations.UserEmployerAndAdminCanOnlyAccessResources;
import br.com.se.sis.bantal.annotations.UserEmployerCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.security.UserCustomDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.modelo.OpcoesVagas;
import br.com.se.sis.bantal.modelo.PublicacaoVaga;
import br.com.se.sis.bantal.modelo.WpInformacoesGerais;
import br.com.se.sis.bantal.modelo.WpPost;
import br.com.se.sis.bantal.modelo.WpTerm;
import br.com.se.sis.bantal.modelo.WpTokenSistema;
import br.com.se.sis.bantal.repository.TaxonomiaRepository;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.repository.VagasRepository;
import br.com.se.sis.bantal.services.InformacoesGeraisService;
import br.com.se.sis.bantal.services.VagasService;
import br.com.se.sis.bantal.util.TipoVaga;
import br.com.se.sis.pagseguro.exception.TransacaoAbortadaException;

@RestController
@RequestMapping("/vagas")
public class VagasController {

	@Autowired
	private VagasRepository vagasRepository;

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private TaxonomiaRepository taxonomiaRepository;

	@Autowired
	private VagasService vagasGerenciador;
	
	@Autowired
	private VagasService vagaService;
	
	@Autowired
	private InformacoesGeraisService informacoesGeraisService;

	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/v2/{cdVaga}")
	@UserEmployerCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getVagasV2(@PathVariable(value = "cdVaga") String cdVaga)
			throws IllegalArgumentException{

		return ResponseEntity.ok(vagasGerenciador.obterVagaPorId(cdVaga));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/empresas/{idempresa}")
	@UserEmployerCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getVagasIdEmpresa(Authentication authentication) throws IllegalArgumentException{
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		List<WpPost> vagas = vagasRepository
				.consultarVagasPorIdEmpresa(new BigInteger(customDetails.getId()));

		return ResponseEntity.ok(vagas);
	}

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/v2/")
	@UserEmployerCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getAllVagasV2(Authentication authentication) throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(informacoesGeraisService.obterInformacoesGerais(customDetails.getId()));
	}

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/opcoes-vagas")
	@UserEmployerCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> obterOpcoesVagas() throws IllegalArgumentException {

		// Obter o id da empresa para cadastrar o anuncio da vaga

		List<WpTerm> listTerm = taxonomiaRepository.obterOpcoesVagaPorTipo(TipoVaga.TIPO_JOB.getValor());
		List<OpcoesVagas> listaVagas = new ArrayList<OpcoesVagas>();

		OpcoesVagas op;
		for (WpTerm term : listTerm) {
			op = new OpcoesVagas();
			op.setId(term.getTermId());
			op.setNome(term.getName());
			listaVagas.add(op);
		}

		// Criando o PostMeta

		return ResponseEntity.ok(listaVagas);
	}

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/informacoes")
	@UserEmployerCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getInformacoesGerais(Authentication authentication) throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		List<Object[]> informacoesGerais = vagasRepository
				.consultarInformacoesGeraisVagas(new BigInteger(customDetails.getId()));

		WpInformacoesGerais info = WpInformacoesGerais.retornarWpInformacoesGerais(informacoesGerais);

		return ResponseEntity.ok(info);
	}
	
	@Transactional(value = TxType.REQUIRES_NEW)
	@RequestMapping(method = RequestMethod.POST, value = "/v2/publicar", consumes = { MediaType.APPLICATION_JSON_VALUE,MediaType.MULTIPART_FORM_DATA_VALUE })
	@UserEmployerCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> publicarV2(Authentication authentication, @RequestBody PublicacaoVaga postsParametro) throws TransacaoAbortadaException, IOException{
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		vagaService.publicarNovaVaga(postsParametro, customDetails.getId());

		return ResponseEntity.status(HttpStatus.CREATED).build();

	}
	
	@Transactional(value = TxType.REQUIRES_NEW)
	@RequestMapping(method = RequestMethod.POST, value = "/v2/atualizar")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> atualizarVaga(Authentication authentication, @RequestBody PublicacaoVaga postsParametro) throws IOException{
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		vagaService.atualizarVaga(postsParametro, customDetails.getId());

		return ResponseEntity.status(HttpStatus.CREATED).build();

	}
	
	@Transactional(value = TxType.REQUIRES_NEW)
	@RequestMapping(method = RequestMethod.GET, value = "/v1/aprovar/{idVaga}")
	@UserAdminCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> aprovarVaga(Authentication authentication, @PathVariable(value = "idVaga") String idVaga) throws IOException{
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		vagaService.aprovarVaga(idVaga, customDetails.getId());

		return ResponseEntity.status(HttpStatus.CREATED).build();

	}
	
	@Transactional(value = TxType.REQUIRES_NEW)
	@RequestMapping(method = RequestMethod.DELETE, value = "/v1/{idVaga}")
	@UserAdminCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> remvoverVaga(Authentication authentication, @PathVariable(value = "idVaga") String idVaga) throws IOException{

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		vagaService.removerVaga(idVaga, customDetails.getId());

		return ResponseEntity.status(HttpStatus.CREATED).build();

	}

}
