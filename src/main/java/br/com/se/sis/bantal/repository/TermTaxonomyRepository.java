package br.com.se.sis.bantal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpTermTaxonomy;

public interface TermTaxonomyRepository extends JpaRepository<WpTermTaxonomy, String>, JpaSpecificationExecutor<WpTermTaxonomy> {
	
	
	@Query(value="SELECT * FROM wp_term_taxonomy where wp_term_taxonomy.term_taxonomy_id = :idTipoVaga and wp_term_taxonomy.taxonomy = 'job_listing_type'",
			nativeQuery = true)
	WpTermTaxonomy obterTaxonomia(@Param("idTipoVaga") String idTipoVaga);

}
