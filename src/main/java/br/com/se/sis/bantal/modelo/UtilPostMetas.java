package br.com.se.sis.bantal.modelo;

import java.util.List;

public class UtilPostMetas {

	public static PublicacaoVaga converterWPUserMetas(List<WpUser> lista) {
		PublicacaoVaga p = new PublicacaoVaga();
		for (WpUser meta : lista) {
			for (WpUsermeta userMeta : meta.getWpUsermetas()) {
				if (userMeta.getMetaKey().equals("billing_cnpj")) {
					p.setCNPJ(userMeta.getMetaValue());
					break;
				}
			}
		}
		return p;
	}

	public static Posts addWpPostFromMetas(WpPost meta, Posts p) {
		for (WpPostmeta wpPostmeta : meta.getWpPostmetas()) {
			if (wpPostmeta.getMetaKey().equals("_job_location")) {
				p.setLocalizacao(wpPostmeta.getMetaValue());
			}
			if (wpPostmeta.getMetaKey().equals("_job_salary")) {
				p.setSalario(wpPostmeta.getMetaValue().replace("Até R$ ", ""));
			}
		}
		return p;
	}

}
