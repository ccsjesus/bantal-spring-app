package br.com.se.sis.bantal.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

@AllArgsConstructor
@NoArgsConstructor
public class FieldErrorResponse {
    String message;
    String field;
}
