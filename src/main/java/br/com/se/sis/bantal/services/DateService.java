package br.com.se.sis.bantal.services;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

@Service
public class DateService {
	
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


	
	public LocalDateTime now() {
		ZonedDateTime dateZone = ZonedDateTime.now(ZoneId.of("America/Bahia"));
		return dateZone.toLocalDateTime();
	}
	
	public String formatLocalDateTimeToString(LocalDateTime dateTime) {
		return  dateTime.format(formatter);

	}
}
