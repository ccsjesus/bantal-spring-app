package br.com.se.sis.bantal.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.dto.WpPerfilDTO;
import br.com.se.sis.bantal.modelo.InscricaoVagaDTO;
import br.com.se.sis.bantal.modelo.WpInscricaoVaga;
import br.com.se.sis.bantal.modelo.WpVagas;
import br.com.se.sis.bantal.repository.WpInscricaoVagasRepository;
import br.com.se.sis.pagseguro.exception.TransacaoAbortadaException;

@Service
@Configurable
public class InscricaoVagaService {

	@Autowired
	private WpInscricaoVagasRepository inscricaoRepository;
	
	@Autowired
	private DateService dataService;
	
	@Autowired
	private PerfilServices perfilServices;
	
	@Autowired
	private VagasService vagasService;

	public List<WpInscricaoVaga> obterInscricoesVagaPorIdUsuario(String idLogado) {
		
		return inscricaoRepository.obterInscricaoVagasPorIdUsuario(idLogado);
	}
	
	public List<WpInscricaoVaga> obterCandidatosIncritos(String idLogado) {
		
		return inscricaoRepository.obterInscricaoVagasPorIdUsuario(idLogado);
	}
	
	public List<InscricaoVagaDTO> obterTodasInscricoes(){
		
		return inscricaoRepository.findAll().stream().map(inscricao -> this.convertToInscricaoDTO(inscricao)).collect(Collectors.toList());
		
	}
	
	public InscricaoVagaDTO convertToInscricaoDTO(WpInscricaoVaga wpInscricaoVaga) {
		InscricaoVagaDTO inscricao = new InscricaoVagaDTO();
		inscricao.setId(wpInscricaoVaga.getId());
		inscricao.setCdUsuario(new BigDecimal(wpInscricaoVaga.getUserId()));
		inscricao.setCodigoVaga(wpInscricaoVaga.getWpVagas().getId());
		inscricao.setComentarioInscricao(wpInscricaoVaga.getComentario());
		
		WpPerfilDTO perfil = perfilServices.obterPerfilUsuario(wpInscricaoVaga.getUserId());
		
		inscricao.setNomeCandidato(perfil.getNome());
		
		return inscricao;
		
	}
	
	public void realizarInscricaoVaga(InscricaoVagaDTO inscricaoVagaParam) {
		WpInscricaoVaga inscricaoVaga = new WpInscricaoVaga();
		inscricaoVaga.setUserId(inscricaoVagaParam.getCdUsuario().toString());
		inscricaoVaga.setDataInscricao(dataService.now());
		
		WpVagas vagas = vagasService.obterWpVagasPorid(inscricaoVagaParam.getCodigoVaga());
		inscricaoVaga.setWpVagas(vagas);
		inscricaoVaga.setComentario(inscricaoVagaParam.getComentarioInscricao());
		inscricaoRepository.save(inscricaoVaga);
	}
	
	public void removerInscricaoVaga(String cdVaga, String idUsuarioLogado) {
		
		WpInscricaoVaga inscricao = inscricaoRepository.obterInscricaoVagaPorCdVagaIdUsuario(cdVaga, idUsuarioLogado);
		
		if (inscricao != null)
			inscricaoRepository.deleteById(inscricao.getId());
		else 
			throw new TransacaoAbortadaException("O Id da Inscrição da vaga informado não existe.");
	}

}
