package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.UserCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.CurriculoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("/v1/experiencia-profissional")
public class ExperienciaProfissionalController {

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private CurriculoServices curriculoServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/sem-experiencia")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> adicionarExperienciaCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		curriculoServices.adicionarSemExperiencia(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);

	}
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-sem-experiencia")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> updateExperienciaCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws IllegalArgumentException, UnsupportedEncodingException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		curriculoServices.atualizarSemExperiencia(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);

	}
}
