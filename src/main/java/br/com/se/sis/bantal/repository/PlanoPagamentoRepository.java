package br.com.se.sis.bantal.repository;

import java.math.BigInteger;
import java.util.Optional;

import br.com.se.sis.bantal.modelo.WpPerfilUsuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpPlanoPagamento;
import br.com.se.sis.bantal.modelo.WpPlanos;

public interface PlanoPagamentoRepository extends JpaRepository<WpPlanoPagamento, String>, JpaSpecificationExecutor<WpPlanoPagamento> {

	@Query(value = "select * from wp_plano_pagamento where wp_plano_pagamento.cd_solicitacao_transacao = :cdSolicitacao",
			nativeQuery = true)
	WpPlanoPagamento obterPlanoPagamentoPorCodigoSolicitacao(@Param("cdSolicitacao") String cdSolicitacao);
	
	@Query(value = "select * from wp_plano_pagamento where wp_plano_pagamento.cd_referencia_transacao = :cdReferencia",
			nativeQuery = true)
	WpPlanoPagamento obterPlanoPagamentoPorCodigoReferencia(@Param("cdReferencia") String cdReferencia);
	
	@Query(value = "select * from wp_plano_pagamento where wp_plano_pagamento.cd_usuario = :idUsuario and wp_plano_pagamento.cd_transacao is not null ORDER by wp_plano_pagamento.data_confrmacao_pagamento desc limit 1",
			nativeQuery = true)
	WpPlanoPagamento obterPlanoAtivoPorIdUsuario(@Param("idUsuario") String idUsuario);
	
	@Query(value = "select * from wp_plano_pagamento where wp_plano_pagamento.cd_solicitacao_transacao = :cdPlanoSolicitacao and wp_plano_pagamento.cd_usuario = :cdUsuario", nativeQuery = true)
	WpPlanoPagamento obterPlanoPagamentoPorCodigoSolicitacao(@Param("cdPlanoSolicitacao") String cdPlanoSolicitacao, BigInteger cdUsuario);

	Optional<WpPlanoPagamento> findByWpPerfilUsuario(WpPerfilUsuario idProfile);
}
