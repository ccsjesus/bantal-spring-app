package br.com.se.sis.bantal.modelo;

import br.com.se.sis.bantal.controller.requests.RegisterUserRequest;
import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.enums.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
@Entity(name = "wp_users_authentication")
public class WpUserAuthenticationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private String cellphone;

    @Column
    private String password;

    @Column
    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @Column
    private String identifier;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER )
    @CollectionTable(name="wp_users_roles", joinColumns = @JoinColumn(name="user_id") )
    private Set<Role> roles = new HashSet<>();


    public static WpUserAuthenticationEntity toAuthenticationEntity(RegisterUserRequest postUserRequest){
        return WpUserAuthenticationEntity.builder()
                .withName(postUserRequest.getName())
                .withIdentifier(postUserRequest.getIdentifier())
                .withEmail(postUserRequest.getEmail())
                .withStatus(UserStatus.ATIVO)
                .withCellphone(postUserRequest.getCellphone())
                .withRoles(Set.of(Role.get(postUserRequest.getProfileSystem())))
                .withPassword(postUserRequest.getPassword()).build();
    }
}
