package br.com.se.sis.bantal.modelo;

import java.util.List;

import lombok.Data;

@Data
public class WpInformacoesGerais {

	private String totalInscritos;
	
	private String qtdVagasAbertas;
	
	//private List<WpResultadoRecrutamentoModel> vagas;
	
	private List<VagasModel> vagas;
	
	public static WpInformacoesGerais retornarWpInformacoesGerais(List<Object[]> pDados){
		WpInformacoesGerais wpResult = null;
		for (Object[] obj : pDados) {
			wpResult = new WpInformacoesGerais();
			wpResult.setTotalInscritos(obj[0] == null ? "0" :  obj[0].toString());
			wpResult.setQtdVagasAbertas(obj[1] == null ? "0" :  obj[1].toString());
		}
		
		return wpResult;
	}
	
}
