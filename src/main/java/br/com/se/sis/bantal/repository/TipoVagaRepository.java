package br.com.se.sis.bantal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpTipoVaga;

public interface TipoVagaRepository extends JpaRepository<WpTipoVaga, String>, JpaSpecificationExecutor<WpTipoVaga> {

	@Query(value = "select * from wp_tipo_vaga par where par.id = :id", nativeQuery = true)
	WpTipoVaga obterTipoVagaPorId(@Param("id") String id);
	
	@Query(value = "select * from wp_tipo_vaga t where t.habilitada = 0", nativeQuery = true)
	List<WpTipoVaga> obterTodosTiposVagaHabilitadas();
	
	@Query(value = "select * from wp_tipo_vaga t where t.habilitada = 1", nativeQuery = true)
	List<WpTipoVaga> obterTodosTiposVagaDesabilitadas();
	
	@Query(value = "delete from wp_tipo_vaga vaga where vaga.id = :id ", nativeQuery = true)
	List<WpTipoVaga> removerTipoVaga(@Param("id") String id);
	
}
