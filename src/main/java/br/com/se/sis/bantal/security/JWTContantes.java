package br.com.se.sis.bantal.security;

public class JWTContantes {
	
	public static final long EXPIRATION_TIME = 864_000_000; // 10 days
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/auth/login";


}
