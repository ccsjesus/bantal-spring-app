package br.com.se.sis.bantal.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.exceptions.NotFoundException;
import br.com.se.sis.bantal.modelo.*;
import br.com.se.sis.bantal.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.dto.WpPerfilDTO;
import br.com.se.sis.bantal.repository.WpUserMetaRepository;
import br.com.se.sis.bantal.repository.WpUserRepository;
import br.com.se.sis.bantal.util.FuncaoSistema;
import br.com.se.sis.bantal.util.Util;

@Service
@Configurable
public class UsuarioService {
	
	@Autowired
	private WpUserMetaRepository wpUserMetaRepository;
	
	@Autowired
	private WpUserRepository wpUserRepository;

	@Autowired
	private CurriculoServices curriculoServices;
	
	@Autowired
	private PerfilServices perfilServices;
	
	@Autowired
	private PlanoPagamentoService planoPagamentoService;

	@Autowired
	private EmpresaService empresaService;

	public WpUser findUserByEmail(String email) {
		return wpUserRepository.findByUserEmail(email).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND));
	}
	
	
	public List<WpPostmeta> obterDadosPerfilUsuario(WpTokenSistema tokenParam) {

		return null;
	}
	
	public void atualizarNomeUsuario(String idUsuarioLogado, String nome) {
		perfilServices.atualizarNomeUsuario(idUsuarioLogado, nome);
	}
	
	public void atualizarDisplayNomeUsuario(String idUsuarioLogado, String nome) {
		perfilServices.atualizarDisplayNomeUsuario(idUsuarioLogado, nome);

	}
	
	public void atualizarEmail(String idUsuarioLogado, String email) {
		perfilServices.atualizarEmail(idUsuarioLogado, email);

	}
	
	public void atualizarSenha(String idUsuarioLogado, Usuario usuario) {
		perfilServices.atualizarSenha(idUsuarioLogado, usuario);

	}

	public WpCurriculos updateUserIdCurriculoById(String idCurriculo, String userId, Role role){
		switch (role){
			case CANDIDATE:
				curriculoServices.updateUserIdCurriculoById(idCurriculo, userId);
				break;
			case ADMIN:
			case EMPLOYER:
				return null;
		}

		return null;
	}

	public WpCurriculos updateUserIdCurriculoByProfileUser(WpPerfilUsuario profileUser, String userId){
		WpUser user = wpUserRepository.findByUserEmail(profileUser.getEmail()).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND));
		WpCurriculos wpCurriculos = curriculoServices.findByUserId(user.getId().toString());
		if(wpCurriculos != null){
			return curriculoServices.updateUserIdCurriculoById(wpCurriculos.getId(), userId);
		} else {
			// O curriculo ja foi atualizado
			return null;
		}
	}

	public WpPlanoPagamento updateCdUserPlanPaymentByProfileUser(WpPerfilUsuario profileUser, String userId){
		return planoPagamentoService.updateCdUserPlanPaymentByProfileUser(profileUser, userId);
	}

	public WpPlanoPagamento updateCdUserPlanPaymentByProfileUserByUser(Usuario user, String userId){
		return planoPagamentoService.updateCdUserPlanPaymentByProfileUser(user, userId);
	}

	
	public Usuario cadastrarInformacoesIniciais(String idUsuarioLogado, WpUser wpUser) {
		UserMeta userMeta = wpUserMetaRepository.obterDadosUsuario(new BigDecimal(idUsuarioLogado));

		Role funcaoSistema = Util.getFuncaoSistema(userMeta);
		WpPerfilDTO perfil = null;
		WpCurriculos wpCurriculos = null;


		switch (funcaoSistema) {
			case CANDIDATE:
			wpCurriculos = curriculoServices.inserirDadosIniciaisCurriculo(wpUser);
			perfil = perfilServices.cadastrarInformacoesIniciais(wpUser, Role.CANDIDATE);
			break;
			case EMPLOYER:
			case ADMIN:
			perfil = perfilServices.cadastrarInformacoesIniciais(wpUser, funcaoSistema);
			break;
			
		default:
			break;
			
		}
		Usuario user = retornarUsuarioFromWpUser(perfil);
		user.setRoleSystem(funcaoSistema);
		user.setIdentificacao(perfil.getIdentificacao());

		if(wpCurriculos != null) {
			user.setIdCurriculo(wpCurriculos.getId());
		}

		return user;
		
	}
	
	public Usuario obterDadosUsuario(String idUsuarioLogado) {
		WpPerfilDTO perfil = perfilServices.obterPerfilUsuario(idUsuarioLogado);

		return retornarUsuarioFromWpUser(perfil);

	}

	public Usuario retornarUsuarioFromWpUser(WpPerfilDTO perfil) {

		BigDecimal id = null;
		String displayName = "";
		String email = "";
		String login = "";
		String niceName = "";
		String firstName = "";
		String identificacao = "";
		String funcaoSistema = "";
		byte [] photo = null;
		String sobreEmpresa = null;
		String sigla = null;

		displayName = perfil.getDisplayNome();
		firstName = perfil.getNome();
		identificacao = perfil.getIdentificacao();
		funcaoSistema = perfil.getPerfil();
		id = new BigDecimal(perfil.getUserId());
		email = perfil.getEmail();
		photo = perfil.getFoto();
		boolean isCurriculoCandidatoValido;

		
		try {
		isCurriculoCandidatoValido = verificarIsCurriculoCandidatoValido(perfil);
		} catch (Exception e) {
			isCurriculoCandidatoValido = false;
		}


		if (!perfil.getPerfil().equalsIgnoreCase(FuncaoSistema.FUNCAO_CANDIDATO.getValor().toUpperCase())) {
			WpEmpresas empresa = recuperarEmpresa(Long.valueOf(perfil.getUserId()));
			if (!Objects.isNull(empresa)) {
				sigla = empresa.getSigla();
				sobreEmpresa = empresa.getObjetivo();
			}
		}

		return Usuario.builder().withDisplayName(displayName)
						.withFirst_name(displayName)
						.withUserEmail(email)
				        .withUserLogin(login)
				        .withUserNicename(niceName)
				        .withId(id)
				        .withFoto(photo)
				        .withFirst_name(firstName)
				        .withIdentificacao(identificacao)
				        .withIsCurriculoCandidatoValido(isCurriculoCandidatoValido)
					  	.withSobreEmpresa(sobreEmpresa)
				      	.withSigla(sigla)
				.withRoleSystem(Role.get(funcaoSistema)).build();
	}

	public WpEmpresas recuperarEmpresa(Long idEmpresa) {
		WpEmpresas empresa = empresaService.obterInformacoesEmpresaPorId(idEmpresa);
		return empresa;
	}

	private boolean verificarIsCurriculoCandidatoValido(WpPerfilDTO perfil) {
		
		if (!perfil.getPerfil().equalsIgnoreCase(FuncaoSistema.FUNCAO_CANDIDATO.getValor().toUpperCase()))
			return false;
		
		boolean isCurriculoValido = false;
		CurriculoModel curriculo = curriculoServices.obterCurriculoModelUsuario(perfil.getUserId());
		if (!Objects.isNull(curriculo)) {
			if ((!Objects.isNull(curriculo.getFormacaoAcademica()) && curriculo.getFormacaoAcademica().size() > 0) 
					&& (!Objects.isNull(curriculo.getInformatica()) && curriculo.getInformatica().size() > 0) 
					&& (!Objects.isNull(curriculo.getExperienciaProfissional()) && curriculo.getExperienciaProfissional().size() > 0)
					&& (!Objects.isNull(curriculo.getAreaAtuacao()) && curriculo.getAreaAtuacao().size() > 0) 
					&& (!Objects.isNull(curriculo.getIdiomas()) && curriculo.getIdiomas().size() > 0)){
				isCurriculoValido = true;
			}
		}
		
		return isCurriculoValido;
	}
	
}
