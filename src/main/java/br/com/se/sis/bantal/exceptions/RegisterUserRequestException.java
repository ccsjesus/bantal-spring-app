package br.com.se.sis.bantal.exceptions;

public class RegisterUserRequestException  extends RuntimeException {
    public RegisterUserRequestException(String s) {
        super(s);
    }
}
