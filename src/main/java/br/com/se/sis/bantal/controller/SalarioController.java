package br.com.se.sis.bantal.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.transaction.Transactional;

import br.com.se.sis.bantal.annotations.UserEmployerAndCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.annotations.UserEmployerCanOnlyAccessTheirOwnResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.modelo.ParametroModel;
import br.com.se.sis.bantal.modelo.WpTokenSistema;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.SalarioServices;

@RestController
@RequestMapping("/v1/salario")
public class SalarioController {


	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private SalarioServices salarioServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserEmployerAndCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getAllSalary() throws IllegalArgumentException{
		
		List<ParametroModel> listaSalario = salarioServices.obterTodosSalarios();
		
		return ResponseEntity.ok(listaSalario);
	}
	
}
