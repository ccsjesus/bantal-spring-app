package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_perfil_usuario")
@Builder(setterPrefix = "with")
public class WpPerfilUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private String id;

	@Column(name="user_id")
	private String userId;

	@Column(name="nome")
	private String nome;
	
	@Column(name="display_name")
	private String displayName;
	
	@Column(name="email")
	private String email;

	@Column(name="perfil")
	private String perfil;

	@Column(name="identificacao")
	private String identificacao;
	
	@Column(name="foto")
	private byte[] foto;
	
	@OneToMany(mappedBy="wpPerfilUsuario", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpPlanoPagamento> planoPagamento;

	@Column(name="password")
	private String password;
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WpPerfilUsuario))
			return false;
		return id != null && id.equals(((WpPerfilUsuario) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}

}