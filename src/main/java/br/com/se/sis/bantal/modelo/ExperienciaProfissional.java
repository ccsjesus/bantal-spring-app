package br.com.se.sis.bantal.modelo;

import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ExperienciaProfissional {
	
	private String id;

	private String empregador;
	
	private String nomeTrabalho;
	
	private String dataInicio;
	
	private String dataTermino;
	
	private String notas;
	
	private String descAtribuicao;
	
	private boolean experiencia;
	
	private String profissao;

	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ExperienciaProfissional))
			return false;
		return id != null && id.equals(((ExperienciaProfissional) o).getId());
	}
	
	@Override
	public int hashCode() {
		return 31;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmpregador() {
		return empregador;
	}

	public void setEmpregador(String empregador) {
		this.empregador = empregador;
	}

	public String getNomeTrabalho() {
		return nomeTrabalho;
	}

	public void setNomeTrabalho(String nomeTrabalho) {
		this.nomeTrabalho = nomeTrabalho;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(String dataTermino) {
		this.dataTermino = dataTermino;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public String getDescAtribuicao() {
		return descAtribuicao;
	}

	public void setDescAtribuicao(String descAtribuicao) {
		this.descAtribuicao = descAtribuicao;
	}
	
	public boolean isExperiencia() {
		return experiencia;
	}

	public void setExperiencia(boolean experiencia) {
		this.experiencia = experiencia;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
	
	
}
