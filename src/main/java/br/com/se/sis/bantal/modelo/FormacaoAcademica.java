package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FormacaoAcademica {
	
	private String id;
	
	private String universidade;
	
	private String qualificacao;
	
	private String dataInicio;
	
	private String dataFim;
	
	private String nota;

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof FormacaoAcademica))
			return false;
		return id != null && id.equals(((FormacaoAcademica) o).getId());
	}
	
	@Override
	public int hashCode() {
		return 31;
	}
	
	
}
