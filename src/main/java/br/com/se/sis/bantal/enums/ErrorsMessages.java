package br.com.se.sis.bantal.enums;

import lombok.Getter;

@Getter
public enum ErrorsMessages {
    USER_NOT_FOUND("USER_NOT_FOUND"),
    AUTHENTICATION_FAILED("AUTHENTICATION_FAILED"),
    TOKEN_EXPIRED("TOKEN_EXPIRED"),
    REFRESH_TOKEN_EXPIRED("REFRESH_TOKEN_EXPIRED"),
    CURRICULO_NOT_FOUND("CURRICULO_NOT_FOUND"),
    PLAN_NOT_FOUND("PLAN_NOT_FOUND"),
    PLAN_EXPIRED("PLAN_EXPIRED"),
    CONNECTION_DATABASE_ERROR("CONNECTION_DATABASE_ERROR"),
    USER_OR_PASSWORD_INVALID("USER_OR_PASSWORD_INVALID"),
	
	VAGAS_NOT_FOUND("Não existem vagas"),

    PROFILE_IS_NOT_A_COMPANY("Perfil não é empresa"),
	EMPRESA_NOT_FOUND("Empresa não encontrada.");
	
    private String description;

    ErrorsMessages(String description){
        this.description = description;
    }
}
