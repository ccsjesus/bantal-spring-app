package br.com.se.sis.bantal.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.modelo.InformaticaModel;
import br.com.se.sis.bantal.modelo.WpCurriculos;
import br.com.se.sis.bantal.modelo.WpTecnologias;
import br.com.se.sis.bantal.repository.TecnologiasRepository;
import br.com.se.sis.bantal.util.iExperiencia;


@Service
@Configurable
public class TecnologiaServices {
	
	private final String ID_INICIAL = "99999";
	
	@Autowired
	private TecnologiasRepository tecnologiasRepository;
	
	
	public void adicionarSemExperiencia(CurriculoModel curriculoModel, WpCurriculos wpCurriculos) {
		
		List<WpTecnologias> xpTecnologia = tecnologiasRepository.obterTecnologiasByIdLogado(wpCurriculos.getId());
		List<InformaticaModel> xpProfissionalCandidato = curriculoModel.getInformatica();
		
		if(xpProfissionalCandidato.size() > 1) {
			xpTecnologia.stream().forEach(experiencia -> tecnologiasRepository.removerTecnologiaById(experiencia.getId()));
			
			xpProfissionalCandidato.stream().filter(a -> a.isExperiencia()).map(wpFormacao -> new WpTecnologias(wpFormacao.getId(), wpFormacao.getTecnologia(), wpFormacao.getNivel(), wpCurriculos, wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia()))
			.collect(Collectors.toList()).forEach(c -> tecnologiasRepository.inserirTecnologias(c));
		}
	}
	
	public void atualizarSemExperiencia(CurriculoModel curriculoModel, WpCurriculos wpCurriculos) {
		
		List<WpTecnologias> wpFor = tecnologiasRepository.obterTecnologiasByIdLogado(wpCurriculos.getId());
		List<InformaticaModel> listFormUsuarioConsulta = WpTecnologias.fromToModel(wpFor);
		List<InformaticaModel> listFormUsuario = curriculoModel.getInformatica();
		int tamanhoLista = curriculoModel.getInformatica().size();
		boolean semExperiencia = curriculoModel.getInformatica().size() > 0 ? curriculoModel.getInformatica().get(0).isExperiencia() : false ;
		String idInicial = "99999";
		
		if(tamanhoLista == 1 ) {
			idInicial = curriculoModel.getInformatica().get(0).getId();
		}
				
		
		if(tamanhoLista == 1 && semExperiencia && (idInicial != null && !"".equals(idInicial) && !ID_INICIAL.equals(idInicial))) {
			InformaticaModel wpExp = curriculoModel.getInformatica().get(0);
			WpTecnologias wp = tecnologiasRepository.findById(wpExp.getId()).get();
			
			wp.setDescAtribuicao(wpExp.getDescAtribuicao());
			
			
		} else {
			
			if(tamanhoLista == wpFor.size()) {
				//alterar o que ja existe
				for(InformaticaModel arg : curriculoModel.getInformatica()) {
					WpTecnologias wp = tecnologiasRepository.findById(arg.getId()).get();
					
					if(arg.isExperiencia() == iExperiencia.NAO_POSSUI) {
						wp.setExperiencia(iExperiencia.NAO_POSSUI);
						wp.setTecnologia(arg.getTecnologia());
						wp.setNivel(arg.getNivel());
						
					} else {
						wp.setTecnologia(arg.getTecnologia());
						wp.setNivel(arg.getNivel());
						wp.setDescAtribuicao(arg.getDescAtribuicao());
						wp.setExperiencia(iExperiencia.POSSUI);
						
					}
					
				}		
			} else if (tamanhoLista < wpFor.size()) {
				
				listFormUsuarioConsulta.stream().filter(a -> !listFormUsuario.contains(a)).map(b -> b.getId()).collect(Collectors.toList()).forEach(c -> tecnologiasRepository.removerTecnologiaById(c));
				
			} else if (tamanhoLista > wpFor.size()) {
				if(this.verificarSemExperiencia(listFormUsuario)) {
					
					listFormUsuario.stream()
					.map(wpFormacao -> new WpTecnologias(wpFormacao.getId(), wpFormacao.getTecnologia(), wpFormacao.getNivel(), wpCurriculos, wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia()))
					.forEach(formacao -> {
						if(formacao.getId() != null ) {
							tecnologiasRepository.removerTecnologiaById(formacao.getId());
						} 
					} );
					
				}
				
				
				listFormUsuario.stream().filter(a -> !listFormUsuarioConsulta.contains(a)).map(wpFormacao -> new WpTecnologias(wpFormacao.getId(), wpFormacao.getTecnologia(), wpFormacao.getNivel(), wpCurriculos, wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia()))
				.collect(Collectors.toList()).forEach(c -> tecnologiasRepository.inserirTecnologias(c));
				
			}
		}
		
	}

	public void atualizarSemExperiencia2(CurriculoModel curriculoModel, WpCurriculos wpCurriculos) {
		
		List<InformaticaModel> xp = curriculoModel.getInformatica();
		
		if(xp.size() == 1) {
			xp.stream()
			.map(wpFormacao -> new WpTecnologias(wpFormacao.getId(), wpFormacao.getTecnologia(), wpFormacao.getNivel(), wpCurriculos, wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia()))
			.forEach(formacao -> {
				if(formacao.getId() != null && formacao.isExperiencia()) {
					tecnologiasRepository.removerTecnologiaById(formacao.getId());
				} 
			} );
			
			xp.stream().filter(a -> a.isExperiencia()).map(wpFormacao -> new WpTecnologias(wpFormacao.getId(), wpFormacao.getTecnologia(), wpFormacao.getNivel(), wpCurriculos, wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia()))
			.collect(Collectors.toList()).forEach(c -> tecnologiasRepository.inserirTecnologias(c));
		}
	}
	
	private boolean verificarSemExperiencia(List<InformaticaModel> listaWpTecnologia) {
		for(InformaticaModel wp : listaWpTecnologia) {
			if(wp.isExperiencia()) {
				return true;			
			}
		}
		return false;
	}
}
