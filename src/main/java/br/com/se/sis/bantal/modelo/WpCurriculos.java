package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.se.sis.bantal.util.Util;
import lombok.Data;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Data
@Entity
@Table(name="wp_curriculos")
public class WpCurriculos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private String id;
	
	@Column(name="user_id")
	private String userId;

	@Column(name="nome")
	private String nome;

	@Column(name="nacionalidade")
	private String nacionalidade;

	@Column(name="estado_civil")
	private String estadoCivil;

	@Column(name="filhos")
	private String filhos;
	
	@Column(name="idade")
	private String idade;

	@Column(name="endereco")
	private String endereco;

	@Column(name="telefone")
	private String telefone;
	
	@Column(name="celular")
	private String celular;
	
	@Column(name="recados")
	private String recados;
	
	@Column(name="email")
	private String email;
	
	@Column(name="linkedin")
	private String linkedin;
	
	@Column(name="identificacao")
	private String identificacao;
	
	@Column(name="cnh")
	private String cnh;

	@Column(name="rg")
	private String rg;
	
	@Column(name="apresentacao")
	private String apresentacao;
	
	@Column(name="objetivo_profissional")
	private String objetivoProfissional;
	
	@Column(name="dt_registro")
	private LocalDateTime dtDegistro;
	
	@Column(name="pretensao_salarial")
	private String pretensaoSalarial;
	
	@Column(name="funcao_cargo")
	private String funcaoCargo;
	
	@Column(name="nivel_cargo")
	private String nivelCargo;
	
	@Column(name="foto")
	private byte[] foto;
	
	@Column(name="formacao")
	private String formacao;
	
	@OneToMany(mappedBy="wpCurriculos", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpFormacao> listFormacaoAcademica;
	
	@OneToMany(mappedBy="wpCurriculosExperiencia", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpExperienciaProfissional> listExperienciaProfissional;
	
	@OneToMany(mappedBy="wpCurriculosCargo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpCargoPreterido> listCargoPreterido;
	
	@OneToMany(mappedBy="wpCurriculosTecnologias", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpTecnologias> listTecnologias;
	
	@OneToMany(mappedBy="wpCurriculosIdiomas", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpIdioma> listIdiomas;
	
	@OneToMany(mappedBy="wpCurriculosAreaAtuacao", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpAreaAtuacao> listAreaAtuacao;
	
	
	public WpCurriculos(){
		
	}
	
	public WpFormacao addWpFormacao(WpFormacao wpFormacao) {
		getListFormacaoAcademica().add(wpFormacao);
		wpFormacao.setWpCurriculos(this);

		return wpFormacao;
	}

	public WpFormacao removeWpFormacao(WpFormacao wpFormacao) {
		getListFormacaoAcademica().remove(wpFormacao);
		wpFormacao.setWpCurriculos(null);

		return wpFormacao;
	}
	
	public byte[] getFoto() {
		if(foto == null) {
			return foto;
		} else {
			return Util.decompressBytes(foto);
		}
	}
	

}