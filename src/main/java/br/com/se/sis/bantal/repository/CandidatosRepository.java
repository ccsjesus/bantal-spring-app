package br.com.se.sis.bantal.repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.UserMeta;
import br.com.se.sis.bantal.modelo.WpPost;

public interface CandidatosRepository extends JpaRepository<WpPost, String>, JpaSpecificationExecutor<WpPost> {

	
	@Query(value="select distinct id, meta_key, meta_value from (SELECT wp_users.ID, wp_usermeta.meta_key, wp_usermeta.meta_value FROM `wp_users` \n" + 
			"join wp_usermeta on wp_users.ID = wp_usermeta.user_id where wp_users.ID in :idUsuarios\n" + 
			"union \n" + 
			"select wp_posts.post_author, wp_postmeta.meta_key, wp_postmeta.meta_value from wp_posts\n" + 
			"join wp_postmeta on wp_postmeta.post_id = wp_posts.ID where wp_posts.post_author in :idUsuarios ) resultado where meta_key in (:listaMetaKey)order by id asc", 
			  nativeQuery = true)
	List<Object[]> consultarCandidatosId(@Param("idUsuarios") List<String> idUsuarios, @Param("listaMetaKey") List<String> listaMetaKey);
	
//	@Query(value="", 
//			  nativeQuery = true)
//	List<WpPost[]> consultarDadosPerfilPorId(@Param("idUsuarios") List<String> idUsuarios, @Param("listaMetaKey") List<String> listaMetaKey);
	
	
	@Query(value="select id, meta_key, meta_value from (SELECT wp_users.ID, wp_usermeta.meta_key, wp_usermeta.meta_value FROM wp_users join wp_usermeta on wp_users.ID = wp_usermeta.user_id where wp_users.ID in :idUsuarios union select wp_posts.post_author, wp_postmeta.meta_key, wp_postmeta.meta_value from wp_posts join wp_postmeta on wp_postmeta.post_id = wp_posts.ID where wp_posts.post_author in :idUsuarios and wp_postmeta.meta_key ='_candidate_cpf' and wp_postmeta.meta_value = :cpfUsuario) resultado", 
			  nativeQuery = true)
	List<Object[]> consultarCandidatosIdV2(@Param("idUsuarios") List<String> idUsuarios, @Param("cpfUsuario") String cpfUsuario);
	
	@Query(value="select new br.com.se.sis.bantal.modelo.UserMeta(wp_users.id, wp_usermeta.metaKey, wp_usermeta.metaValue) FROM WpUser wp_users join WpUsermeta wp_usermeta on wp_users.id = wp_usermeta.wpUser.id where wp_users.id in (:idUsuarios) ")
	List<UserMeta> consultarDadosCandidatosId(@Param("idUsuarios") List<BigDecimal> idUsuarios);
	
	@Query(value="select new br.com.se.sis.bantal.modelo.UserMeta(wp_posts.postAuthor, wp_postmeta.metaKey, wp_postmeta.metaValue) from WpPost wp_posts join WpPostmeta wp_postmeta on wp_postmeta.wpPost.id = wp_posts.id where wp_posts.postAuthor in (:idUsuarios) order by id asc")
	List<UserMeta> consultarDadosCandidatosIdUnion(@Param("idUsuarios") List<BigDecimal> idUsuarios);
	
}
