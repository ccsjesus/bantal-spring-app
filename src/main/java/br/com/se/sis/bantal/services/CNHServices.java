package br.com.se.sis.bantal.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.CNHModel;
import br.com.se.sis.bantal.modelo.WpParametros;
import br.com.se.sis.bantal.repository.ParametrosRepository;
import br.com.se.sis.bantal.util.IParametros;
import br.com.se.sis.bantal.util.Util;


@Service
@Configurable
public class CNHServices {
	
	@Autowired
	private ParametrosRepository parametrosServices;
	
	
	public List<CNHModel> obterTodasCNH() {
		WpParametros wpParametros = parametrosServices.obterValorPorCategoria(IParametros.CNH);
		String[] valor = Util.splitEspecial(wpParametros.getValor());
		
		List<CNHModel> listAreaAtuacao = new ArrayList<>();
		CNHModel areaAtuacao;
		for(String str : valor) {
			areaAtuacao = new CNHModel();
			areaAtuacao.setName(str);
			areaAtuacao.setValue(str);
			areaAtuacao.setSelected(false);
			listAreaAtuacao.add(areaAtuacao);
		}
		
		return listAreaAtuacao;
		
	}

}
