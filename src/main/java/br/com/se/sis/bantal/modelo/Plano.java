package br.com.se.sis.bantal.modelo;

import lombok.Data;

@Data
public class Plano {
	
	private String referenciaPlano;
	
	private String nomePlano;
	
	private String codigoTransacao;
	
	private String slug;
	
	private boolean aderido = false;
	
	private int duracao;
	
}
