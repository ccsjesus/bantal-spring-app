package br.com.se.sis.bantal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import br.com.se.sis.bantal.modelo.WpTerm;

public interface TaxonomiaRepository extends JpaRepository<WpTerm, String>, JpaSpecificationExecutor<WpTerm> {

	@Query(value="SELECT * FROM wp_terms JOIN wp_term_taxonomy ON wp_terms.term_id = wp_term_taxonomy.term_id where wp_term_taxonomy.taxonomy = :opcao group by wp_terms.name ORDER by wp_term_taxonomy.taxonomy", 
			  nativeQuery = true)	
	List<WpTerm> obterOpcoesVagaPorTipo(String opcao);
	
	@Query(value="SELECT * FROM wp_terms JOIN wp_term_taxonomy ON wp_terms.term_id = wp_term_taxonomy.term_id where wp_term_taxonomy.taxonomy = :opcao and wp_term_taxonomy.term_id = :idValor group by wp_terms.name ORDER by wp_term_taxonomy.taxonomy", 
			  nativeQuery = true)	
	WpTerm obterOpcoesVagaPorTipoId(String opcao, int idValor);
	
}
