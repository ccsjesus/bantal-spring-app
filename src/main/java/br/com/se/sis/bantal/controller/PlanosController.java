package br.com.se.sis.bantal.controller;

import java.io.UnsupportedEncodingException;

import javax.transaction.Transactional;

import br.com.se.sis.bantal.annotations.UserCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.annotations.UserEmployerCanOnlyAccessTheirOwnResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.modelo.WpTokenSistema;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.PlanoService;

@RestController
@RequestMapping("/v1/planos")
public class PlanosController {


	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private PlanoService planoServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/empresa")
	@UserEmployerCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getPlanosEmpresa() throws IllegalArgumentException{

		return ResponseEntity.ok(planoServices.obterPlanoEmpresa());
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/candidatos")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getPlanosCadidatos() throws IllegalArgumentException{

		return ResponseEntity.ok(planoServices.obterPlanoCandidato());
	}
	
}
