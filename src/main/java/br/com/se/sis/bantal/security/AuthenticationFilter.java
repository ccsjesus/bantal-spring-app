package br.com.se.sis.bantal.security;

import br.com.se.sis.bantal.clients.ClientUsuarioOauthWp;
import br.com.se.sis.bantal.controller.requests.LoginRequest;
import br.com.se.sis.bantal.controller.requests.RegisterUserRequest;
import br.com.se.sis.bantal.controller.requests.ResponseToken;
import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.exceptions.AuthenticationException;
import br.com.se.sis.bantal.exceptions.AuthenticationExceptionError;
import br.com.se.sis.bantal.infra.SpringLoggingFilter;
import br.com.se.sis.bantal.modelo.*;
import br.com.se.sis.bantal.repository.UserAuthenticationRepository;
import br.com.se.sis.bantal.repository.WpPerfilRepository;
import br.com.se.sis.bantal.services.RefreshTokenService;
import br.com.se.sis.bantal.services.UserDetailCustomService;
import br.com.se.sis.bantal.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);

    private AuthenticationManager authenticationManager;

    private UserAuthenticationRepository userAuthenticationRepository;

    private JWTUtil jwtUtil;

    private RefreshTokenService refreshTokenService;

    private WpPerfilRepository profileRepository;

    private UserDetailCustomService userDetailCustomService;

    private ClientUsuarioOauthWp clientUserOauthWp;

    private UsuarioService userService;

    public AuthenticationFilter(AuthenticationManager authenticationManager,
                                UserAuthenticationRepository userAuthenticationRepository,
                                JWTUtil jwtUtil,
                                RefreshTokenService refreshTokenService,
                                WpPerfilRepository profileRepository,
                                UserDetailCustomService userDetailCustomService,
                                ClientUsuarioOauthWp clientUserOauthWp,
                                UsuarioService userService) {

        this.authenticationManager = authenticationManager;
        this.userAuthenticationRepository = userAuthenticationRepository;
        this.jwtUtil = jwtUtil;
        this.refreshTokenService = refreshTokenService;
        this.profileRepository = profileRepository;
        this.userDetailCustomService = userDetailCustomService;
        this.clientUserOauthWp = clientUserOauthWp;
        this.userService = userService;
    }

    //TODO Aqui ocorre o LOGIN
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {

        ObjectMapper mapper = new ObjectMapper();
        LoginRequest loginRequest = null;
        try {
            loginRequest = mapper.readValue(request.getInputStream(), LoginRequest.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        WpUserAuthenticationEntity entity = userAuthenticationRepository.findByIdentifier(loginRequest.getIdentifier()).orElse(null);
        UsernamePasswordAuthenticationToken oauthToken = getOauthAuthentication(loginRequest, entity);

        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(oauthToken);
        } catch (Exception ex ) {
            var message = "Error request login: user:" + loginRequest.getIdentifier();
            logger.error(message, ex);
            throw new AuthenticationException(ErrorsMessages.USER_OR_PASSWORD_INVALID.getDescription(), ErrorsMessages.USER_OR_PASSWORD_INVALID);
        }
        return authentication;

    }

    public UsernamePasswordAuthenticationToken getOauthAuthentication(LoginRequest loginRequest, WpUserAuthenticationEntity entity) {
        WpPerfilUsuario profileUser = profileRepository.findByIdentificacao(loginRequest.getIdentifier()).orElse(null);
        String userIdEntity;
        WpUser pWpUser;
        Usuario user = null;

        if(entity == null) {
            pWpUser = getWpUser(loginRequest);

            if (profileUser == null) {
                user = userService.cadastrarInformacoesIniciais(pWpUser.getId().toString(), pWpUser);
                profileUser = WpPerfilUsuario.builder()
                        .withEmail(user.getUserEmail())
                        .withFoto(user.getFoto())
                        .withDisplayName(user.getDisplayName())
                        .withIdentificacao(user.getIdentificacao())
                        .withPerfil(user.getRoleSystem().name())
                        .withNome(user.getDisplayName()).build();
            }
            entity = registerNewAuthentication(loginRequest, profileUser);
            
            userIdEntity = entity.getId() + "";
            if (user == null) {
            	user = userService.obterDadosUsuario(pWpUser.getId().toString());
            }
            updateCurriculoUser(user, userIdEntity);
            updatePlanAccessUserByUser(user, userIdEntity);
        } else {
            WpUser wpUser = userService.findUserByEmail(entity.getEmail());
            Usuario usuario = Usuario.builder()
                    .withId(wpUser.getId()).build();
            updatePlanAccessUserByUser(usuario, entity.getId() + "");
        }
        return new UsernamePasswordAuthenticationToken(entity.getIdentifier(), loginRequest.getPassword());
    }

    private WpUser getWpUser(LoginRequest loginRequest) {
        WpUser pWpUser;
        ClientUsuarioOauthWp client;
        pWpUser = WpUser.builder().withUserEmail(loginRequest.getIdentifier())
                .withUserPass(loginRequest.getPassword()).build();

        client = clientUserOauthWp.obterUsuarioWp(pWpUser);

        if (client == null && client.getUser_email() == null) {
            throw new AuthenticationException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND);
        }

        pWpUser = userService.findUserByEmail(client.getUser_email());
        return pWpUser;
    }

    private WpUserAuthenticationEntity registerNewAuthentication(LoginRequest loginRequest, WpPerfilUsuario profileUser) {
        RegisterUserRequest register = RegisterUserRequest.toRequest(profileUser);
        register.setPassword(loginRequest.getPassword());
        WpUserAuthenticationEntity userAuthenticationEntity = userDetailCustomService.registerUser(WpUserAuthenticationEntity.toAuthenticationEntity(register));
        profileUser = profileRepository.findByIdentificacao(loginRequest.getIdentifier()).orElse(null);

        return updateProfileUser(profileUser, userAuthenticationEntity);
    }

    private WpUserAuthenticationEntity updateProfileUser(WpPerfilUsuario profileUser, WpUserAuthenticationEntity userAuthenticationEntity) {
        profileUser.setUserId(userAuthenticationEntity.getId() + "");
        profileUser.setPerfil(Role.get(profileUser.getPerfil()).name());
        profileRepository.saveAndFlush(profileUser);
        return userAuthenticationEntity;
    }

    private void updateCurriculoUser(Usuario user, String newUserId) {
        userService.updateUserIdCurriculoById(user.getIdCurriculo(),newUserId, user.getRoleSystem());
    }

    private void updatePlanAccessUserByUser(Usuario user, String newUserId) {
        userService.updateCdUserPlanPaymentByProfileUserByUser(user,newUserId);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException {
        UserCustomDetails customDetails = ((UserCustomDetails)authResult.getPrincipal());
        String token = jwtUtil.generateTokenWithPayload(customDetails.getId(), customDetails);
        //response.addHeader("Authorization", "Bearer "+ token);

        RefreshToken refreshToken = refreshTokenService.createRefreshToken(customDetails.getId());

        ResponseToken responseToken = ResponseToken.builder()
                .withAccessToken(token)
                .withExpiresIn(jwtUtil.getExpiration())
                .withTokenType("Bearer")
                .withRefreshToken(refreshToken.getToken())
                .build();

        responseToken(response, responseToken);
    }

    private void responseToken(HttpServletResponse response, ResponseToken responseToken) throws IOException {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_OK);
        response.getOutputStream().print(new ObjectMapper().writerWithDefaultPrettyPrinter()
                .writeValueAsString(responseToken));
    }
}
