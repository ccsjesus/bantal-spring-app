package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.UserCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.NivelCargoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/v1/nivel-cargo")
public class NivelCargoController {


	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private NivelCargoServices nivelCargoServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getLevelPorToken() throws IllegalArgumentException{

		return ResponseEntity.ok(nivelCargoServices.obterTodosNiveisCargo());
	}
	
}
