package br.com.se.sis.bantal.security;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.exceptions.AuthenticationException;
import br.com.se.sis.bantal.exceptions.TokenExpiredException;
import br.com.se.sis.bantal.services.UserDetailCustomService;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthorizationFilter extends BasicAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    private UserDetailCustomService userCustomDetails;
    private JWTUtil jwtUtil;

    public AuthorizationFilter(AuthenticationManager authenticationManager, UserDetailCustomService userCustomDetails, JWTUtil jwtUtil) {
        super(authenticationManager);
        this.authenticationManager =authenticationManager ;
        this.userCustomDetails = userCustomDetails;
        this.jwtUtil = jwtUtil;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

            String authorization = request.getHeader(HttpHeaders.AUTHORIZATION);

            if(authorization != null && authorization.startsWith("Bearer")) {
                UsernamePasswordAuthenticationToken auth = getAuthentication(authorization.split(" ")[1]);
                SecurityContextHolder.getContext().setAuthentication(auth);

            }
            chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) {
        if(!jwtUtil.isValidToken(token)) {
            //throw new TokenExpiredException(ErrorsMessages.TOKEN_EXPIRED.getDescription(), ErrorsMessages.TOKEN_EXPIRED);
        }
        String subject = jwtUtil.getSubject(token);
        UserDetails customer = userCustomDetails.findById(subject).get();
        return new UsernamePasswordAuthenticationToken(customer, null, customer.getAuthorities());
    }


}
