package br.com.se.sis.bantal.modelo;

public class CursoModel {
	
	private String instituicao;
	
	private String nomeCurso;
	
	private int anoInicio;
	
	private int anoFim;
	
	private int cargaHoraria;
	
	public CursoModel() {
		// TODO Auto-generated constructor stub
	}
	
	

	public CursoModel(String instituicao, String nomeCurso, int anoInicio, int anoFim, int cargaHoraria) {
		this.instituicao = instituicao;
		this.nomeCurso = nomeCurso;
		this.anoInicio = anoInicio;
		this.anoFim = anoFim;
		this.cargaHoraria = cargaHoraria;
	}



	public String getInstituicao() {
		return instituicao;
	}

	public void setInstituicao(String instituicao) {
		this.instituicao = instituicao;
	}

	public String getNomeCurso() {
		return nomeCurso;
	}

	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}

	public int getAnoInicio() {
		return anoInicio;
	}

	public void setAnoInicio(int anoInicio) {
		this.anoInicio = anoInicio;
	}

	public int getAnoFim() {
		return anoFim;
	}

	public void setAnoFim(int anoFim) {
		this.anoFim = anoFim;
	}

	public int getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	
	
	
}
