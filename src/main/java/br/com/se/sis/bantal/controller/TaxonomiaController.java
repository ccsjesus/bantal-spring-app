package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.modelo.Term;
import br.com.se.sis.bantal.modelo.WpTerm;
import br.com.se.sis.bantal.repository.TaxonomiaRepository;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.util.TipoVaga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("/taxonomia")
public class TaxonomiaController {

	@Autowired
	private TaxonomiaRepository taxonomiaRepository;
	
	@Autowired
	private TokenRepository tokenRepository;

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/tipo-vaga")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> getTipoVaga() throws IllegalArgumentException, UnsupportedEncodingException {

		List<WpTerm> t = taxonomiaRepository.obterOpcoesVagaPorTipo(TipoVaga.TIPO_JOB.getValor());
		List<Term> term = Term.converterToList(t);
		return ResponseEntity.ok(term);
	}

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/areas-atuacao")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> getAreasAtuacao() throws IllegalArgumentException, UnsupportedEncodingException {

		List<WpTerm> t = taxonomiaRepository.obterOpcoesVagaPorTipo(TipoVaga.RESUME_CATEGORY.getValor());
		List<Term> term = Term.converterToList(t);

		return ResponseEntity.ok(term);
	}

}
