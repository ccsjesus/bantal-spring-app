package br.com.se.sis.bantal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpParametros;

public interface ParametrosRepository extends JpaRepository<WpParametros, String>, JpaSpecificationExecutor<WpParametros> {

	@Query(value = "select * from wp_parametros par where par.categoria = :cdCategoria", nativeQuery = true)
	WpParametros obterValorPorCategoria(@Param("cdCategoria") String cdCategoria);
	
}
