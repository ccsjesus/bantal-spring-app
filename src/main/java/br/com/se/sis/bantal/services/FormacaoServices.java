package br.com.se.sis.bantal.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.modelo.FormacaoAcademica;
import br.com.se.sis.bantal.modelo.WpCurriculos;
import br.com.se.sis.bantal.modelo.WpFormacao;
import br.com.se.sis.bantal.repository.FormacaoAcademicaRepository;


@Service
@Configurable
public class FormacaoServices {
	
	@Autowired
	private FormacaoAcademicaRepository formacaoAcademicaRepository;
	
	
	public void atualizarFormacaoAcademica(CurriculoModel curriculoModel, WpCurriculos wpCurriculos) {
		
		List<WpFormacao> wpFor = formacaoAcademicaRepository.obterFormacaoByIdLogado(wpCurriculos.getId());
		List<FormacaoAcademica> listFormAcademicaUsuarioConsulta = WpFormacao.fromToFormacaoAcademica(wpFor);
		List<FormacaoAcademica> listFormAcademicaUsuario = curriculoModel.getFormacaoAcademica();


		if(curriculoModel.getFormacaoAcademica().size() == wpFor.size()) {
			//alterar o que ja existe
			for(FormacaoAcademica arg : curriculoModel.getFormacaoAcademica()) {
				Optional<WpFormacao> wpFormacao = formacaoAcademicaRepository.findById(arg.getId());
				WpFormacao wpf = wpFormacao.get();
				wpf.setUniversidade(arg.getUniversidade());
				wpf.setDtInicio(arg.getDataInicio());
				wpf.setDtTermino(arg.getDataFim());
				wpf.setNota(arg.getNota());
				wpf.setQualificacao(arg.getQualificacao());
			}		
		} else if (curriculoModel.getFormacaoAcademica().size() < wpFor.size()) {

			listFormAcademicaUsuarioConsulta.stream().filter(a -> !listFormAcademicaUsuario.contains(a)).map(b -> b.getId()).collect(Collectors.toList()).forEach(c -> formacaoAcademicaRepository.removerFormacaoById(c));
			
		} else if (curriculoModel.getFormacaoAcademica().size() > wpFor.size()) {
			
			listFormAcademicaUsuario.stream().filter(a -> !listFormAcademicaUsuarioConsulta.contains(a)).map(wpFormacao -> new WpFormacao(wpFormacao.getUniversidade(), wpFormacao.getQualificacao(), wpFormacao.getDataInicio(), wpFormacao.getDataFim(), wpFormacao.getNota(), wpCurriculos))
			.collect(Collectors.toList()).forEach(c -> formacaoAcademicaRepository.inserirFormacaoAcademica(c));
			
		}
		
	}

}
