package br.com.se.sis.bantal.modelo;

import lombok.Data;

@Data
public class AreaAtuacaoDTO {
	
	private String idAreaAtuacao;
	private String nome;
	private String foto;
	private Long quantidade;

	public AreaAtuacaoDTO(String idAreaAtuacao, String nome, String foto, Long quantidade) {
		this.idAreaAtuacao = idAreaAtuacao;
		this.nome = nome;
		this.foto = foto;
		this.quantidade = quantidade;
	}
	
}
