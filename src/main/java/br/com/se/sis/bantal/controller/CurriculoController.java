package br.com.se.sis.bantal.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.annotations.UserCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.modelo.Usuario;
import br.com.se.sis.bantal.modelo.WpPostmeta;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.CurriculoServices;
import br.com.se.sis.bantal.util.Constantes;
import br.com.se.sis.pagseguro.exception.TransacaoAbortadaException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@RestController
@RequestMapping("/v1/curriculo")
public class CurriculoController {

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private CurriculoServices curriculoServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getCurriculoPorToken(Authentication authentication) throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(curriculoServices.obterCurriculoModelUsuario(customDetails.getId()));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/profissao/{idProfissao}")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> obterCurriculoPorProfissao(@PathVariable("idProfissao") String idProfissao) throws IllegalArgumentException {		
		return ResponseEntity.ok(curriculoServices.obterCurriculoPorProfissao(idProfissao));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-informacoes-principais-candidato")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> updateInformacoesPrincipaisCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws TransacaoAbortadaException  {
			UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

			curriculoServices.atualizarDadosCurriculo(customDetails.getId(), curriculo);
			return ResponseEntity.status(HttpStatus.CREATED).body(null);	    
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-dados-pessoais")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> updateDadosPessoaisCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws TransacaoAbortadaException  {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		curriculoServices.atualizarDadosCurriculo(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-formacao-candidato")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> updateFormacaoCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		curriculoServices.atualizarDadosCurriculo(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-idiomas-candidato")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> updateIdiomasCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		curriculoServices.atualizarDadosCurriculo(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);

	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-informatica-candidato")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> updateInformaticaCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		curriculoServices.atualizarDadosCurriculo(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);

	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-experiencia-profissional-candidato")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> updateExperienciaCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws IllegalArgumentException, UnsupportedEncodingException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		curriculoServices.atualizarDadosCurriculo(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);

	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-cargo-preterido-candidato")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> updateCargoPreteridoCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		curriculoServices.atualizarDadosCurriculo(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);

	}
	
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/imprimir")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> imprimir(Authentication authentication, @RequestBody List<String> array, HttpServletResponse response)
			throws JRException, IOException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		InputStream jasperStream = this.getClass().getResourceAsStream("/relatorios/livros.jasper");

		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);

		if(array.isEmpty()){
			array = Arrays.asList(customDetails.getId());
		}
		
		List<Usuario> listUsuario = curriculoServices.cadastrarInformacoesImpressaoCurriculo(array);
		
		JRDataSource jrDataSource = new JRBeanCollectionDataSource(listUsuario);
		JRBeanCollectionDataSource reports = new JRBeanCollectionDataSource(listUsuario, false);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ItemDataSource",reports);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrDataSource);

		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline; filename=bantal_relatorio.pdf");

		// Faz a exportação do relatório para o HttpServletResponse
		final OutputStream outStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		return ResponseEntity.ok("Impressão realizada com suceso!");
	}
		
	private boolean metaKeyExists(WpPostmeta meta, Constantes keyMeta) {
		return meta.getMetaKey().equals(keyMeta.toString()) ? true : false;
		
	}
	
	
	
	
}
