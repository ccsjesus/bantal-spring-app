package br.com.se.sis.bantal.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpUser;

public interface WpUserRepository extends JpaRepository<WpUser, String>, JpaSpecificationExecutor<WpUser> {


	@Query(value=" Select * from wp_users users where users.id = :id", nativeQuery = true)
	Optional<List<WpUser>> obterInformacoesUsuarioPorId(@Param("id") String id);

	Optional<WpUser> findByUserEmail(String email);
}
