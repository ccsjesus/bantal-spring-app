package br.com.se.sis.bantal.modelo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the wp_term_relationships database table.
 * 
 */
@Entity
@Table(name="wp_term_relationships")
@NamedQuery(name="WpTermRelationship.findAll", query="SELECT w FROM WpTermRelationship w")
public class WpTermRelationship implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private WpTermRelationshipPK id;

	@Column(name="term_order")
	private int termOrder;

	//bi-directional many-to-one association to WpTermTaxonomy
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="term_taxonomy_id", insertable = false, updatable = false)
	@JsonBackReference
	private WpTermTaxonomy wpTermTaxonomy;

	public WpTermRelationship() {
	}

	public WpTermRelationshipPK getId() {
		return this.id;
	}

	public void setId(WpTermRelationshipPK id) {
		this.id = id;
	}

	public int getTermOrder() {
		return this.termOrder;
	}

	public void setTermOrder(int termOrder) {
		this.termOrder = termOrder;
	}

	public WpTermTaxonomy getWpTermTaxonomy() {
		return this.wpTermTaxonomy;
	}

	public void setWpTermTaxonomy(WpTermTaxonomy wpTermTaxonomy) {
		this.wpTermTaxonomy = wpTermTaxonomy;
	}

}