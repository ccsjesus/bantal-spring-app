package br.com.se.sis.bantal.controller;

import java.io.UnsupportedEncodingException;

import javax.transaction.Transactional;

import br.com.se.sis.bantal.annotations.UserEmployerAndCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.security.UserCustomDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.modelo.WpTokenSistema;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.VagasService;

@RestController
@RequestMapping("/vagas-empresa")
public class VagasEmpresaController {

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private VagasService vagasGerenciador;

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserEmployerAndCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getVagasDisponiveis(Authentication authentication) throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(vagasGerenciador.obterTodasVagasPublicadasIncritasCandidato(customDetails.getId()));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/vigentes")
	@UserEmployerAndCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getVagasVigentes(Authentication authentication) throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(vagasGerenciador.obterTodasVagasPublicadasVigentesIncritasCandidato(customDetails.getId()));
	}
	
	

}
