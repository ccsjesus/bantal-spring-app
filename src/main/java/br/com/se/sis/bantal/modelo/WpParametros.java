package br.com.se.sis.bantal.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The persistent class for the wp_posts database table.
 * 
 */

@Data
@Entity
@Table(name = "wp_parametros")
public class WpParametros implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private String cdParametro;

	@Column(name = "categoria")
	private String nomeCategoria;

	@Column(name = "valor")
	private String valor;


	public WpParametros() {
	}


}