package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.SuporteService;
import br.com.se.sis.pagseguro.domain.vo.SuporteModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/suporte")
public class SuporteController {

	
	@Autowired
	private TokenRepository tokenRepository;
	
	@Autowired
	private SuporteService suporteService;
	

	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/enviar-email")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> sendMessage(Authentication authentication, @RequestBody SuporteModel suporteModel)
			throws IllegalArgumentException {

		return ResponseEntity.ok(suporteService.sendEmail(suporteModel));
	}
	
	
	

}
