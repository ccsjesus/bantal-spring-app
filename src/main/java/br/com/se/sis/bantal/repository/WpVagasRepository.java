package br.com.se.sis.bantal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import br.com.se.sis.bantal.modelo.WpVagas;

public interface WpVagasRepository extends JpaRepository<WpVagas, String>, JpaSpecificationExecutor<WpVagas> {

	@Query(value="select * from wp_vagas where wp_vagas.user_id = :idLogado", nativeQuery = true)
	public List<WpVagas> obterVagasCadastradasPorIdUsuario(String idLogado);
	
	@Query(value="select * from wp_vagas where wp_vagas.id = :idVaga", nativeQuery = true)
	public WpVagas obterVagaPorId(String idVaga);
	
	@Query(value="select * from wp_vagas v join wp_inscricao i on i.id_vaga = v.id where i.user_id = :userIdCandidato", nativeQuery = true)
	public List<WpVagas> obterVagasInscritasCandidato(String userIdCandidato);
	
	@Query(value="select * from wp_vagas v where v.status = :status", nativeQuery = true)
	public List<WpVagas> obterTodasVagasPorStatus(String status);
	
	@Query(value="select * from wp_vagas v where v.status = 'approved' and CURDATE() <= STR_TO_DATE(v.data_limite,'%d/%m/%Y')", nativeQuery = true)
	public List<WpVagas> obterTodasVagasVigentes();
	
	@Query(value="select * from wp_vagas v order by v.status DESC", nativeQuery = true)
	public List<WpVagas> obterTodasVagas();
	
}
