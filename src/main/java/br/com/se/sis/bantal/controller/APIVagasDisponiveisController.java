package br.com.se.sis.bantal.controller;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.services.VagasDisponiveisServices;

@RestController()
@RequestMapping("/publico")
public class APIVagasDisponiveisController {
	@Autowired
	private VagasDisponiveisServices vagasDisponiveisServices;

	@Transactional
    @RequestMapping(method = RequestMethod.GET, value = "/vagas-disponives")
    public ResponseEntity<Object> recuperarVagasDisponiveis() throws IllegalArgumentException{
        return ResponseEntity.ok(vagasDisponiveisServices.recuperarVagasDisponiveis());
    }
	
	@Transactional
    @RequestMapping(method = RequestMethod.GET, value = "/vagas-disponives/empresa/{idEmpresa}")
    public ResponseEntity<Object> recuperarVagasDisponiveisPorEmpresa(@PathVariable String idEmpresa) throws IllegalArgumentException{
        return ResponseEntity.ok(vagasDisponiveisServices.recuperarVagasDisponiveisPorIdEmpr(idEmpresa));
    }
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/vagas-disponives/detalhamento/{idVaga}")
	public ResponseEntity<Object> recuperarDetalhementoVagaDisponivelPorID(@PathVariable String idVaga)
			throws IllegalArgumentException {
		System.out.println("Entrou metodo 2");
		return ResponseEntity.ok(vagasDisponiveisServices.recuperarDetalhementoVagaDisponivelPorID(idVaga));
	}
	
	@Transactional
    @RequestMapping(method = RequestMethod.GET, value = "/empresas")
    public ResponseEntity<Object> recuperarEmpresas() throws IllegalArgumentException{
        return ResponseEntity.ok(vagasDisponiveisServices.recuperarEmpresas());
    }
	
	@Transactional
    @RequestMapping(method = RequestMethod.GET, value = "/empresas/{idEmpresa}")
    public ResponseEntity<Object> recuperarDetalhamentoEmpresaPorId(@PathVariable String idEmpresa) throws IllegalArgumentException{
        return ResponseEntity.ok(vagasDisponiveisServices.recuperarDetalhamentoEmpresaPorId(idEmpresa));
    }

    @Transactional
    @RequestMapping(method = RequestMethod.GET, value = "/empresas/sigla/{sigla}")
    public ResponseEntity<Object> recuperarDetalhamentoEmpresaPorSigla(@PathVariable String sigla) throws IllegalArgumentException{
        return ResponseEntity.ok(vagasDisponiveisServices.recuperarDetalhamentoEmpresaPorSigla(sigla));
    }

}