package br.com.se.sis.bantal.util;

public @interface IUtils {

	String CURINGA = "[[*]]";
	
	//Status das Vagas
	
	String NOVA = "new";
	String EXPIRADO = "expired";
	String APROVADA= "approved";
	String REJEITADO = "rejeitado";
}
