package br.com.se.sis.bantal.services;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.enums.TipoPlano;
import br.com.se.sis.bantal.exceptions.PlanNotFoundException;
import br.com.se.sis.bantal.modelo.WpControleVagaAvulsa;
import br.com.se.sis.bantal.modelo.WpPlanoPagamento;
import br.com.se.sis.bantal.modelo.WpPlanos;
import br.com.se.sis.bantal.repository.ControleVagaAvulsaRepository;

@Service
@Configurable
public class ControleVagaAvulsaService {

	@Autowired
	private ControleVagaAvulsaRepository controleVagaAvulsaRepository;

	@Autowired
	private PlanoPagamentoService planosService;

	public boolean isTipoPlanoAvulso(WpPlanos planos) {
		if (Objects.isNull(planos))
			return false;

		if (planos.getTipo().equalsIgnoreCase(TipoPlano.AVULSO.getDescricao()))
			return true;

		return false;
	}

	public WpControleVagaAvulsa registrarInicioControleVagaAvulsa(WpPlanoPagamento planoPagamento) {
		WpControleVagaAvulsa controleVagaAvulsa = new WpControleVagaAvulsa();

		controleVagaAvulsa.setWpPlanoPagamento(planoPagamento);
		controleVagaAvulsa.setQuantidade(planoPagamento.getWpPlano().getDuracao());

		return controleVagaAvulsaRepository.save(controleVagaAvulsa);
	}

	public void registrarVagaAvulsaPublicada(String idUsuarioLogado) {
		WpPlanoPagamento planoAtivoUsuario = planosService.obterPlanoPagamentoPorIdUsuario(idUsuarioLogado);

		if (!isTipoPlanoAvulso(planoAtivoUsuario.getWpPlano()))
			return;

		WpControleVagaAvulsa controleVagaAvulsa = controleVagaAvulsaRepository
				.recuperarControleVagasAvulsasPorEmpresa(idUsuarioLogado);

		if (!Objects.isNull(controleVagaAvulsa) && controleVagaAvulsa.getQuantidade() <= 0) {
			throw new PlanNotFoundException(ErrorsMessages.PLAN_EXPIRED.getDescription(), ErrorsMessages.PLAN_EXPIRED);
		}

		controleVagaAvulsaRepository.subtratirQuantidadeVagaAvulsa(controleVagaAvulsa.getCdControle());
	}
	
	public void registrarVagaAvulsaPublicadaRejeitada(String idUsuarioLogado) {
		WpPlanoPagamento planoAtivoUsuario = planosService.obterPlanoPagamentoPorIdUsuario(idUsuarioLogado);

		if (!isTipoPlanoAvulso(planoAtivoUsuario.getWpPlano()))
			return;

		WpControleVagaAvulsa controleVagaAvulsa = controleVagaAvulsaRepository
				.recuperarControleVagasAvulsasPorEmpresa(idUsuarioLogado);

		controleVagaAvulsaRepository.adicionarQuantidadeVagaAvulsa(controleVagaAvulsa.getCdControle());
	}
	
	public boolean isEmpresaComPlanoAvulsoPermitePublicarVaga(WpPlanoPagamento planoAtivoUsuario, boolean isFalseLevantarExcecao) {
		
		if (!isTipoPlanoAvulso(planoAtivoUsuario.getWpPlano()))
			return true;

		WpControleVagaAvulsa controleVagaAvulsa = controleVagaAvulsaRepository.recuperarControleVagasAvulsasPorEmpresa(planoAtivoUsuario.getCdUsuario());

		if (!Objects.isNull(controleVagaAvulsa) && controleVagaAvulsa.getQuantidade() <= 0) {
			if (isFalseLevantarExcecao) {
				throw new PlanNotFoundException(ErrorsMessages.PLAN_EXPIRED.getDescription(),ErrorsMessages.PLAN_EXPIRED);
			}
			return false;
		}
		
		return true;
	}

}
