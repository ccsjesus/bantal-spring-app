package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_experiencia_profissional")
public class WpExperienciaProfissional implements Serializable {
	
	private static final long serialVersionUID = 1L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private String id;
	

	@Column(name="empregador")
	private String empregador;
	

	@Column(name="cargo_ocupado")
	private String cargoOcupado;


	@Column(name="dt_inicio")
	private String dtInicio;
	

	@Column(name="dt_fim")
	private String dtFim;
	

	@Column(name="notas")
	private String notas;
	
	@Column(name="desc_atribuicao")
	private String descAtribuicao;
	
	@Column(name = "experiencia", nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean experiencia;
	
	@Column(name="id_profissao")
	private String idProfissao;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="id_curriculo", insertable = false, updatable = false)
	@JsonBackReference
	private WpCurriculos wpCurriculosExperiencia;


	public WpExperienciaProfissional(String empregador, String cargoOcupado, String dtInicio, String dtFim,
			String notas, String desAtribuicao, boolean experiencia, WpCurriculos wpCurriculos) {
		super();
		this.empregador = empregador;
		this.cargoOcupado = cargoOcupado;
		this.dtInicio = dtInicio;
		this.dtFim = dtFim;
		this.notas = notas;
		this.descAtribuicao = desAtribuicao;
		this.experiencia = experiencia;
		this.wpCurriculosExperiencia = wpCurriculos;
	}

	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WpExperienciaProfissional))
			return false;
		return id != null && id.equals(((WpExperienciaProfissional) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
	
	public static List<ExperienciaProfissional> fromToModel(List<WpExperienciaProfissional> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new ExperienciaProfissional(wpFormacao.getId(), wpFormacao.getEmpregador(), wpFormacao.getCargoOcupado(), wpFormacao.getDtInicio(), wpFormacao.getDtFim(), wpFormacao.getNotas(), wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia(), wpFormacao.getIdProfissao()))
		        .collect(Collectors.toList());
	}
	
	public static List<WpExperienciaProfissional> fromToWpEntity(List<ExperienciaProfissional> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new WpExperienciaProfissional(wpFormacao.getId(), wpFormacao.getEmpregador(), wpFormacao.getNomeTrabalho(), wpFormacao.getDataInicio(), wpFormacao.getDataTermino(), wpFormacao.getNotas(), wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia(), wpFormacao.getProfissao(), null))
		        .collect(Collectors.toList());
	}

}