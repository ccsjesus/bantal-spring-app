package br.com.se.sis.bantal.services;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.exceptions.PlanNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.dto.WpPerfilDTO;
import br.com.se.sis.bantal.modelo.InscricaoVagaDTO;
import br.com.se.sis.bantal.modelo.Profissao;
import br.com.se.sis.bantal.modelo.PublicacaoVaga;
import br.com.se.sis.bantal.modelo.Vagas;
import br.com.se.sis.bantal.modelo.VagasModel;
import br.com.se.sis.bantal.modelo.WpAreaAtuacaoParametro;
import br.com.se.sis.bantal.modelo.WpCandidatoModel;
import br.com.se.sis.bantal.modelo.WpPlanoPagamento;
import br.com.se.sis.bantal.modelo.WpTipoVaga;
import br.com.se.sis.bantal.modelo.WpVagas;
import br.com.se.sis.bantal.repository.ProfissaoRepository;
import br.com.se.sis.bantal.repository.WpVagasRepository;
import br.com.se.sis.bantal.util.FuncaoSistema;
import br.com.se.sis.bantal.util.IExclusaoVaga;
import br.com.se.sis.bantal.util.IUtils;
import br.com.se.sis.bantal.util.StatusVaga;
import br.com.se.sis.bantal.util.TipoMensagem;
import br.com.se.sis.bantal.util.Util;
import br.com.se.sis.pagseguro.exception.TransacaoAbortadaException;

@Service
@Configurable
public class VagasService {


	@Autowired
	private DateService dataService;
	
	@Autowired
	private PlanoPagamentoService planosService;
	
	@Autowired
	private WpVagasRepository vagasRepo;
	
	@Autowired
	private TipoVagaServices tipoVagaServices;
	
	@Autowired
	private AreaAtuacaoParametroServices areaAtuacaoParametroServices;
	
	@Autowired
	private CurriculoServices curriculoServices;
	
	@Autowired
	private PerfilServices perfilServices;
	
	@Autowired
	private InscricaoVagaService inscricaoVagaService;
	
	@Autowired
	private MensagemService mensagemService;
	
	@Autowired
	private CandidatoService candidatoService;
	
	@Autowired
	private ControleVagaAvulsaService controleVagaAvulsaService;
	
	@Autowired
	private ProfissaoRepository profissaoRepository;
	
	
	public List<VagasModel> obterVagasInscritasCandidato(String idUsuarioLogado) {
		
		List<WpVagas> listaVagasCandidato = vagasRepo.obterVagasInscritasCandidato(idUsuarioLogado);
		
		List<VagasModel> retorno =  this.convertToVagaModelFromListCandidato(listaVagasCandidato, idUsuarioLogado);
		return retorno;
	}
	
	public WpVagas obterWpVagasPorid(String idVaga) {
		return vagasRepo.findById(idVaga).get();
	}
	
	
	
	public List<Vagas> obterTodasVagasPublicadasIncritasCandidato(String idUsuarioLogado){
		List<Vagas> listaVagas = WpVagas.fromToListVagas(vagasRepo.obterTodasVagasPorStatus(IUtils.APROVADA));
		
		List<Vagas> listaVagasCandidato = WpVagas.fromToListVagas(vagasRepo.obterVagasInscritasCandidato(idUsuarioLogado));
		
		listaVagas.stream().forEach(vaga -> {
			if(listaVagasCandidato.contains(vaga)) {
				vaga.setInscrito(true);
			} else {
				vaga.setInscrito(false);
			}
		});
		return listaVagas;
	}
	
	public List<Vagas> obterTodasVagasPublicadasVigentesIncritasCandidato(String idUsuarioLogado){
		List<Vagas> listaVagas = WpVagas.fromToListVagas(vagasRepo.obterTodasVagasVigentes());
		
		List<Vagas> listaVagasCandidato = WpVagas.fromToListVagas(vagasRepo.obterVagasInscritasCandidato(idUsuarioLogado));
		
		listaVagas.stream().forEach(vaga -> {
			if(listaVagasCandidato.contains(vaga)) {
				vaga.setInscrito(true);
			} else {
				vaga.setInscrito(false);
			}
		});
		return listaVagas;
	}
	
	public void realizarNovaInscricaoVaga(InscricaoVagaDTO inscricaoVaga, String idUsuarioLogado) {

		if (planosService.isPlanoUsuarioValido(idUsuarioLogado)) {
			throw new PlanNotFoundException(ErrorsMessages.PLAN_EXPIRED.getDescription(),ErrorsMessages.PLAN_EXPIRED);
		}

		inscricaoVaga.setCdUsuario(new BigDecimal(idUsuarioLogado));
		inscricaoVagaService.realizarInscricaoVaga(inscricaoVaga);
	}
	
	public void removerInscricaoVaga(String cdVaga, String idUsuarioLogado) {

		if (planosService.isPlanoUsuarioValido(idUsuarioLogado)) {
			throw new PlanNotFoundException(ErrorsMessages.PLAN_EXPIRED.getDescription(),ErrorsMessages.PLAN_EXPIRED);
		}

		inscricaoVagaService.removerInscricaoVaga(cdVaga, idUsuarioLogado);
	}

	public void publicarNovaVaga(PublicacaoVaga publicacaoParametro, String idUsuarioLogado) throws TransacaoAbortadaException, IOException {
		
			WpVagas vagas = new WpVagas();
			vagas.setUserId(idUsuarioLogado);
			vagas.setTitulo(publicacaoParametro.getTitulo());
			vagas.setLocalizacao(publicacaoParametro.getLocalizacao());
			
			WpTipoVaga tipoVaga = tipoVagaServices.obterWpTipoVagaPorId(publicacaoParametro.getIdTipoVaga()).get(0);
			WpAreaAtuacaoParametro area = areaAtuacaoParametroServices.obterAreaAtuacaoPorId(publicacaoParametro.getIdAreaAtuacao());
			Profissao profissao = profissaoRepository.recuperarProfissaoPorId(Long.valueOf(publicacaoParametro.getIdProfissao()));
					
			vagas.setWpTipoVaga(tipoVaga);
			vagas.setWpAreaAtuacaoParametro(area);
			vagas.setProfissao(profissao);
			vagas.setDescricao(publicacaoParametro.getPostContent());
			vagas.setDataLimite(publicacaoParametro.getDataExpiracao());
			vagas.setSalario(publicacaoParametro.getSalario());
			vagas.setStatus(IUtils.NOVA);
			if(publicacaoParametro.getFoto() != null) {
				vagas.setLogomarca(Util.compressBytes(publicacaoParametro.getFoto()));
			}
			vagas.setDataCriacao(dataService.now());
			vagasRepo.save(vagas);
			
			controleVagaAvulsaService.registrarVagaAvulsaPublicada(idUsuarioLogado);
	}
	
	public void aprovarVaga(String idVaga, String idUsuarioLogado) throws TransacaoAbortadaException, IOException {
		
		WpPerfilDTO perfil = perfilServices.obterPerfilUsuario(idUsuarioLogado);
		WpVagas vaga = vagasRepo.obterVagaPorId(idVaga);
		vaga.setStatus(StatusVaga.APROVADA);
		vagasRepo.save(vaga);

	}
	
	public void removerVaga(String idVaga, String idUsuarioLogado) throws TransacaoAbortadaException, IOException {
		
		WpVagas vaga = vagasRepo.obterVagaPorId(idVaga);
		vaga.setStatus(StatusVaga.EXCLUIDA);
		vaga.setExcluido(IExclusaoVaga.EXCLUIDO);
		vagasRepo.save(vaga);
		
		controleVagaAvulsaService.registrarVagaAvulsaPublicadaRejeitada(vaga.getUserId());
	}
	
	public void atualizarVaga(PublicacaoVaga publicacaoParametro, String idUsuarioLogado) throws IOException {
		WpVagas vagas = vagasRepo.obterVagasCadastradasPorIdUsuario(idUsuarioLogado).stream().filter(vaga -> vaga.getId().equals(publicacaoParametro.getIdVaga())).collect(Collectors.toList()).get(0);

		if(vagas != null) {
			vagas.setTitulo(publicacaoParametro.getTitulo());
			vagas.setLocalizacao(publicacaoParametro.getLocalizacao());
			
			WpTipoVaga tipoVaga = tipoVagaServices.obterWpTipoVagaPorId(publicacaoParametro.getIdTipoVaga()).get(0);
			WpAreaAtuacaoParametro area = areaAtuacaoParametroServices.obterAreaAtuacaoPorId(publicacaoParametro.getIdAreaAtuacao());
			Profissao profissao = profissaoRepository.recuperarProfissaoPorId(Long.valueOf(publicacaoParametro.getIdProfissao()));
			
			vagas.setWpTipoVaga(tipoVaga);
			vagas.setWpAreaAtuacaoParametro(area);
			vagas.setProfissao(profissao);
			vagas.setDescricao(publicacaoParametro.getPostContent());
			vagas.setDataLimite(publicacaoParametro.getDataExpiracao());
			vagas.setLogomarca(Util.compressBytes(publicacaoParametro.getFoto()));
			vagas.setSalario(publicacaoParametro.getSalario());
			vagas.setDataCriacao(dataService.now());
			
		}
	}
	
	public Object atualizarSituacaoVagaPorId(String idVaga, String statusVaga) {
		WpVagas vagas = vagasRepo.obterVagaPorId(idVaga);
		
		if(vagas != null) {
			vagas.setStatus(statusVaga);
		}
		return null;
	}

	public List<VagasModel> obterVagasDisponiveisporUsuarioLogado(String idLogado) {
		
		return vagasRepo.obterVagasCadastradasPorIdUsuario(idLogado).stream().map(wpVagas -> this.convertToVagaModel(wpVagas)).collect(Collectors.toList());
	}
	
	public VagasModel obterVagaPorId(String idVaga) {
		return this.convertToVagaModel(vagasRepo.obterVagaPorId(idVaga));
	}
	
	private VagasModel convertToVagaModel(WpVagas wpFor) {
		return this.convertToVagaModel(wpFor, null);
	}
	
	private VagasModel convertToVagaModel(WpVagas wpFor, String idUsuarioLogado) {
					
		
		VagasModel vagas = new VagasModel();
		
		vagas.setId(wpFor.getId());
		vagas.setUserId(wpFor.getUserId());
		vagas.setTitulo(wpFor.getTitulo());
		vagas.setDescricao(wpFor.getDescricao());
		vagas.setDataLimite(wpFor.getDataLimite());
		vagas.setLocalizacao(wpFor.getLocalizacao());
		vagas.setSalario(wpFor.getSalario());
		
		vagas.setTipoVagaModel(WpTipoVaga.fromToModel(tipoVagaServices.obterWpTipoVagaPorId(wpFor.getWpTipoVaga().getId())).get(0));
		vagas.setAreaAtuacaoModel(WpAreaAtuacaoParametro.fromToModel(Arrays.asList(areaAtuacaoParametroServices.obterAreaAtuacaoPorId(wpFor.getWpAreaAtuacaoParametro().getId()))).get(0));
		if (!Objects.isNull(wpFor.getProfissao()))
			vagas.setProfissao(profissaoRepository.recuperarProfissaoPorId(Long.valueOf(wpFor.getProfissao().getIdProfissao())));
		
		vagas.setFoto(Util.decompressBytes(wpFor.getLogomarca()));
		vagas.setStatus(wpFor.getStatus());
		
		vagas.setListCandidatoInscrito(wpFor.getListInscricaoVagas().stream()
				.map(curriculoModel -> curriculoServices.obterCurriculoModelUsuario(curriculoModel.getUserId()))
				.map(inscritos -> new WpCandidatoModel(Integer.parseInt(wpFor.getId()), inscritos.getIdentificacao(), inscritos.getNome(), inscritos.getUserId()))
				.collect(Collectors.toList()));
		
		if(idUsuarioLogado == null) {
			vagas.setMensagens(mensagemService.obterMsgEmpresa(wpFor.getId(), wpFor.getUserId(), TipoMensagem.MENSAGEM_NAO_LIDA));
		}  else {
			vagas.setMensagens(mensagemService.obterMsgEmpresa(wpFor.getId(), idUsuarioLogado, TipoMensagem.MENSAGEM_NAO_LIDA));
			
		}
		vagas.setNomeEmpresa(perfilServices.obterPerfilUsuario(wpFor.getUserId()).getNome());
		
		return vagas;
			
	}
	
	private List<VagasModel> convertToVagaModelFromListCandidato(List<WpVagas> listWpVaga, String idCandidato) {
		
		List<VagasModel> listaVagaModel = new ArrayList<>();
		
		for(WpVagas wpFor : listWpVaga) {
			listaVagaModel.add(this.convertToVagaModel(wpFor, idCandidato));
			
		}
					
		return listaVagaModel;
	}
	
	public List<VagasModel> obterTodasVagas() {
		return vagasRepo.obterTodasVagas().stream().map(wpVagas -> this.convertToVagaModel(wpVagas)).collect(Collectors.toList());
		
	}
	


}
