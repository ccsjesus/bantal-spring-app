package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The persistent class for the wp_posts database table.
 * 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "wp_tipo_vaga")
public class WpTipoVaga implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private String id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "valor")
	private String valor;
	
	@Column(name = "habilitada")
	private int habilitada;
	
	@Transient
	@OneToOne(mappedBy = "wpTipoVaga", cascade = CascadeType.ALL,fetch = FetchType.LAZY, optional = false)
	private WpVagas vagas;


	public static List<TipoVagaModel> fromToModel(List<WpTipoVaga> wpFor) {
		return wpFor.stream()
		        .map(arg -> new TipoVagaModel(arg.getId(), arg.getNome(), arg.getValor(), arg.getHabilitada()))
		        .collect(Collectors.toList());
	}
	
	public static List<WpTipoVaga> fromToWpEntity(List<TipoVagaModel> wpFor) {
		return wpFor.stream()
		        .map(arg -> new WpTipoVaga(arg.getId(), arg.getNome(), arg.getValor(), arg.getHabilitada(), null))
		        .collect(Collectors.toList());
	}


}