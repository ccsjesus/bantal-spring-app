package br.com.se.sis.bantal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.Profissao;
import br.com.se.sis.bantal.modelo.ProfissaoDTO;
import br.com.se.sis.bantal.repository.ProfissaoRepository;


@Service
@Configurable
public class FuncaoCargoServices {
	
	@Autowired
	private ProfissaoRepository profissaoRepository;
	
	
	public List<Profissao> recuperarProfissoes() {
		List<Profissao> listaProfissoes = profissaoRepository.recuperarProfissoes();
		
		return listaProfissoes;
	}
	
	public List<ProfissaoDTO> recuperarProfissoesPorIdAreaAtuacao(String idAreaAtuacao) {
		List<ProfissaoDTO> listaProfissoes = profissaoRepository.recuperarProfissoesPorIdAreaAtuacao(Long.valueOf(idAreaAtuacao));
		
		return listaProfissoes;
	}

}
