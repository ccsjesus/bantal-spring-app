package br.com.se.sis.bantal.modelo;

import lombok.Data;

@Data
public class ProfissaoDTO {

	private Long idProfissao;
	private String nome;
	private Long idAreaAtuacao;
	private boolean habilitada;
	private Long quantidade;

	public ProfissaoDTO(Long idProfissao, String nome, Long idAreaAtuacao, boolean habilitada, Long quantidade) {
		this.idProfissao = idProfissao;
		this.nome = nome;
		this.idAreaAtuacao = idAreaAtuacao;
		this.habilitada = habilitada;
		this.quantidade = quantidade;
	}

}