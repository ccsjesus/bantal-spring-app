package br.com.se.sis.bantal.modelo;

import java.util.ArrayList;
import java.util.List;

import br.com.se.sis.bantal.util.Util;
import lombok.Data;

@Data
public class CurriculoModel {
	
	private String userId;
	
	private String nome;
	
	private String apresentacao;
	
	private String objetivoProfissional;
	
	private String nacionalidade;
	
	private String estadoCivil;
	
	private String filhos;
	
	private String idade;
	
	private String dataNascimento;
	
	private String endereco;
	
	private String telefone;
	
	private String celular;
	
	private String telefoneRecados;
	
	private String email;
	
	private String linkedin;
	
	private List<CNHModel> cnh;
	
	private String identificacao;
	
	private String rg;

	private List<AreaAtuacaoModel> areaAtuacao;
	
	private String funcaoCargo;
	
	private String nivel;
	
	private String pretensaoSalarial;
	
	private String qualificacao;
	
	private String formacao;
	
	private byte[] foto;
	
	private List<FormacaoAcademica> formacaoAcademica;
	
	private List<ExperienciaProfissional> experienciaProfissional;
	
	private List<CursoModel> cursos;
	
	private List<IdiomaModel> idiomas;
	
	private List<InformaticaModel> informatica;

	public CurriculoModel() {
		// TODO Auto-generated constructor stub
	}
	

	public static CurriculoModel montarCurriculoFromWpCurriculo(WpCurriculos wpCurriculo) {
		CurriculoModel cm = new CurriculoModel();
		
		if(wpCurriculo != null) {
			cm.setNome(wpCurriculo.getNome() == null ? "" : wpCurriculo.getNome());
			cm.setUserId(wpCurriculo.getUserId() == null ? "" : wpCurriculo.getUserId());
			cm.setNacionalidade(wpCurriculo.getNacionalidade() == null ? "" : wpCurriculo.getNacionalidade());
			cm.setEstadoCivil(wpCurriculo.getEstadoCivil() == null ? "" : wpCurriculo.getEstadoCivil());
			cm.setFilhos(wpCurriculo.getFilhos() == null || "".equals(wpCurriculo.getFilhos()) ? "não" : wpCurriculo.getFilhos());
			cm.setEndereco(wpCurriculo.getEndereco()== null ? "" : wpCurriculo.getEndereco());
			cm.setCelular(wpCurriculo.getCelular()== null ? "" : wpCurriculo.getCelular());
			cm.setTelefone(wpCurriculo.getTelefone() == null ? "" : wpCurriculo.getTelefone());
			cm.setTelefoneRecados(wpCurriculo.getRecados() == null ? "" : wpCurriculo.getRecados());
			cm.setEmail(wpCurriculo.getEmail() == null ? "" : wpCurriculo.getEmail());
			cm.setObjetivoProfissional(wpCurriculo.getObjetivoProfissional() == null ? "" : wpCurriculo.getObjetivoProfissional());
			cm.setApresentacao(wpCurriculo.getApresentacao() == null ? "" : wpCurriculo.getApresentacao());
			cm.setLinkedin(wpCurriculo.getLinkedin() == null ? "" : wpCurriculo.getLinkedin());
			cm.setIdentificacao(wpCurriculo.getIdentificacao() == null ? "" : wpCurriculo.getIdentificacao());
			cm.setRg(wpCurriculo.getRg() == null ? "" : wpCurriculo.getRg());
			cm.setPretensaoSalarial(wpCurriculo.getPretensaoSalarial() == null ? "" : wpCurriculo.getPretensaoSalarial());
			cm.setFuncaoCargo(wpCurriculo.getFuncaoCargo() == null ? "" : wpCurriculo.getFuncaoCargo());
			cm.setNivel(wpCurriculo.getNivelCargo() == null ? "" : wpCurriculo.getNivelCargo());
			cm.setFoto(wpCurriculo.getFoto());
			cm.setFormacao(wpCurriculo.getFormacao() == null ? "" : wpCurriculo.getFormacao());
			cm.setCnh(montarListaCNH(wpCurriculo.getCnh() == null ? "" : wpCurriculo.getCnh()));
			cm.setFormacaoAcademica(montarFormacaoAcademica(wpCurriculo.getListFormacaoAcademica()));
			cm.setInformatica(montarInformatica(wpCurriculo.getListTecnologias()));
			cm.setIdiomas(montarIdiomas(wpCurriculo.getListIdiomas()));
		    cm.setExperienciaProfissional(montarExperienciaProfissional(wpCurriculo.getListExperienciaProfissional()));
		    cm.setAreaAtuacao(montarAreaAtuacao(wpCurriculo.getListAreaAtuacao()));
		    cm.setIdade(wpCurriculo.getIdade() == null ? "não" :  wpCurriculo.getIdade());
//		cm.setIdade(wpCurriculo.getId() == null || "".equals(wpCurriculo.getMetaValue().split(" ")[0]) ? 0 : Integer.valueOf(wpCurriculo.getMetaValue().split(" ")[0]));
			
		} else {
			cm.setNome("");
			cm.setNacionalidade("");
			cm.setEstadoCivil("");
			cm.setFilhos("");
			cm.setEndereco("");
			cm.setCelular("");
			cm.setTelefone("");
			cm.setTelefoneRecados("");
			cm.setEmail("");
			cm.setObjetivoProfissional("");
			cm.setApresentacao("");
			cm.setLinkedin("");
			cm.setIdentificacao("");
			cm.setRg("");
			cm.setPretensaoSalarial("");
			cm.setFuncaoCargo("");
			cm.setNivel("");
			cm.setFormacao("");
		}
		
		return cm;
	}
	
	private static List<CNHModel> montarListaCNH(String value) {
		String[] valor = Util.splitEspecial(value);
		
		List<CNHModel> arg1 = new ArrayList<CNHModel>();
		CNHModel cnh = null;
		
		for(String str : valor) {
			cnh =new CNHModel(str, str, true);
			arg1.add(cnh);
		}
		return arg1;
	}
	
	private static List<IdiomaModel> montarIdiomas(List<WpIdioma> listFormacaoIdioma) {
		List<IdiomaModel> lista =  new ArrayList<IdiomaModel>();
		IdiomaModel cursoModel = null;
		for(WpIdioma idioma: listFormacaoIdioma) {
				cursoModel = new IdiomaModel();
				cursoModel.setId(idioma.getId());
				cursoModel.setIdioma(idioma.getIdioma());
				cursoModel.setLeitura(idioma.getNivelLeitura());
				cursoModel.setEscrita(idioma.getNivelEscrita());
				cursoModel.setConversacao(idioma.getNivelConversacao());
				lista.add(cursoModel);					
		}
		
		return lista;
	}
	
	private static List<InformaticaModel>  montarInformatica(List<WpTecnologias> listFormacaoTecnologia) {
		List<InformaticaModel> lista =  new ArrayList<InformaticaModel>();
		InformaticaModel cursoModel = null;
			for(WpTecnologias tecnologia : listFormacaoTecnologia) {
				cursoModel = new InformaticaModel();
				cursoModel.setId(tecnologia.getId());
				cursoModel.setTecnologia(tecnologia.getTecnologia());
				cursoModel.setNivel(tecnologia.getNivel());
				cursoModel.setExperiencia(tecnologia.isExperiencia());
				cursoModel.setDescAtribuicao(tecnologia.getDescAtribuicao());
				lista.add(cursoModel);					
			}
		
		return lista;
	}
	
	private static List<FormacaoAcademica> montarFormacaoAcademica(List<WpFormacao> listFormacaoAcademica) {

		List<FormacaoAcademica> lista =  new ArrayList<FormacaoAcademica>();
		FormacaoAcademica forAcademica = null;
		
		for(WpFormacao formacao : listFormacaoAcademica) {
			forAcademica = new FormacaoAcademica();
			forAcademica.setId(formacao.getId());
			forAcademica.setUniversidade(formacao.getUniversidade());
			forAcademica.setQualificacao(formacao.getQualificacao());
			forAcademica.setDataInicio(formacao.getDtInicio());
			forAcademica.setDataFim(formacao.getDtTermino());
			forAcademica.setNota(formacao.getNota());
			lista.add(forAcademica);					
		}
		
		return lista;
	}
	
	private static List<ExperienciaProfissional> montarExperienciaProfissional(List<WpExperienciaProfissional> listExperienciaProfissional) {
		List<ExperienciaProfissional> lista =  new ArrayList<ExperienciaProfissional>();
		
		ExperienciaProfissional xpProfissional = null;
			for(WpExperienciaProfissional experiencia : listExperienciaProfissional) {
				xpProfissional = new ExperienciaProfissional();
				xpProfissional.setId(experiencia.getId());
				xpProfissional.setEmpregador(experiencia.getEmpregador());
				xpProfissional.setNomeTrabalho(experiencia.getCargoOcupado());
				xpProfissional.setDataInicio(experiencia.getDtInicio());
				xpProfissional.setDataTermino(experiencia.getDtFim());
				xpProfissional.setNotas(experiencia.getNotas());
				xpProfissional.setDescAtribuicao(experiencia.getDescAtribuicao());
				xpProfissional.setExperiencia(experiencia.isExperiencia());
				xpProfissional.setProfissao(experiencia.getIdProfissao());
				lista.add(xpProfissional);
			}
		
		return lista;
	}
	
	private static List<AreaAtuacaoModel> montarAreaAtuacao(List<WpAreaAtuacao> listAreaAtuacao) {
		List<AreaAtuacaoModel> lista =  new ArrayList<AreaAtuacaoModel>();
		
		AreaAtuacaoModel areaAtuacaoModel = null;
			for(WpAreaAtuacao atuacao : listAreaAtuacao) {
				areaAtuacaoModel = new AreaAtuacaoModel();
				areaAtuacaoModel.setTermId(atuacao.getWpAreaAtuacaoParametro().getId());
//				areaAtuacaoModel.setName(atuacao.getNome());
//				areaAtuacaoModel.setSlug(atuacao.getValor());
				lista.add(areaAtuacaoModel);
			}
		
		return lista;
	}
	
	
	
}
