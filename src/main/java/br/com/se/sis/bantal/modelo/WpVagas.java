package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.se.sis.bantal.services.AreaAtuacaoParametroServices;
import br.com.se.sis.bantal.services.DateService;
import br.com.se.sis.bantal.services.TipoVagaServices;
import br.com.se.sis.bantal.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_vagas")
public class WpVagas implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private String id;
	
	@Column(name="user_id")
	private String userId;
	
	@Column(name="nome_empresa")
	private String nomeEmpresa;

	@Column(name="titulo")
	private String titulo;

	@Column(name="localizacao")
	private String localizacao;

	@Column(name="descricao")
	private String descricao;

	@Column(name="data_limite")
	private String dataLimite;

	@Column(name="status")
	private String status;
	
	@Column(name="salario")
	private String salario;
	
	@Column(name="logomarca")
	private byte[] logomarca;
	
	@Column(name="data_criacao")
	@Basic
	private LocalDateTime dataCriacao;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_area_atuacao")
	private WpAreaAtuacaoParametro wpAreaAtuacaoParametro;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_vaga")
	private WpTipoVaga wpTipoVaga;
	
	@OneToMany(mappedBy="wpVagas", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpInscricaoVaga> listInscricaoVagas;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_profissao")
	private Profissao profissao;
	
	@Transient
	@Autowired
	private DateService dateService;
	
	@Transient
	@Autowired
	private static TipoVagaServices tipoVagaServices;
	
	@Transient
	@Autowired
	private static AreaAtuacaoParametroServices areaAtuacaoParametroServices;
	
	@Column(name="confidencial")
	private int confidencial;
	
	@Column(name="excluido")
	private int excluido;

	public WpVagas(String userId, String titulo, String localizacao, String descricao, String dataLimite,
			String status, byte[] logomarca, int confidencial, int excluido) {
		super();
		this.userId = userId;
		this.titulo = titulo;
		this.localizacao = localizacao;
		this.descricao = descricao;
		this.dataLimite = dataLimite;
		this.status = status;
		this.logomarca = logomarca;
		this.confidencial = confidencial;
		this.excluido = excluido;
	}
	
	public LocalDateTime getDataCriacao() {
		return dateService.now();
	}
	
	public static List<Vagas> fromToListVagas(List<WpVagas> arg) {
		return arg.stream()
		        .map(vaga -> new Vagas(vaga.getId(), vaga.getTitulo(), vaga.getLocalizacao(), Integer.parseInt(vaga.getWpTipoVaga().getId()), vaga.getWpTipoVaga().getNome(), vaga.getWpAreaAtuacaoParametro().getNome(),vaga.getDescricao(), "email", vaga.getDataLimite(), "vagas", "quantidade_vagas", "total_inscrito", "usuario", vaga.getStatus(), vaga.getSalario(), Integer.parseInt(vaga.getWpAreaAtuacaoParametro().getId()), vaga.getWpAreaAtuacaoParametro().getNome(), vaga.getWpAreaAtuacaoParametro().getValor(), "Estado", "Cidade", Util.decompressBytes(vaga.getLogomarca()), false))
		        .collect(Collectors.toList());
	}
	
	public static List<PublicacaoVaga> fromToModel(List<WpVagas> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new PublicacaoVaga(wpFormacao.getId(), wpFormacao.getDescricao(), wpFormacao.getTitulo(), wpFormacao.getLocalizacao(), wpFormacao.getSalario(), " CNPJ ",wpFormacao.getWpTipoVaga().getId(), wpFormacao.getWpAreaAtuacaoParametro().getId(), wpFormacao.getDataLimite(), wpFormacao.getWpTipoVaga().getNome(), null, wpFormacao.getStatus(), wpFormacao.getProfissao().getIdProfissao().toString()))
		        .collect(Collectors.toList());
	}
	
	public static List<WpVagas> fromToWpEntity(List<VagasModel> wpFor) {
		WpVagas vagas = null;
		List<WpVagas> listaVagas = new ArrayList<WpVagas>();
		for(VagasModel pub : wpFor) {
					
			vagas = new WpVagas();
			vagas.setId(pub.getId());
			vagas.setTitulo(pub.getTitulo());
			vagas.setDescricao(pub.getDescricao());
			vagas.setDataLimite(pub.getDataLimite());
			vagas.setLocalizacao(pub.getLocalizacao());
			vagas.setSalario(pub.getSalario());
			vagas.setLogomarca(pub.getFoto());
			vagas.setStatus(pub.getStatus());
			vagas.setConfidencial(pub.getConfidencial());
			vagas.setExcluido(pub.getExcluido());

			listaVagas.add(vagas);
		}
		return listaVagas;
	}
	
}