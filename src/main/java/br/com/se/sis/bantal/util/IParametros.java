package br.com.se.sis.bantal.util;

public @interface IParametros {

	String FORMACAO = "FORMACAO";

	String CNH = "CNH";

	String ESTADO_CIVIL = "ESTADO_CIVIL";

	String INFORMATICA = "INFORMATICA";

	String AREA_ATUACAO = "AREA_ATUACAO";

	String FUNCAO_CARGO = "FUNCAO_CARGO";

	String NIVEL_CARGO = "NIVEL_CARGO";

	String PRETENSAO_SALARIAL = "PRETENSAO_SALARIAL";

	String IDIOMA = "IDIOMA";

}
