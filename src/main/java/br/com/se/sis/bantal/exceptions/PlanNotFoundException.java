package br.com.se.sis.bantal.exceptions;

import br.com.se.sis.bantal.enums.ErrorsMessages;

public class PlanNotFoundException extends RuntimeException {

    private ErrorsMessages code;

    public PlanNotFoundException(String message, ErrorsMessages code) {
        super(message);
        this.code = code;
    }
}
