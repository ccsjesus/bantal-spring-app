package br.com.se.sis.bantal.dto;

import java.util.List;

import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.modelo.InscricaoVagaDTO;
import br.com.se.sis.bantal.modelo.VagasModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PainelAdministradorDTO {
	
	private List<WpPerfilDTO> listaPerfil;
	
	private List<CurriculoModel> listaCurriculos;
	
	private List<InscricaoVagaDTO> listaInscricaoVagas;
	
	private List<VagasModel> listaVagas;

}
