package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * The persistent class for the wp_usermeta database table.
 * 
 */
@Entity
@Table(name = "wp_usermeta")
public class WpUsermeta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "ID")
	private BigDecimal id;

	@Column(name = "meta_key")
	private String metaKey;

	@Lob
	@Column(name = "meta_value")
	private String metaValue;

	@Id
	@Column(name = "umeta_id")
	private BigInteger umetaId;

	// bi-directional many-to-one association to WpUser
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "ID")
    @JsonBackReference
	private WpUser wpUser;

	public WpUsermeta() {
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getMetaKey() {
		return this.metaKey;
	}

	public void setMetaKey(String metaKey) {
		this.metaKey = metaKey;
	}

	public String getMetaValue() {
		return this.metaValue;
	}

	public void setMetaValue(String metaValue) {
		this.metaValue = metaValue;
	}

	public BigInteger getUmetaId() {
		return this.umetaId;
	}

	public void setUmetaId(BigInteger umetaId) {
		this.umetaId = umetaId;
	}

	public WpUser getWpUser() {
		return this.wpUser;
	}

	public void setWpUser(WpUser wpUser) {
		this.wpUser = wpUser;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WpUsermeta))
			return false;
		return id != null && id.equals(((WpUsermeta) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}

}