package br.com.se.sis.bantal.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.AreaAtuacaoDTO;
import br.com.se.sis.bantal.modelo.AreaAtuacaoModel;
import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.modelo.WpAreaAtuacao;
import br.com.se.sis.bantal.modelo.WpAreaAtuacaoParametro;
import br.com.se.sis.bantal.modelo.WpCurriculos;
import br.com.se.sis.bantal.repository.AreaAtuacaoParametroRepository;
import br.com.se.sis.bantal.repository.AreaAtuacaoRepository;
import br.com.se.sis.bantal.util.ITipoVaga;


@Service
@Configurable
public class AreaAtuacaoServices {
	
	@Autowired
	private AreaAtuacaoRepository areaAtuacaoRepository;
	
	@Autowired
	private AreaAtuacaoParametroRepository areaAtuacaoParametroRepository;
	
	@Autowired
	private PerfilServices perfilServices;
	
	
	public List<AreaAtuacaoModel> obterAreaAtuacaoIdUsuaarioLogado(String id) {
		return areaAtuacaoParametroRepository.obterAreaAtuacaoParametroByIdLogado(id)
				.stream()
				//.map(area -> area.getWpAreaAtuacaoParametro())
				.map(c -> new AreaAtuacaoModel(c.getId(), c.getNome(), c.getValor(), c.getHabilitada()))
				.collect(Collectors.toList());
	};
	
	public List<AreaAtuacaoModel> obterTodasAreasAtuacao() {
		return areaAtuacaoParametroRepository.obterTodasAreasAtuacao()
				.stream()
				//.map(area -> area.getWpAreaAtuacaoParametro())
				.map(c -> new AreaAtuacaoModel(c.getId(), c.getNome(), c.getValor(), c.getHabilitada()))
				.collect(Collectors.toList());
	}
	
	public List<AreaAtuacaoDTO> obterTodasAreasAtuacaoAgrupadaPorCurriculo() {
		return areaAtuacaoParametroRepository.obterTodasAreasAtuacaoAgrupadaPorCurriculo();
	}
	
	public List<AreaAtuacaoModel> obterTodosTipoVagaHabilitadas() {
		return areaAtuacaoParametroRepository.obterTodasAreasAtuacaoHabilitadas().stream()
				//.map(area -> area.getWpAreaAtuacaoParametro())
				.map(c -> new AreaAtuacaoModel(c.getId(), c.getNome(), c.getValor(), c.getHabilitada()))
				.collect(Collectors.toList());
		
	}
	
	public List<AreaAtuacaoModel> obterTodosTipoVagaDesabilitadas() {
		return areaAtuacaoParametroRepository.obterTodasAreasAtuacaoDesabilitadas().stream()
				//.map(area -> area.getWpAreaAtuacaoParametro())
				.map(c -> new AreaAtuacaoModel(c.getId(), c.getNome(), c.getValor(), c.getHabilitada()))
				.collect(Collectors.toList());
	}
	
	
	public WpAreaAtuacao obterAreaAtuacaoPorId(String id) {
		return areaAtuacaoRepository.obterAreaAtuacaoPorId(id);
	}
	
	public void atualizarAreaAtuacao(CurriculoModel curriculoModel, WpCurriculos wpCurriculos) {
			
			List<WpAreaAtuacao> wpFor = areaAtuacaoRepository.obterAreaAtuacaoByIdLogado(wpCurriculos.getId());
			List<AreaAtuacaoModel> listFormUsuarioConsulta = wpFor.stream()
																.map(area -> area.getWpAreaAtuacaoParametro())
																.map(c -> new AreaAtuacaoModel(c.getId(), c.getNome(), c.getValor(), c.getHabilitada()))
																.collect(Collectors.toList());
			
			List<AreaAtuacaoModel> listFormUsuario = curriculoModel.getAreaAtuacao();
			int tam = curriculoModel.getAreaAtuacao().size();
	
	
			if (tam < wpFor.size()) {
	
				listFormUsuarioConsulta.stream().filter(a -> !listFormUsuario.contains(a)).map(b -> b.getTermId()).collect(Collectors.toList()).forEach(c -> areaAtuacaoRepository.removerAreaAtuacaoById(c));
				
			} else if (tam > wpFor.size()) {
				
				listFormUsuario.stream().filter(a -> !listFormUsuarioConsulta.contains(a)).map(wpFormacao -> areaAtuacaoParametroRepository.findById(wpFormacao.getTermId()).get())
				.map(area -> new WpAreaAtuacao(area, wpCurriculos))
				.collect(Collectors.toList()).forEach(c -> areaAtuacaoRepository.inserirAreaAtuacao(c));
				
			}
			
		}
	
	public void incluirNovoTipoVaga(String idUsuarioLogado, AreaAtuacaoModel modelParam) {

		WpAreaAtuacaoParametro model = new WpAreaAtuacaoParametro();
		model.setNome(modelParam.getName());
		model.setValor(modelParam.getSlug());
		model.setHabilitada(ITipoVaga.NAO_HABILITADO);
		areaAtuacaoParametroRepository.save(model);
		
	}
	
	public void excluirNovoTipoVaga(String idUsuarioLogado, String id) {
		
		WpAreaAtuacaoParametro model = areaAtuacaoParametroRepository.obterAreaAtuacaoParametroById(id);
		model.setHabilitada(ITipoVaga.NAO_HABILITADO);

		areaAtuacaoParametroRepository.save(model);

	}
	
	public void habilitarNovoTipoVaga(String idUsuarioLogado, String id) {
		
		WpAreaAtuacaoParametro model = areaAtuacaoParametroRepository.obterAreaAtuacaoParametroById(id);
		model.setHabilitada(ITipoVaga.HABILITADO);
		
		areaAtuacaoParametroRepository.save(model);

	}
}
