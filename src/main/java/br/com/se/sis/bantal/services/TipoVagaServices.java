package br.com.se.sis.bantal.services;

import br.com.se.sis.bantal.modelo.TipoVagaModel;
import br.com.se.sis.bantal.modelo.WpTipoVaga;
import br.com.se.sis.bantal.repository.TipoVagaRepository;
import br.com.se.sis.bantal.util.ITipoVaga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;


@Service
@Configurable
public class TipoVagaServices {
	
	@Autowired
	private TipoVagaRepository tipoVagaRepository;
	
	@Autowired
	private PerfilServices perfilServices;
	
	
	public List<TipoVagaModel> obterTipoVagaPorId(String id) {
		List<WpTipoVaga> wpParametros = Arrays.asList(tipoVagaRepository.obterTipoVagaPorId(id));
		
		return WpTipoVaga.fromToModel(wpParametros);
		
	}
	
	public List<WpTipoVaga> obterWpTipoVagaPorId(String id) {
		List<WpTipoVaga> wpParametros = Arrays.asList(tipoVagaRepository.obterTipoVagaPorId(id));
		
		return wpParametros;
		
	}
	
	public List<TipoVagaModel> obterTodosTipoVaga() {
		List<WpTipoVaga> wpParametros = tipoVagaRepository.findAll();
		
		return WpTipoVaga.fromToModel(wpParametros);
		
	}
	
	public List<TipoVagaModel> obterTodosTipoVagaHabilitadas() {
		List<WpTipoVaga> wpParametros = tipoVagaRepository.obterTodosTiposVagaHabilitadas();
		
		return WpTipoVaga.fromToModel(wpParametros);
		
	}
	
	public List<TipoVagaModel> obterTodosTipoVagaDesabilitadas() {
		List<WpTipoVaga> wpParametros = tipoVagaRepository.obterTodosTiposVagaDesabilitadas();
		
		return WpTipoVaga.fromToModel(wpParametros);
		
	}
	
	public void incluirNovoTipoVaga(String idUsuarioLogado, TipoVagaModel tipoVagaModel) {
			WpTipoVaga tipoVaga = new WpTipoVaga();
			tipoVaga.setNome(tipoVagaModel.getNome());
			tipoVaga.setValor(tipoVagaModel.getValor());
			tipoVaga.setHabilitada(ITipoVaga.NAO_HABILITADO);
			tipoVagaRepository.save(tipoVaga);
	}
	
	public void excluirNovoTipoVaga(String idUsuarioLogado, String id) {
		
		WpTipoVaga tipoVaga = tipoVagaRepository.obterTipoVagaPorId(id);
		tipoVaga.setHabilitada(ITipoVaga.NAO_HABILITADO);

		tipoVagaRepository.save(tipoVaga);

	}
	
	public void habilitarNovoTipoVaga(String idUsuarioLogado, String id) {
		
		WpTipoVaga tipoVaga = tipoVagaRepository.obterTipoVagaPorId(id);
		tipoVaga.setHabilitada(ITipoVaga.HABILITADO);
		
		tipoVagaRepository.save(tipoVaga);
	}

}
