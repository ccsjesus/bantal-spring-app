package br.com.se.sis.bantal.exceptions;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import lombok.Data;

@Data
public class TokenExpiredException extends RuntimeException {

    private ErrorsMessages code;

    public TokenExpiredException(String message, ErrorsMessages code) {
        super(message);
        this.code = code;
    }
}
