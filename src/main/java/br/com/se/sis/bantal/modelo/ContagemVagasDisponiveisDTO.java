package br.com.se.sis.bantal.modelo;

import lombok.Data;
import org.apache.bcel.generic.StackInstruction;

@Data
public class ContagemVagasDisponiveisDTO {

	private String userId;
	private String nomeEmpresa;
	private String objetivo;
	private String sigla;
	private Long quantidade;
	private byte[] foto;



	public ContagemVagasDisponiveisDTO(String userId, String nomeEmpresa,String objetivo, String sigla, Long quantidade, byte[] foto
	) {
		this.userId = userId;
		this.nomeEmpresa = nomeEmpresa;
		this.objetivo=objetivo;
		this.sigla=sigla;
		this.quantidade = quantidade;
		this.foto = foto;

	}

}
