package br.com.se.sis.bantal.enums;

public enum UserStatus {
    ATIVO,
    INATIVO
}
