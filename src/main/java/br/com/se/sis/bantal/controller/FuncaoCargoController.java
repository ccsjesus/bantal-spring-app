package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.UserEmployerAndCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.FuncaoCargoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/v1/funcao-cargo")
public class FuncaoCargoController {


	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private FuncaoCargoServices funcaoCargoServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserEmployerAndCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> recuperarProfissoes() throws IllegalArgumentException{

		return ResponseEntity.ok(funcaoCargoServices.recuperarProfissoes());
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/area-atuacao/{idAreaAtuacao}")
	@UserEmployerAndCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> recuperarProfissoesPorIdAreaAtuacao(@PathVariable("idAreaAtuacao") String idAreaAtuacao) throws IllegalArgumentException{

		return ResponseEntity.ok(funcaoCargoServices.recuperarProfissoesPorIdAreaAtuacao(idAreaAtuacao));
	}
	
}
