package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * The persistent class for the wp_posts database table.
 * 
 */
public class UserMeta implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal userId;

	private String metaKey;

	private String metaValue;

	public UserMeta(BigDecimal userId, String metaKey, String metaValue) {
		super();
		this.userId = userId;
		this.metaKey = metaKey;
		this.metaValue = metaValue;
	}

	public BigDecimal getUserId() {
		return userId;
	}

	public void setUserId(BigDecimal userId) {
		this.userId = userId;
	}

	public String getMetaKey() {
		return metaKey;
	}

	public void setMetaKey(String metaKey) {
		this.metaKey = metaKey;
	}

	public String getMetaValue() {
		return metaValue;
	}

	public void setMetaValue(String metaValue) {
		this.metaValue = metaValue;
	}

}