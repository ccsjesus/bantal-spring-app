package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TipoVagaModel {
	
	private String id;
	
	private String nome;
	
	private String valor;
	
	private int habilitada;
	
	@Override
	public int hashCode() {
		return 31;
	}
		
	@Override
	public boolean equals (Object object) {
	    boolean result = false;
	    if (object == null || object.getClass() != getClass()) {
	        result = false;
	    } else {
	        TipoVagaModel employee = (TipoVagaModel) object;
	        if (this.id.equals(employee.getNome())) {
	            result = true;
	        }
	    }
	    return result;
	}
	

}
