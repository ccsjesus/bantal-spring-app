package br.com.se.sis.bantal.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.modelo.ExperienciaProfissional;
import br.com.se.sis.bantal.modelo.WpCurriculos;
import br.com.se.sis.bantal.modelo.WpExperienciaProfissional;
import br.com.se.sis.bantal.repository.ExperienciaProfissionalRepository;
import br.com.se.sis.bantal.util.iExperiencia;


@Service
@Configurable
public class ExperienciaProfissionalServices {
	
	private final String ID_INICIAL = "99999";
	
	@Autowired
	private ExperienciaProfissionalRepository experienciaProfissionalRepository;
	
	public void adicionarSemExperiencia(CurriculoModel curriculoModel, WpCurriculos wpCurriculos) {
	
		List<WpExperienciaProfissional> wpXpProfissional = experienciaProfissionalRepository.obterExperienciaProfissionalByIdLogado(wpCurriculos.getId());
		List<ExperienciaProfissional> xpProfissionalCandidato = curriculoModel.getExperienciaProfissional();
		
		if(xpProfissionalCandidato.size() > 1) {
			wpXpProfissional.stream().forEach(experiencia -> experienciaProfissionalRepository.removerXpProfissionalById(experiencia.getId()));
			
			xpProfissionalCandidato.stream().filter(a -> a.isExperiencia()).map(wpFormacao -> new WpExperienciaProfissional(wpFormacao.getEmpregador(), wpFormacao.getNomeTrabalho(), wpFormacao.getDataInicio(), wpFormacao.getDataTermino(), wpFormacao.getNotas(), wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia(), wpCurriculos))
			.collect(Collectors.toList()).forEach(c -> experienciaProfissionalRepository.inserirXpProfissional(c));
		}
	}
	
	public void atualizarSemExperiencia(CurriculoModel curriculoModel, WpCurriculos wpCurriculos) {
		
		List<ExperienciaProfissional> xpProfissionalCandidato = curriculoModel.getExperienciaProfissional();
		
		if(xpProfissionalCandidato.size() == 1) {
			xpProfissionalCandidato.stream()
			.map(wpFormacao -> new WpExperienciaProfissional(wpFormacao.getId(), wpFormacao.getEmpregador(), wpFormacao.getNomeTrabalho(), wpFormacao.getDataInicio(), wpFormacao.getDataTermino(), wpFormacao.getNotas(), wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia(), wpFormacao.getProfissao(), wpCurriculos))
			.forEach(formacao -> {
				if(formacao.getId() != null && formacao.isExperiencia()) {
					experienciaProfissionalRepository.removerXpProfissionalById(formacao.getId());
				} 
			} );
			
			xpProfissionalCandidato.stream().filter(a -> a.isExperiencia()).map(wpFormacao -> new WpExperienciaProfissional(wpFormacao.getEmpregador(), wpFormacao.getNomeTrabalho(), wpFormacao.getDataInicio(), wpFormacao.getDataTermino(), wpFormacao.getNotas(), wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia(), wpCurriculos))
			.collect(Collectors.toList()).forEach(c -> experienciaProfissionalRepository.inserirXpProfissional(c));
		}
	}
	
	public void atualizarExperienciaProfissional(CurriculoModel curriculoModel, WpCurriculos wpCurriculos) {
		
		List<WpExperienciaProfissional> wpFor = experienciaProfissionalRepository.obterExperienciaProfissionalByIdLogado(wpCurriculos.getId());
		List<ExperienciaProfissional> listFormUsuarioConsulta = WpExperienciaProfissional.fromToModel(wpFor);
		List<ExperienciaProfissional> listFormUsuario = curriculoModel.getExperienciaProfissional();
		int tamanhoLista = curriculoModel.getExperienciaProfissional().size();
		boolean semExperiencia = curriculoModel.getExperienciaProfissional().size() > 0 ? curriculoModel.getExperienciaProfissional().get(0).isExperiencia() : false ;
		String idInicial = "99999";
		
		if(tamanhoLista == 1 ) {
			idInicial = curriculoModel.getExperienciaProfissional().get(0).getId();
		}
				
		
		if(tamanhoLista == 1 && semExperiencia && (idInicial != null && !"".equals(idInicial) && !ID_INICIAL.equals(idInicial))) {
			ExperienciaProfissional wpExp = curriculoModel.getExperienciaProfissional().get(0);
			WpExperienciaProfissional wp = experienciaProfissionalRepository.findById(wpExp.getId()).get();
			
			wp.setDescAtribuicao(wpExp.getDescAtribuicao());
			
			
		} else {
			
			if(tamanhoLista == wpFor.size()) {
				//alterar o que ja existe
				for(ExperienciaProfissional arg : curriculoModel.getExperienciaProfissional()) {
					WpExperienciaProfissional wp = experienciaProfissionalRepository.findById(arg.getId()).get();
					
					if(arg.isExperiencia() == iExperiencia.NAO_POSSUI) {
						wp.setExperiencia(iExperiencia.NAO_POSSUI);
						wp.setNotas(arg.getNotas());
						
					} else {
						wp.setEmpregador(arg.getEmpregador());
						wp.setCargoOcupado(arg.getNomeTrabalho());
						wp.setDtInicio(arg.getDataInicio());
						wp.setDtFim(arg.getDataTermino());
						wp.setDescAtribuicao(arg.getDescAtribuicao());
						wp.setExperiencia(iExperiencia.POSSUI);
						wp.setIdProfissao(arg.getProfissao());
						
					}
					
				}		
			} else if (tamanhoLista < wpFor.size()) {
				
				listFormUsuarioConsulta.stream().filter(a -> !listFormUsuario.contains(a)).map(b -> b.getId()).collect(Collectors.toList()).forEach(c -> experienciaProfissionalRepository.removerXpProfissionalById(c));
				
			} else if (tamanhoLista > wpFor.size()) {
				
				if(this.verificarSemExperiencia(listFormUsuario)) {
					listFormUsuario.stream()
					.map(wpFormacao -> new WpExperienciaProfissional(wpFormacao.getId(), wpFormacao.getEmpregador(), wpFormacao.getNomeTrabalho(), wpFormacao.getDataInicio(), wpFormacao.getDataTermino(), wpFormacao.getNotas(), wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia(), wpFormacao.getProfissao(), wpCurriculos))
					.forEach(formacao -> {
						if(formacao.getId() != null) {
							experienciaProfissionalRepository.removerXpProfissionalById(formacao.getId());
						} 
					} );
					
				}
				
				
				listFormUsuario.stream().filter(a -> !listFormUsuarioConsulta.contains(a)).map(wpFormacao -> new WpExperienciaProfissional(null, wpFormacao.getEmpregador(), wpFormacao.getNomeTrabalho(), wpFormacao.getDataInicio(), wpFormacao.getDataTermino(), wpFormacao.getNotas(), wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia(), wpFormacao.getProfissao(), wpCurriculos))
				.collect(Collectors.toList()).forEach(c -> experienciaProfissionalRepository.inserirXpProfissional(c));
				
			}
		}
		
	}
	
	private boolean verificarSemExperiencia(List<ExperienciaProfissional> listaWpTecnologia) {
		for(ExperienciaProfissional wp : listaWpTecnologia) {
			if(wp.isExperiencia()) {
				return true;			
			}
		}
		return false;
	}

}
