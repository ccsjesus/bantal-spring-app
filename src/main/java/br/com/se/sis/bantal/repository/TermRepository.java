package br.com.se.sis.bantal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpTerm;

public interface TermRepository extends JpaRepository<WpTerm, String>, JpaSpecificationExecutor<WpTerm> {
	
	
	@Query(value="SELECT t.*, tt.*, tr.object_id FROM wp_terms AS t INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id INNER JOIN wp_term_relationships AS tr ON tr.term_taxonomy_id = tt.term_taxonomy_id WHERE tt.taxonomy IN ('resume_category') AND tr.object_id = :idVaga ORDER BY t.name ASC",
			nativeQuery = true)
	WpTerm obterAreaAtuacaoPorIdVaga(@Param("idVaga") String idVaga);
	
	@Query(value="SELECT t.*, tt.*, tr.object_id FROM wp_terms AS t INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id INNER JOIN wp_term_relationships AS tr ON tr.term_taxonomy_id = tt.term_taxonomy_id WHERE tt.taxonomy IN ('job_listing_type') AND tr.object_id = :idVaga ORDER BY t.name ASC limit 1",
			nativeQuery = true)
	WpTerm obterTipoVagaPorIdVaga(@Param("idVaga") String idVaga);

}
