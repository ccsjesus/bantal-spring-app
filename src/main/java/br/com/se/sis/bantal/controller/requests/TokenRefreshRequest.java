package br.com.se.sis.bantal.controller.requests;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class TokenRefreshRequest {

    @NotEmpty(message = "RefreshToken é obrigatório")
    private String refreshToken;
}
