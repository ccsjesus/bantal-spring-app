package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.annotations.UserEmployerAndAdminCanOnlyAccessResources;
import br.com.se.sis.bantal.annotations.UserEmployerAndCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.modelo.Usuario;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.EmpresaService;
import br.com.se.sis.bantal.services.PerfilServices;
import br.com.se.sis.bantal.services.PlanoPagamentoService;
import br.com.se.sis.bantal.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.util.Objects;

@RestController
@RequestMapping("/dados")
public class DadosController {

	
	@Autowired
	private TokenRepository tokenRepository;
	
	@Autowired
	private UsuarioService usuarioGerenciador;
	
	@Autowired
	private PlanoPagamentoService planosService;
	
	@Autowired
	private PerfilServices perfilServices;

	@Autowired
	private EmpresaService empresaService;
	
	@Transactional
	@RequestMapping(consumes="application/json", method = RequestMethod.POST , value = "/alterar")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> alterarDados(Authentication authentication, @RequestBody Usuario usuario) throws IllegalArgumentException, UnsupportedEncodingException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		//alterando firstName

		String idUser = customDetails.getId();

		if(usuario != null && usuario.getFirst_name().length() > 3) {
			perfilServices.atualizarNomeUsuario(idUser, usuario.getFirst_name());
		}
		
		//alterando lastName 
		//if(usuario != null && usuario.getLast_name().length() > 3) {
			//perfilServices.atualizarNomeUsuario(tokenEmpresa.getWpUserTokens().getId().toString(), usuario.getLast_name());
		//}
		
		//alterando displayName 
		if(usuario != null && !"".equals(usuario.getDisplayName())) {
			perfilServices.atualizarDisplayNomeUsuario(idUser, usuario.getDisplayName());
		}
		
		//alterando displayName 
		if(usuario != null && !"".equals(usuario.getEmail())) {
			perfilServices.atualizarEmail(idUser, usuario.getEmail());
		}

		if (!Objects.isNull(usuario.getSobreEmpresa()) || !Objects.isNull(usuario.getSigla())) {
			empresaService.atualizarempresaPorUsuario(idUser, usuario);
		}
		
		if(usuario != null 
				&& !"".equals(usuario.getNewSenha())
				&& !"".equals(usuario.getUserPass())
		   ) {
			perfilServices.atualizarSenha(idUser, usuario);
		}
		
		return ResponseEntity.ok(null);
	}
	
	@RequestMapping(method = RequestMethod.GET , value = "/")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> obterDadosUsuario(Authentication authentication) throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(usuarioGerenciador.obterDadosUsuario(customDetails.getId()));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/obter-dados-usuario")
	@UserEmployerAndCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getDadosPorToken(Authentication authentication) throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(usuarioGerenciador.obterDadosUsuario(customDetails.getId()));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/obter-expiracao-plano")
	@UserEmployerAndCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getExpirationPlan(Authentication authentication) throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(planosService.obterPeriodoVingenciaPlano(customDetails.getId()));
	}
	

}
