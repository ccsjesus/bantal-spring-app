package br.com.se.sis.bantal.services;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.exceptions.NotFoundException;
import br.com.se.sis.bantal.modelo.EmpresasDTO;
import br.com.se.sis.bantal.modelo.ContagemVagasDisponiveisDTO;
import br.com.se.sis.bantal.modelo.DetalhesVagasDisponiveisDTO;
import br.com.se.sis.bantal.modelo.VagasDisponiveisDTO;
import br.com.se.sis.bantal.repository.VagasDisponiveisRepository;
import br.com.se.sis.bantal.util.Util;

@Service
public class VagasDisponiveisServices {
	@Autowired
	private VagasDisponiveisRepository vagasDisponiveisRepository;

	public List<VagasDisponiveisDTO> recuperarVagasDisponiveis() {

		List<VagasDisponiveisDTO> listaVagasDisponiveis = vagasDisponiveisRepository.recuperarVagasDisponiveis();
		return listaVagasDisponiveis;

	}

	public List<VagasDisponiveisDTO> recuperarVagasDisponiveisPorIdEmpr(String idEmpresa) {
		List<VagasDisponiveisDTO> ListaVagasDisponiveisPorIdEmpr = vagasDisponiveisRepository
				.recuperarVagasDisponiveisPorIdEmpr(idEmpresa);
		if (ListaVagasDisponiveisPorIdEmpr.isEmpty()) {
			throw new NotFoundException(ErrorsMessages.EMPRESA_NOT_FOUND.getDescription(),
					ErrorsMessages.EMPRESA_NOT_FOUND);
		}
		return ListaVagasDisponiveisPorIdEmpr;
	}

	public List<DetalhesVagasDisponiveisDTO> recuperarDetalhementoVagaDisponivelPorID(String idVaga) {

		List<DetalhesVagasDisponiveisDTO> detalhesListaVagasDisponiveisPorId = vagasDisponiveisRepository
				.recuperarDetalhementoVagaDisponivelPorID(idVaga);

		if (detalhesListaVagasDisponiveisPorId.isEmpty()) {
			throw new NotFoundException(ErrorsMessages.VAGAS_NOT_FOUND.getDescription(),
					ErrorsMessages.VAGAS_NOT_FOUND);
		}

		return detalhesListaVagasDisponiveisPorId;
	}

	public List<EmpresasDTO> recuperarEmpresas() {
		List<EmpresasDTO> contagemVagasDisponiveis = vagasDisponiveisRepository.recuperarEmpresas();
		
		if (contagemVagasDisponiveis.isEmpty()) {
			throw new NotFoundException(ErrorsMessages.EMPRESA_NOT_FOUND.getDescription(),
					ErrorsMessages.EMPRESA_NOT_FOUND);
		}
		
		descompressBytesEmpresasDTO(contagemVagasDisponiveis);
		
		return contagemVagasDisponiveis;
	}

	public List<ContagemVagasDisponiveisDTO> recuperarDetalhamentoEmpresaPorId(String idEmpresa) {
		List<ContagemVagasDisponiveisDTO> contagemVagasDisponiveisPorIdEmpr = vagasDisponiveisRepository
				.recuperarDetalhamentoEmpresaPorId(idEmpresa);
		
		if (contagemVagasDisponiveisPorIdEmpr.isEmpty()) {
			throw new NotFoundException(ErrorsMessages.EMPRESA_NOT_FOUND.getDescription(),
					ErrorsMessages.EMPRESA_NOT_FOUND);
		}
		
		for (ContagemVagasDisponiveisDTO empresa : contagemVagasDisponiveisPorIdEmpr) {
			empresa.setQuantidade(vagasDisponiveisRepository.contagemVagasPorIdEmpresa(idEmpresa));
		}
		
		descompressBytesContagemVagasDisponiveisDTO(contagemVagasDisponiveisPorIdEmpr);
		
		return contagemVagasDisponiveisPorIdEmpr;
	}

	public List<ContagemVagasDisponiveisDTO> recuperarDetalhamentoEmpresaPorSigla(String sigla) {
		List<ContagemVagasDisponiveisDTO> contagemVagasDisponiveisPorSigla = vagasDisponiveisRepository
				.contagemVagasDisponiveisPorSigla(sigla);
		
		if (contagemVagasDisponiveisPorSigla.isEmpty()) {
			throw new NotFoundException(ErrorsMessages.EMPRESA_NOT_FOUND.getDescription(),
					ErrorsMessages.EMPRESA_NOT_FOUND);
		}
		
		for (ContagemVagasDisponiveisDTO empresa : contagemVagasDisponiveisPorSigla) {
			empresa.setQuantidade(vagasDisponiveisRepository.contagemVagasPorIdEmpresa(empresa.getUserId()));
		}
		
		descompressBytesContagemVagasDisponiveisDTO(contagemVagasDisponiveisPorSigla);
		
		return contagemVagasDisponiveisPorSigla;
	}
	
	public void descompressBytesEmpresasDTO(List<EmpresasDTO> lista) {
		for(EmpresasDTO empresa : lista) {
			if(!Objects.isNull(empresa.getFoto())) {
				empresa.setFoto(Util.decompressBytes(empresa.getFoto()));
			}
		}
	}
	
	public void descompressBytesContagemVagasDisponiveisDTO(List<ContagemVagasDisponiveisDTO> lista) {
		for(ContagemVagasDisponiveisDTO empresa : lista) {
			if(!Objects.isNull(empresa.getFoto())) {
				empresa.setFoto(Util.decompressBytes(empresa.getFoto()));
			}
		}
	}	

}