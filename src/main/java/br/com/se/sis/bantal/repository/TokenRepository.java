package br.com.se.sis.bantal.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpTokenSistema;

public interface TokenRepository extends JpaRepository<WpTokenSistema, String>, JpaSpecificationExecutor<WpTokenSistema> {

	@Query("SELECT distinct token FROM WpTokenSistema token where token.wpUserTokens.id = :id and token.tokenStatus = :status")
	List<WpTokenSistema> consultarTokenAutorizado(@Param("id") BigDecimal id, @Param("status") int status);
	
	@Query("SELECT distinct token FROM WpTokenSistema token where token.token = :token and token.tokenStatus = 0")
	WpTokenSistema consultarToken(@Param("token") String token);
	
}
