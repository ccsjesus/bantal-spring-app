package br.com.se.sis.bantal.modelo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;


/**
 * The persistent class for the wp_users database table.
 * 
 */

@Data
@Entity
@Table(name="wp_cargo_preterido")
public class WpCargoPreterido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_cargo")
	private String id;

	@Column(name="area_atuacao")
	private String areaAtuacao;

	@Column(name="funcao_cargo")
	private String funcaoCargo;

	@Column(name="nivel_cargo")
	private String nivelCargo;

	@Column(name="pretensao_salarial")
	private String pretensaoSalarial;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="id_curriculo", insertable = false, updatable = false)
	@JsonBackReference
	private WpCurriculos wpCurriculosCargo;

	public WpCargoPreterido(){
		
	}
	

}