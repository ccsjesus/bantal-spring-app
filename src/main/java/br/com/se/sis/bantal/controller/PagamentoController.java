package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.dto.WpPlanoPagamentoDTO;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.DateService;
import br.com.se.sis.bantal.services.PlanoPagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {
	
	@Autowired
	private TokenRepository tokenRepository;
	
	@Autowired
	private DateService dateService;
	
	@Autowired
	private PlanoPagamentoService pagamentoService;
	
	
	@Transactional
	@RequestMapping(consumes="application/json", method = RequestMethod.POST , value = "/registrarPagamento")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> registrarPagamento(Authentication authentication, @RequestBody WpPlanoPagamentoDTO planoPagamento) {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		planoPagamento.setDataConfirmacaoPagamento(dateService.now());
		planoPagamento.setCdUsuario(customDetails.getId());
		pagamentoService.registrarTransacaoPagamento(planoPagamento, customDetails.getId());

		return ResponseEntity.ok(planoPagamento);
	}
	
	@Transactional
	@RequestMapping(consumes="application/json", method = RequestMethod.POST , value = "/registrarSolicitacaoPagamento")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> registerAccessPayment(Authentication authentication, @RequestBody WpPlanoPagamentoDTO planoPagamento) {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		planoPagamento.setDataSolicitacaoPagamento(dateService.now());
		planoPagamento.setCdUsuario(customDetails.getId());
		
		pagamentoService.registrarSolicitacaoPagamento(planoPagamento);
		
		return ResponseEntity.ok(planoPagamento);
	}
	
	@Transactional
	@RequestMapping(consumes="application/json", method = RequestMethod.GET , value = "/registrarSPagamento")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> registerPayment(@RequestBody Object planoPagamento) {
		
		System.out.println(planoPagamento);
		
		return ResponseEntity.ok(planoPagamento);
	}

}
