package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import br.com.se.sis.bantal.dto.WpMensagemDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VagasModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String userId;
	
	private String nomeEmpresa;

	private String titulo;

	private String localizacao;

	private String descricao;

	private String dataLimite;

	private String status;
	
	private String salario;
	
	private byte [] foto;
	
	private LocalDateTime dataCriacao;
	
	private AreaAtuacaoModel areaAtuacaoModel;
	
	private TipoVagaModel tipoVagaModel;
	
	private List<WpCandidatoModel> listCandidatoInscrito;
	
	private int confidencial;
	
	private int excluido;
	
	private List<WpMensagemDTO> mensagens;
	
	private Profissao profissao;
	
	public VagasModel(String id, String userId, String titulo, String descricao, String dataLimite, String localizacao, String salario,
			TipoVagaModel tipoVaga, AreaAtuacaoModel areaAtuacao,  String status, byte[] foto, int confidencial, int excluido) {
		super();
		this.id = id;
		this.userId = userId;
		this.titulo = titulo;
		this.descricao = descricao;
		this.dataLimite = dataLimite;
		this.localizacao = localizacao;
		this.salario = salario;
		this.tipoVagaModel = tipoVaga;
		this.areaAtuacaoModel = areaAtuacao;
		this.status = status;
		this.foto = foto;
		this.confidencial = confidencial;
		this.excluido = excluido;
				
	}
	
}