package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.UserAdminCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.PerfilServices;
import br.com.se.sis.bantal.services.VagasService;
import br.com.se.sis.bantal.util.StatusVaga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/v1/vagas-admin")
public class VagasAdminController {

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private PerfilServices perfilServices;
	
	@Autowired
	private VagasService vagasService;

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/aprovar/{idvaga}")
	@UserAdminCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> aprovarVaga(Authentication authentication, @PathParam("idvaga") String idVaga)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(vagasService.atualizarSituacaoVagaPorId(customDetails.getId(), StatusVaga.APROVADA));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/excluir/{idvaga}")
	@UserAdminCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> excluirVaga(Authentication authentication, @PathParam("idvaga") String idVaga)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(vagasService.atualizarSituacaoVagaPorId(customDetails.getId(), StatusVaga.EXCLUIDA));
	}
	
	

}
