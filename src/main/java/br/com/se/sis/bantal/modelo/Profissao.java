package br.com.se.sis.bantal.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Entity
@Table(name = "wp_profissao")
public class Profissao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_profissao")
	@JsonProperty("idProfissao")
	private Long idProfissao;
	
	@Column(name = "nome")
	@JsonProperty("nome")
	private String nome;
	
	@Column(name = "id_area_atuacao")
	@JsonProperty("idAreaAtuacao")
	private Long idAreaAtuacao;
	
	@Column(name = "habilitada")
	@JsonProperty("habilitada")
	private boolean habilitada;
	
}
