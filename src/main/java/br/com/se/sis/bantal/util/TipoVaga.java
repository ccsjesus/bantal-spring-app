package br.com.se.sis.bantal.util;

public enum TipoVaga {
	
	TIPO_JOB_LISTING("job_listing"),
	TIPO_JOB_APPLICATION("job_application"),
	TIPO_JOB("job_listing_type"),
	AREA_ATUACAO("rea_de_atuao2"),
	RESUME_CATEGORY("resume_category"); // refere-se as area de atuação do candidato
	
	private String valor;
	
	TipoVaga(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}
	
	@Override
	public String toString() {
		return this.valor;
	}

}

