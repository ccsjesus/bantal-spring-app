package br.com.se.sis.bantal.modelo;

public class FuncaoModel {
	
	private String nome;
	
	private String valor;

	public FuncaoModel() {
		// TODO Auto-generated constructor stub
	}
	
	public FuncaoModel(String nome, String valor) {
		super();
		this.nome = nome;
		this.valor = valor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
}
