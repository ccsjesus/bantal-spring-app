package br.com.se.sis.bantal.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.se.sis.bantal.modelo.CNHModel;
import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.modelo.ExperienciaProfissional;
import br.com.se.sis.bantal.modelo.Profissao;
import br.com.se.sis.bantal.modelo.Usuario;
import br.com.se.sis.bantal.modelo.WpCurriculos;
import br.com.se.sis.bantal.modelo.WpExperienciaProfissional;
import br.com.se.sis.bantal.modelo.WpFormacao;
import br.com.se.sis.bantal.modelo.WpIdioma;
import br.com.se.sis.bantal.modelo.WpTecnologias;
import br.com.se.sis.bantal.modelo.WpUser;
import br.com.se.sis.bantal.modelo.WpUsermeta;
import br.com.se.sis.bantal.repository.CurriculoRepositoryV2;
import br.com.se.sis.bantal.repository.ProfissaoRepository;
import br.com.se.sis.bantal.util.IUtils;
import br.com.se.sis.bantal.util.MetaKeys;
import br.com.se.sis.bantal.util.Util;
import br.com.se.sis.bantal.util.iExperiencia;
import br.com.se.sis.pagseguro.exception.TransacaoAbortadaException;

@Service
public class CurriculoServices {

	@Autowired
	private CurriculoRepositoryV2 curriculoRepository;
	
	@Autowired
	private DateService dateService;
	
	@Autowired
	private FormacaoServices formacaoServices;
	
	@Autowired
	private TecnologiaServices tecnologiaServices;
	
	@Autowired
	private ExperienciaProfissionalServices experienciaProfissionalServices;
	
	@Autowired
	private IdiomaServices idiomaServices;
	
	@Autowired
	private AreaAtuacaoServices areaAtuacaoServices;
	
	@Autowired
	private ProfissaoRepository profissaoRepository;
	

	
	public CurriculoModel obterCurriculoModelUsuario(String idUsuario) {
		WpCurriculos curriculo = curriculoRepository.obterCurriculoCandidato(idUsuario);
		
		CurriculoModel cModel = CurriculoModel.montarCurriculoFromWpCurriculo(curriculo);
		for (ExperienciaProfissional experienciaProfissional : cModel.getExperienciaProfissional()) {
			if (!Objects.isNull(experienciaProfissional.getProfissao()) && !experienciaProfissional.getProfissao().equals("-1")) {
				Profissao profissao = profissaoRepository.recuperarProfissaoPorId(Long.valueOf(experienciaProfissional.getProfissao()));
				experienciaProfissional.setNomeTrabalho(profissao.getIdProfissao() + " - " + profissao.getNome());
			} else {
				experienciaProfissional.setProfissao("");
			}
		}
		
		return cModel;
	}
	
	public List<CurriculoModel> obterTodosCurriculos(){
		return curriculoRepository.findAll().stream().map(curriculo -> CurriculoModel.montarCurriculoFromWpCurriculo(curriculo)).collect(Collectors.toList());
	}
	
	public WpCurriculos inserirDadosIniciaisCurriculo(WpUser wpUser) {
		String id = wpUser.getId().toString();
		WpCurriculos curriculo = curriculoRepository.obterCurriculoCandidato(id);
		String identificacao = "";
		
		if(curriculo == null) {
			for(WpUsermeta meta : wpUser.getWpUsermetas()) {
				if (Util.metaKeyExists(meta.getMetaKey(), MetaKeys.META_BILLING_CNPJ)  ||
					Util.metaKeyExists(meta.getMetaKey(), MetaKeys.META_BILLING_CPF) ||
					Util.metaKeyExists(meta.getMetaKey(), MetaKeys.META_CADIDATO_CPF)) {
					identificacao = meta.getMetaValue();
					break;
				}
			}

			curriculo = new WpCurriculos();
			curriculo.setUserId(wpUser.getId().toString());
			curriculo.setNome(wpUser.getDisplayName());
			curriculo.setIdentificacao(identificacao);
			curriculo.setEmail(wpUser.getUserEmail());
			curriculo.setDtDegistro(dateService.now());
			curriculoRepository.save(curriculo);
		}
		return curriculo;
	}
	
	public void adicionarSemExperiencia(String idLogado, CurriculoModel curriculoModel) throws TransacaoAbortadaException{
		WpCurriculos wpCurriculos = curriculoRepository.obterCurriculoCandidato(idLogado);

		if (curriculoModel.getExperienciaProfissional() != null) {
			experienciaProfissionalServices.adicionarSemExperiencia(curriculoModel, wpCurriculos);
		}
		
	}
	
	
	public void atualizarSemExperiencia(String idLogado, CurriculoModel curriculoModel) throws TransacaoAbortadaException{
		WpCurriculos wpCurriculos = curriculoRepository.obterCurriculoCandidato(idLogado);

		if (curriculoModel.getExperienciaProfissional() != null) {
			experienciaProfissionalServices.atualizarSemExperiencia(curriculoModel, wpCurriculos);
		}
		
	}
	
	public void adicionarTecnologiaSemExperiencia(String idLogado, CurriculoModel curriculoModel) throws TransacaoAbortadaException{
		WpCurriculos wpCurriculos = curriculoRepository.obterCurriculoCandidato(idLogado);
		
		if (curriculoModel.getInformatica() != null) {
			tecnologiaServices.adicionarSemExperiencia(curriculoModel, wpCurriculos);
		}
		
	}
	
	public void atualizarTecnologiaSemExperiencia(String idLogado, CurriculoModel curriculoModel) throws TransacaoAbortadaException{
		WpCurriculos wpCurriculos = curriculoRepository.obterCurriculoCandidato(idLogado);

		if (curriculoModel.getInformatica() != null) {
			tecnologiaServices.atualizarSemExperiencia(curriculoModel, wpCurriculos);
		}
		
	}
	
	
	
	public void atualizarDadosCurriculo(String idLogado, CurriculoModel curriculoModel) throws TransacaoAbortadaException{
		WpCurriculos wpCurriculos = curriculoRepository.obterCurriculoCandidato(idLogado);

		if (curriculoModel.getApresentacao() != null ) {
			if(curriculoModel.getApresentacao().length() <= 255) {
				wpCurriculos.setApresentacao(curriculoModel.getApresentacao());
			} else {
				throw new TransacaoAbortadaException("Conteúdo da apresentacão tem " + curriculoModel.getApresentacao().length() + " caracteres e é maior que o permitido, 255 caracteres.");
			}
		} 

		if (curriculoModel.getObjetivoProfissional() != null ) { 
			if ( curriculoModel.getApresentacao().length() <= 255) {
				wpCurriculos.setObjetivoProfissional(curriculoModel.getObjetivoProfissional());
		    } else {
				throw new TransacaoAbortadaException("Conteúdo do objetivo tem " + curriculoModel.getObjetivoProfissional().length() + " caracteres e é  maior que o permitido, 255 caracteres.");
			}
		}

		if (curriculoModel.getNome() != null) {
			wpCurriculos.setNome(curriculoModel.getNome());
		}

		if (curriculoModel.getNacionalidade() != null) {
			wpCurriculos.setNacionalidade(curriculoModel.getNacionalidade());
		}

		if (curriculoModel.getEstadoCivil() != null) {
			wpCurriculos.setEstadoCivil(curriculoModel.getEstadoCivil());
		}

		if (curriculoModel.getFilhos() != null) {
			wpCurriculos.setFilhos(curriculoModel.getFilhos());
		}
		
		if (curriculoModel.getIdade() != null) {
			wpCurriculos.setIdade(curriculoModel.getIdade());			
		}

		if (curriculoModel.getEndereco() != null) {
			wpCurriculos.setEndereco(curriculoModel.getEndereco());
		}

		if (curriculoModel.getTelefone() != null) {
			wpCurriculos.setTelefone(curriculoModel.getTelefone());
		}

		if (curriculoModel.getTelefoneRecados() != null) {
			wpCurriculos.setRecados(curriculoModel.getTelefoneRecados());
		}

		if (curriculoModel.getCelular() != null) {
			wpCurriculos.setCelular(curriculoModel.getCelular());
		}

		if (curriculoModel.getEmail() != null) {
			wpCurriculos.setEmail(curriculoModel.getEmail());
		}
		
		if (curriculoModel.getRg() != null) {
			wpCurriculos.setRg(curriculoModel.getRg());
		}

		if (curriculoModel.getLinkedin() != null) {
			wpCurriculos.setLinkedin(curriculoModel.getLinkedin());
		}

		if (curriculoModel.getFormacao() != null) {
			wpCurriculos.setFormacao(curriculoModel.getFormacao());
		}

		if (curriculoModel.getCnh() != null) {
			String selecionados = "";
			for(CNHModel cnh : curriculoModel.getCnh()) {
				if(cnh.isSelected()) {
					selecionados += cnh.getValue() + IUtils.CURINGA;
				}
			}
			wpCurriculos.setCnh(selecionados);
		}
		
		if (curriculoModel.getFormacaoAcademica() != null) {
			formacaoServices.atualizarFormacaoAcademica(curriculoModel, wpCurriculos);
		}
		
		if (curriculoModel.getInformatica() != null) {
			tecnologiaServices.atualizarSemExperiencia(curriculoModel, wpCurriculos);
		}
		
		if (curriculoModel.getExperienciaProfissional() != null) {
			experienciaProfissionalServices.atualizarExperienciaProfissional(curriculoModel, wpCurriculos);
		}
		
		if (curriculoModel.getIdiomas() != null) {
			idiomaServices.atualizarIdioma(curriculoModel, wpCurriculos);
		}
		
		if (curriculoModel.getAreaAtuacao() != null) {
			areaAtuacaoServices.atualizarAreaAtuacao(curriculoModel, wpCurriculos);
		}
		
		if (curriculoModel.getPretensaoSalarial() != null) {
			wpCurriculos.setPretensaoSalarial(curriculoModel.getPretensaoSalarial());
		}
		
		if (curriculoModel.getFuncaoCargo() != null) {
			wpCurriculos.setFuncaoCargo(curriculoModel.getFuncaoCargo());
		}
		
		if (curriculoModel.getNivel() != null) {
			wpCurriculos.setNivelCargo(curriculoModel.getNivel());
		}
	}
	
	public List<Usuario> cadastrarInformacoesImpressaoCurriculo(List<String> listIdLogado) throws TransacaoAbortadaException{
		List<WpCurriculos> wpCurriculos = curriculoRepository.obterCurriculoCandidato(listIdLogado);
		List<Usuario> listUser = new ArrayList<Usuario>();
		Usuario usuario;
		
		for(WpCurriculos wpCurr : wpCurriculos) {
			usuario = new Usuario();
			
			if (wpCurr.getIdentificacao() != null ) {
				usuario.setIdentificacao(wpCurr.getIdentificacao());
			} 
			
			if (wpCurr.getApresentacao() != null ) {
				usuario.setApresentacao(wpCurr.getApresentacao());
			} 

			if (wpCurr.getObjetivoProfissional() != null ) {
				usuario.setObjetivos(wpCurr.getObjetivoProfissional());
			}
			
			if (wpCurr.getNome() != null) {
				usuario.setDisplayName(wpCurr.getNome());
				usuario.setFirst_name(wpCurr.getNome());
			}
			
			if (wpCurr.getNacionalidade() != null) {
				usuario.setCountry(wpCurr.getNacionalidade());
			}

			if (wpCurr.getEstadoCivil() != null) {
				usuario.setEstadoCivil(wpCurr.getEstadoCivil());
			}

			if (wpCurr.getFilhos() != null) {
				usuario.setQtdFilhos(wpCurr.getFilhos());
			}
			
			if (wpCurr.getIdade() != null) {
				usuario.setIdade(wpCurr.getIdade());
			}
			
			if (wpCurr.getEndereco() != null) {
				usuario.setAddress_1(wpCurr.getEndereco());
			}
			
			if (wpCurr.getEndereco() != null) {
				usuario.setPhone(wpCurr.getTelefone());
			}

			if (wpCurr.getTelefone() != null) {
				usuario.setCellphone(wpCurr.getTelefone());
			}

			if (wpCurr.getRecados() != null) {
				usuario.setRecado(wpCurr.getRecados());
			}

			if (wpCurr.getEmail() != null) {
				usuario.setEmail(wpCurr.getEmail());
			}
			
			if (wpCurr.getRg() != null) {
				usuario.setRg(wpCurr.getRg());
			}

			if (wpCurr.getLinkedin() != null) {
				usuario.setLikedin(wpCurr.getLinkedin());
			}
			
			if (wpCurr.getFoto() != null) {
				usuario.setImagemPerfil(Base64.getEncoder().encodeToString(wpCurr.getFoto()));	
			}

			if (wpCurr.getListFormacaoAcademica() != null) {
				
				String formacao = "";
				for(WpFormacao form : wpCurr.getListFormacaoAcademica()) {
					formacao += form.getUniversidade()
								.concat("\n".concat(form.getQualificacao())
								.concat(" ")).concat(form.getDtInicio()
								.concat(" ")).concat(form.getDtTermino().concat("\n\n"));

				}
				usuario.setFormacao(formacao);
			}
			
			if (wpCurr.getListIdiomas() != null) {
				
				String str = "";
				for(WpIdioma arg2 : wpCurr.getListIdiomas()) {
					str += arg2.getIdioma()
								.concat("\n| leitura: ".concat(arg2.getNivelLeitura())
								.concat(" | conversação: ")).concat(arg2.getNivelConversacao()
								.concat(" | escrita: ")).concat(arg2.getNivelEscrita().concat("\n\n"));

				}
				usuario.setIdiomas(str);
			}
			
			if (wpCurr.getListTecnologias() != null) {
				
				String str = "";
				for(WpTecnologias arg2 : wpCurr.getListTecnologias()) {
					// se o campo sem experiencia estiver macardo como sim
					if(arg2.isExperiencia() == iExperiencia.POSSUI) {
						str += arg2.getDescAtribuicao();
						
					} else {
						str += arg2.getTecnologia()
									.concat(" | ".concat(arg2.getNivel().concat("\n")));
					}

				}
				usuario.setInformatica(str);
			}
			
			if (wpCurr.getListExperienciaProfissional() != null) {
				
				String str = "";
				for(WpExperienciaProfissional arg2 : wpCurr.getListExperienciaProfissional()) {
					// se o campo sem experiencia estiver macardo como sim
					if(arg2.isExperiencia() == iExperiencia.POSSUI) {
						str += arg2.getDescAtribuicao();
						
					} else {
						
						str += arg2.getEmpregador()
								.concat(" | ".concat(arg2.getCargoOcupado())
										.concat(" ")).concat(arg2.getDtInicio()
												.concat(" ")).concat(arg2.getDtFim().concat("\n"));
					}

				}
				usuario.setExperiencia(str);
			}
			
			if (wpCurr.getCnh() != null) {
				String[] valor = Util.splitEspecial(wpCurr.getCnh());
				
				String str = "";
				for(String arg2 : valor) {
					str += arg2.concat(" ");

				}
				usuario.setCnh(str);
			}
			listUser.add(usuario);
		}
		
		return listUser;
		
	}
	
	public void uploadImagem(String idLogado, MultipartFile file) throws IOException {
		WpCurriculos wpCurriculos = curriculoRepository.obterCurriculoCandidato(idLogado);

		wpCurriculos.setFoto(Util.compressBytes(file.getBytes()));

		curriculoRepository.save(wpCurriculos);
	}

	public WpCurriculos updateUserIdCurriculoById(String idCurriculo, String userId){
		WpCurriculos wpCurriculos = curriculoRepository.findById(idCurriculo).orElseThrow(() -> new NotFoundException(ErrorsMessages.CURRICULO_NOT_FOUND.getDescription(), ErrorsMessages.CURRICULO_NOT_FOUND));
		wpCurriculos.setUserId(userId);
		return curriculoRepository.save(wpCurriculos);
	}

	public WpCurriculos findByUserId(String userId) {
		return curriculoRepository.findByUserId(userId).orElse(null);
	}

	public List<CurriculoModel> obterCurriculoPorProfissao(String idProfissao) {
		return curriculoRepository.obterCurriculoPorProfissao(idProfissao).stream()
				.map(curriculo -> CurriculoModel.montarCurriculoFromWpCurriculo(curriculo))
				.collect(Collectors.toList());
	}
}