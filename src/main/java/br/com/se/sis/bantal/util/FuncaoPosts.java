package br.com.se.sis.bantal.util;

public enum FuncaoPosts {	
	
	FUNCAO_CANDIDATE("candidate_funcao"),
	FUNCAO_CANDIDATE_NIVEL("candidate_nivel"),
	FUNCAO_CANDIDATE_SALARIO("candidate_salario");
	
	private String valor;
	
	FuncaoPosts(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}
	
	@Override
	public String toString() {
		return this.valor;
	}

}

