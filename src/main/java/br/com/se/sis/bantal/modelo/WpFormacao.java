package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_formacao")
public class WpFormacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private String id;

	@Column(name="universidade")
	private String universidade;

	@Column(name="qualificacao")
	private String qualificacao;

	@Column(name="dt_inicio")
	private String dtInicio;

	@Column(name="dt_termino")
	private String dtTermino;

	@Column(name="nota")
	private String nota;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="id_curriculo", insertable = false, updatable = false)
	@JsonBackReference
	private WpCurriculos wpCurriculos;
	
	public WpFormacao(String universidade, String qualificacao, String dtInicio, String dtTermino, String nota,
			WpCurriculos wpCurriculos) {
		super();
		this.universidade = universidade;
		this.qualificacao = qualificacao;
		this.dtInicio = dtInicio;
		this.dtTermino = dtTermino;
		this.nota = nota;
		this.wpCurriculos = wpCurriculos;
	}
	

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WpFormacao))
			return false;
		return id != null && id.equals(((WpFormacao) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
	
	public static List<FormacaoAcademica> fromToFormacaoAcademica(List<WpFormacao> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new FormacaoAcademica(wpFormacao.getId(), wpFormacao.getUniversidade(), wpFormacao.getQualificacao(), wpFormacao.getDtInicio(), wpFormacao.getDtTermino(), wpFormacao.getNota()))
		        .collect(Collectors.toList());
	}
	
	public static List<WpFormacao> fromToFormacao(List<FormacaoAcademica> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new WpFormacao(wpFormacao.getId(), wpFormacao.getUniversidade(), wpFormacao.getQualificacao(), wpFormacao.getDataInicio(), wpFormacao.getDataFim(), wpFormacao.getNota(), null))
		        .collect(Collectors.toList());
	}


}