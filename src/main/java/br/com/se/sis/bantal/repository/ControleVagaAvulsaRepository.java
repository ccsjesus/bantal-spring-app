package br.com.se.sis.bantal.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpControleVagaAvulsa;

public interface ControleVagaAvulsaRepository extends JpaRepository<WpControleVagaAvulsa, String>, JpaSpecificationExecutor<WpControleVagaAvulsa> {

	@Query(value = "select controle.* from wp_controle_vagas_avulsas controle join wp_plano_pagamento pagamento on controle.cd_plano_pagamento = pagamento.id where pagamento.cd_usuario = :idUsuario and pagamento.cd_transacao is not null", nativeQuery = true)
	WpControleVagaAvulsa recuperarControleVagasAvulsasPorEmpresa(@Param("idUsuario") String idUsuario);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "update wp_controle_vagas_avulsas set qtd_publicada=qtd_publicada-1 where cd_controle_vagas_avulsas = :idControle", nativeQuery = true)
	void subtratirQuantidadeVagaAvulsa(@Param("idControle") String idControle);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "update wp_controle_vagas_avulsas set qtd_publicada=qtd_publicada+1 where cd_controle_vagas_avulsas = :idControle", nativeQuery = true)
	void adicionarQuantidadeVagaAvulsa(@Param("idControle") String idControle);

}
