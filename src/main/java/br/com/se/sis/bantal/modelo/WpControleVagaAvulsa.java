package br.com.se.sis.bantal.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
@Table(name = "wp_controle_vagas_avulsas")
public class WpControleVagaAvulsa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cd_controle_vagas_avulsas")
	private String cdControle;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cd_plano_pagamento")
	@JsonBackReference
	private WpPlanoPagamento wpPlanoPagamento;

	@Column(name = "qtd_publicada")
	private Integer quantidade;
		
}