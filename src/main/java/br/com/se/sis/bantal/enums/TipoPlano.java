package br.com.se.sis.bantal.enums;

public enum TipoPlano {
	
	SEMESTRE("SEMESTRE"), ANUAL("ANUAL"), AVULSO("AVULSO"), FREE("FREE"), ADMIN("ADMIN"), INICIAL("INICIAL");
	
	private String descricao;

	TipoPlano(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
