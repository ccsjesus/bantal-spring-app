package br.com.se.sis.bantal.exceptions;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import lombok.Data;

@Data
public class AuthenticationExceptionError extends RuntimeException {

    private String code;

    public AuthenticationExceptionError(String message, String code) {
        super(message);
        this.code = code;
    }
}
