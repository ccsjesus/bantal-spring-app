package br.com.se.sis.bantal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.VagasModel;
import br.com.se.sis.bantal.modelo.WpInformacoesGerais;
import br.com.se.sis.bantal.modelo.WpInscricaoVaga;

@Service
@Configurable
public class InformacoesGeraisService {

	@Autowired
	private VagasService vagasService;
	
	@Autowired
	private InscricaoVagaService inscricaoVagaServices;


	public WpInformacoesGerais obterInformacoesGerais(String idLogado) {
		List<WpInscricaoVaga> listaInscritosVaga = inscricaoVagaServices.obterInscricoesVagaPorIdUsuario(idLogado);
		List<VagasModel> listaVagas = vagasService.obterVagasDisponiveisporUsuarioLogado(idLogado);
		
		WpInformacoesGerais info =  new WpInformacoesGerais();
		info.setTotalInscritos(listaInscritosVaga != null ? listaInscritosVaga.size() + ""  :  "0");
		info.setQtdVagasAbertas(listaVagas != null ? listaVagas.size() + ""  :  "0");
		
		info.setVagas(listaVagas);
		return info;
	}

}
