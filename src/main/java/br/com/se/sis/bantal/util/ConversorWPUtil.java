package br.com.se.sis.bantal.util;

import br.com.se.sis.bantal.modelo.Term;
import br.com.se.sis.bantal.modelo.WpTerm;

public class ConversorWPUtil {

	private static ConversorWPUtil instance;
	
	
	private ConversorWPUtil() {
	
	}
	
	public static ConversorWPUtil getInstance() {
		return instance == null ? instance = new ConversorWPUtil() : instance;
	}
	
	public Term getTermofromWPTerm(WpTerm wp) {
		return Term.getInstance(wp);
	}
	
}
