package br.com.se.sis.bantal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpTermRelationship;
import br.com.se.sis.bantal.modelo.WpTermRelationshipPK;

public interface RelacionamentoTaxomiaRepository extends JpaRepository<WpTermRelationship, WpTermRelationshipPK>, JpaSpecificationExecutor<WpTermRelationship> {

	@Query(value="SELECT COUNT(*) FROM wp_term_relationships, wp_posts WHERE wp_posts.ID = wp_term_relationships.object_id AND post_status = 'publish' AND post_type IN ('job_listing', 'job_alert') AND term_taxonomy_id = :idTipoVaga",
			nativeQuery = true)
	Object obterRelacionamentoTaxonomiaTipoVaga(@Param("idTipoVaga") String idTipoVaga);
	
	@Query(value="SELECT COUNT(*) FROM wp_term_relationships, wp_posts WHERE wp_posts.ID = wp_term_relationships.object_id AND post_status = 'publish' AND post_type IN ('job_listing') AND term_taxonomy_id = :idTipoVaga",
			nativeQuery = true)
	Object obterRelacionamentoTaxonomiaAreaAtuacao(@Param("idTipoVaga") String idTipoVaga);
	
	@Query(value="SELECT rt.object_id, rt.term_taxonomy_id, rt.term_order FROM wp_term_relationships rt JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = rt.term_taxonomy_id where tt.taxonomy = :opcao and rt.object_id = :idVaga",
			nativeQuery = true)
	WpTermRelationship obterTaxonomiaTipoVaga(@Param("opcao") String opcao, @Param("idVaga") String idVaga);
	
	@Query(value="SELECT rt.object_id, rt.term_taxonomy_id, rt.term_order FROM wp_term_relationships rt JOIN wp_term_taxonomy tt ON tt.term_taxonomy_id = rt.term_taxonomy_id where tt.taxonomy = :opcao and rt.object_id = :idVaga",
			nativeQuery = true)
	List<WpTermRelationship> obterListaTaxonomiaTipoVaga(@Param("opcao") String opcao, @Param("idVaga") String idVaga);
	
	@Modifying
	@Query(value="update wp_term_relationships rt set rt.term_taxonomy_id = :idNovoTipoVaga \r\n" + 
			"			   where rt.object_id = :idVaga and rt.term_taxonomy_id = :idTipoVagaAtual",
			nativeQuery = true)
	void atualizarTipoVaga(@Param("idVaga") String idVaga, @Param("idNovoTipoVaga") String idNovoTipoVaga, @Param("idTipoVagaAtual") String idTipoVagaAtual);

	@Modifying
	@Query(value="DELETE FROM wp_term_relationships WHERE wp_term_relationships.object_id = :idPostCandidato AND wp_term_relationships.term_taxonomy_id = :idAreaAtuacao",
			nativeQuery = true)
	public void excluirAreaAtuacao(@Param("idAreaAtuacao") String idAreaAtuacao, @Param("idPostCandidato") String idPostCandidato);
	
}
