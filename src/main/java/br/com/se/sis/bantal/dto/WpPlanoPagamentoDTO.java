package br.com.se.sis.bantal.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WpPlanoPagamentoDTO {

	private String cdPlano;
	private String cdUsuario;
	private String cdTransacao;
	private String cdSolicitacaoTransacao;
	private LocalDateTime dataConfirmacaoPagamento;
	private LocalDateTime dataSolicitacaoPagamento;
	private String statusPagamento;;
	
}
