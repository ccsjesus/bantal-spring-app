package br.com.se.sis.bantal.util;

public @interface TipoMensagem {
	
	int MENSAGEM_LIDA = 1;
	
	int MENSAGEM_NAO_LIDA = 0;
	
	int MENSAGEM_TODAS = Integer.MAX_VALUE;
	
}

