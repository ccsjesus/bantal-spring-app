package br.com.se.sis.bantal.services;

import br.com.se.sis.bantal.dto.PainelAdministradorDTO;
import br.com.se.sis.bantal.dto.WpPerfilDTO;
import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.modelo.InscricaoVagaDTO;
import br.com.se.sis.bantal.modelo.VagasModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PainelAdministradorServices {

	@Autowired
	private PerfilServices perfilServices;
	
	@Autowired
	private VagasService vagasServices;

	@Autowired
	private CurriculoServices curriculoServices;
	
	@Autowired
	private InscricaoVagaService inscricoesServices;

	public PainelAdministradorDTO obterDadosPainelAdministrador(String idUsuarioLogado) {
		
		List<WpPerfilDTO> listaEmpresas = perfilServices.obterPerfisPorFuncaoSistema(Role.EMPLOYER);

		List<VagasModel> listaVagas = vagasServices.obterTodasVagas();

		List<CurriculoModel> listaCurriculos = curriculoServices.obterTodosCurriculos();

		List<InscricaoVagaDTO> listaInscricoesServices = inscricoesServices.obterTodasInscricoes();

		return new PainelAdministradorDTO(listaEmpresas, listaCurriculos, listaInscricoesServices, listaVagas);

	}
}