package br.com.se.sis.bantal.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpPost;
import br.com.se.sis.bantal.modelo.WpPostmeta;

public interface VagasRepository extends JpaRepository<WpPost, String>, JpaSpecificationExecutor<WpPost> {

	@Query("SELECT distinct post FROM WpPost post where post.postType = :tipo and post.id = :id")
	WpPost consultarVagasPorCodigo(@Param("id") String id, @Param("tipo") String tipo);

	@Query("SELECT distinct post FROM WpPost post where post.postType = 'job_listing' and post.postStatus = 'publish'")
	List<WpPost> obterTodasVagas();
	
	@Query(value = "select * from wp_postmeta where wp_postmeta.post_id in (select wp_posts.ID from wp_posts where wp_posts.post_author = :idEmpresa)", nativeQuery = true)
	Optional<WpPost> consultarDadosPorId(@Param("idEmpresa") String id);

	@Query("select post from WpPost post where post.postType = 'job_application' and post.postAuthor = :idEmpresa")
	List<WpPost> consultarVagasPorIdEmpresa(@Param("idEmpresa") BigInteger idEmpresa);

	@Query(value = "select qtd_inscritos, titulo, sum(codigo) codigo, max(situacao) situacao from (SELECT count(*) qtd_inscritos, wp_postmeta.meta_value titulo, '' codigo, '' situacao FROM wp_postmeta join wp_posts on wp_posts.ID = wp_postmeta.post_id WHERE wp_posts.post_author =:idEmpresa and  wp_postmeta.meta_key = '_job_applied_for' and wp_posts.post_status <> 'trash'  group by wp_postmeta.meta_value union SELECT 0 qtd_inscritos, wp_posts.post_title titulo, wp_posts.ID codigo, wp_posts.post_status situacao FROM wp_postmeta JOIN wp_posts ON wp_posts.ID = wp_postmeta.post_id WHERE wp_posts.post_type = 'job_listing' and wp_posts.post_author =:idEmpresa and wp_posts.post_status <> 'trash'  group by wp_posts.post_title) resultado GROUP by titulo order by qtd_inscritos desc", nativeQuery = true)
	List<Object[]> consultarTotalCandidatosVagasPorIdEmpresa(@Param("idEmpresa") BigInteger idEmpresa);

	@Query(value = "select wp_posts.post_parent codigo_vaga, wp_usermeta.meta_value cpf_candidato, wp_posts.post_title candidato, wp_postmeta.meta_value id_usuario from wp_posts JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id JOIN wp_usermeta on wp_usermeta.user_id = wp_postmeta.meta_value and wp_usermeta.meta_key = 'billing_cpf' where wp_postmeta.meta_key='_candidate_user_id'and wp_posts.post_type= 'job_application' and wp_posts.post_status <> 'trash' and wp_posts.post_parent in (select sum(codigo) codigo from ( SELECT count(*) qtd_inscritos, wp_postmeta.meta_value titulo, '' codigo FROM `wp_postmeta` WHERE wp_postmeta.meta_key = '_job_applied_for' and wp_postmeta.meta_id = wp_postmeta.meta_id group by wp_postmeta.meta_value union SELECT 0 qtd_inscritos, wp_posts.post_title titulo, wp_posts.ID codigo FROM `wp_postmeta` JOIN wp_posts ON wp_posts.ID = wp_postmeta.post_id WHERE wp_posts.post_type = 'job_listing' and wp_posts.post_author =:idEmpresa and wp_posts.post_status <> 'trash' group by wp_posts.post_title) resultado GROUP by titulo)", nativeQuery = true)
	List<Object[]> consultarCandidatosVagasPorIdEmpresa(@Param("idEmpresa") BigInteger idEmpresa);

	@Query(value = "select sum(qtd_inscritos), count(*) tota_vagas from ( select  qtd_inscritos, titulo, sum(codigo) codigo from ( SELECT count(*) qtd_inscritos, wp_postmeta.meta_value titulo, '' codigo FROM `wp_postmeta` join wp_posts on wp_posts.ID = wp_postmeta.post_id WHERE wp_posts.post_author =:idEmpresa and wp_posts.post_status <> 'trash' and  wp_postmeta.meta_key = '_job_applied_for' and wp_postmeta.meta_id = wp_postmeta.meta_id group by wp_postmeta.meta_value union SELECT 0 qtd_inscritos, wp_posts.post_title titulo, wp_posts.ID codigo FROM `wp_postmeta` JOIN wp_posts ON wp_posts.ID = wp_postmeta.post_id WHERE wp_posts.post_type = 'job_listing' and wp_posts.post_author =:idEmpresa and wp_posts.post_status <> 'trash' group by wp_posts.post_title) resultado GROUP by titulo) total order by sum(qtd_inscritos) asc", nativeQuery = true)
	List<Object[]> consultarInformacoesGeraisVagas(@Param("idEmpresa") BigInteger idEmpresa);

	@Query(value = "select * from wp_posts WHERE wp_posts.post_author =  :idEmpresa", nativeQuery = true)
	List<WpPost> consultarPostPorIdEmpresa(@Param("idEmpresa") BigInteger idEmpresa);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE wp_posts set wp_posts.post_title = :novoTitulo where wp_posts.ID = :idEmpresa", nativeQuery = true)
	void atualizarTitulo(@Param("novoTitulo") String novoTitulo, @Param("idEmpresa") BigInteger idEmpresa);	

	@Query(value = "SELECT meta_key, meta_value, meta_id, post_id FROM wp_postmeta WHERE post_id = (select wp_posts.ID from wp_posts JOIN wp_usermeta on wp_usermeta.user_id = wp_posts.post_author WHERE wp_posts.post_author = :idCandidato and wp_usermeta.meta_value = :cpfCandidato and wp_posts.post_status = 'publish' and wp_posts.post_type = 'resume') ORDER BY meta_key,meta_id", nativeQuery = true)
	List<WpPostmeta> obterVagasCandidato(@Param("idCandidato") String idCandidato);
}
