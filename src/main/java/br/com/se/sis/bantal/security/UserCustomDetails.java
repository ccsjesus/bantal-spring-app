package br.com.se.sis.bantal.security;

import br.com.se.sis.bantal.enums.UserStatus;
import br.com.se.sis.bantal.modelo.WpUserAuthenticationEntity;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

@Data
public class UserCustomDetails implements UserDetails {

    private WpUserAuthenticationEntity customer;

    private String id;

    public UserCustomDetails(WpUserAuthenticationEntity customer) {
        this.customer = customer;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return customer.getRoles().stream().map(r -> new SimpleGrantedAuthority(r.getDescription())).collect(Collectors.toList());
    }

    public String getId() {
        return this.customer.getId() + "";
    }

    public String getIdentifier() {
        return this.customer.getIdentifier() + "";
    }

    @Override
    public String getPassword() {
        return customer.getPassword();
    }

    @Override
    public String getUsername() {
        return customer.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return customer.getStatus() == UserStatus.ATIVO;
    }
}
