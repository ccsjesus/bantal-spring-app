package br.com.se.sis.bantal.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import br.com.se.sis.bantal.modelo.WpMensagem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WpMensagemDTO implements Serializable, Comparable<WpMensagemDTO> {

	private String ID;

	private String mensagem;

	private int tipoMensagem;

	private String idVaga;

	private String dataMensagem;

	private String codigoAutorMensagem;
	
	private String nomeAutorMensagem;
	
	private String codigoReferenciaMensagem;
	
	private String lida;


	public String getDataMensagem() {
		if(dataMensagem == null) {
			return LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")); 
		}
		return dataMensagem;
	}

	public WpMensagem toWpMensagem() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		WpMensagem p = new WpMensagem();
		p.setID(this.getID());
		p.setIdVaga(this.getIdVaga());
		p.setCodigoAutorMensagem(this.getCodigoAutorMensagem());
		p.setCodigoReferenciaMensagem(this.getCodigoReferenciaMensagem());
		ZonedDateTime dataHoraZonaEspecifica = ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("America/Sao_Paulo")).minusHours(3);
//		p.setDataMensagem(LocalDateTime.parse(this.getDataMensagem(), formatter));
		p.setDataMensagem(dataHoraZonaEspecifica.toLocalDateTime());
		p.setMensagem(this.getMensagem());
		p.setTipoMensagem(this.getTipoMensagem());
		p.setLida(this.getLida());
		
		return p;
	}
	
	public int compareTo(WpMensagemDTO posts) {
		if (Integer.parseInt(this.ID) < Integer.parseInt(posts.getID())) {
			return -1;
		}
		if (Integer.parseInt(this.ID) > Integer.parseInt(posts.getID())) {
			return 1;
		}
		return 0;
	}
	
}