package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.se.sis.bantal.dto.WpMensagemDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The persistent class for the wp_posts database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_mensagem")
public class WpMensagem implements Serializable, Comparable<WpMensagem> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private String ID;

	@Column(name="mensagem")
	private String mensagem;

	@Column(name="tipo_mensagem")
	private int tipoMensagem;

	@Column(name="id_vaga")
	@JsonBackReference
	private String idVaga;

	@Column(name="data_mensagem")
	private LocalDateTime dataMensagem;

	@Column(name="id_autor")
	private String codigoAutorMensagem;
	
	@Column(name="id_referencia_mensagem")
	private String codigoReferenciaMensagem;
	
	@Column(name="lida")
	private String lida;

	@JsonProperty("ID")
	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}


	public static List<WpMensagemDTO> converterListMensagem(List<WpMensagem> lista) {
		List<WpMensagemDTO> listaPost = new ArrayList<WpMensagemDTO>();

		for (WpMensagem wp : lista) {
			listaPost.add(getMensagemInstance(wp));

		}
		return listaPost;
	}
	
	public static WpMensagemDTO getMensagemInstance(WpMensagem wp) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		WpMensagemDTO p = new WpMensagemDTO();
		p.setID(wp.getID());
		p.setCodigoAutorMensagem(wp.getCodigoAutorMensagem());
		p.setCodigoReferenciaMensagem(wp.getCodigoReferenciaMensagem());
		p.setDataMensagem(wp.getDataMensagem().format(formatter));
		p.setMensagem(wp.getMensagem());
		p.setTipoMensagem(wp.getTipoMensagem());
		p.setLida(wp.getLida());
		
		return p;
	}

	public int compareTo(WpMensagem posts) {
		if (Integer.parseInt(this.ID) < Integer.parseInt(posts.getID())) {
			return -1;
		}
		if (Integer.parseInt(this.ID) > Integer.parseInt(posts.getID())) {
			return 1;
		}
		return 0;
	}

}