package br.com.se.sis.bantal.controller;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.security.UserCustomDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.se.sis.bantal.modelo.WpPost;
import br.com.se.sis.bantal.modelo.WpPostmeta;
import br.com.se.sis.bantal.modelo.WpTokenSistema;
import br.com.se.sis.bantal.repository.PostMetasRepository;
import br.com.se.sis.bantal.repository.PostsRepository;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.repository.VagasRepository;
import br.com.se.sis.bantal.services.CurriculoServices;
import br.com.se.sis.bantal.services.DateService;
import br.com.se.sis.bantal.services.PerfilServices;
import br.com.se.sis.bantal.util.Constantes;
import br.com.se.sis.bantal.util.Util;

@RestController
@RequestMapping("/imagem")
public class ImagemController {

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private CurriculoServices curriculoServices;
	
	@Autowired
	private PerfilServices perfilServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/v2/img-upload", consumes = "multipart/form-data")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> uploadImageV2(Authentication authentication, @RequestParam("imageFile") MultipartFile image)
			throws Exception {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		curriculoServices.uploadImagem(customDetails.getId(), image);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/img-upload-perfil")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> uploadImageCandidato(Authentication authentication, @RequestParam("imageFile") MultipartFile image)
			throws Exception {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		perfilServices.atualizarFotoPerfil(customDetails.getId(), image);
		
		return ResponseEntity.status(201).body(null);
	}
	
}
