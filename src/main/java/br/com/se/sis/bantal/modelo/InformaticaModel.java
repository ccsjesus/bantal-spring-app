package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InformaticaModel {
	
	private String id;
	
    private String tecnologia;
	
	private String nivel;
	
	private String descAtribuicao;
	
	private boolean experiencia;
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof InformaticaModel))
			return false;
		return id != null && id.equals(((InformaticaModel) o).getId());
	}
	
	@Override
	public int hashCode() {
		return 31;
	}

}
