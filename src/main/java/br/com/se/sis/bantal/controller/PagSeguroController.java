package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.pagseguro.domain.NotificacaoPagSeguroDTO;
import br.com.se.sis.pagseguro.domain.PagamentoDTO;
import br.com.se.sis.pagseguro.domain.PreAprovadoDTO;
import br.com.se.sis.pagseguro.domain.RegistroPreAprovado;
import br.com.se.sis.pagseguro.exception.TransacaoAbortadaException;
import br.com.se.sis.pagseguro.services.CheckoutService;
import br.com.se.sis.pagseguro.services.CheckoutTransparenteService;
import br.com.se.sis.pagseguro.services.PreAprovadoService;
import br.com.uol.pagseguro.api.checkout.RegisteredCheckout;
import br.com.uol.pagseguro.api.common.domain.*;
import br.com.uol.pagseguro.api.common.domain.xml.PaymentItemXML;
import br.com.uol.pagseguro.api.preapproval.RegisterPreApprovalResponseXML;
import br.com.uol.pagseguro.api.preapproval.RegisteredPreApproval;
import br.com.uol.pagseguro.api.transaction.search.TransactionDetail;
import br.com.uol.pagseguro.api.transaction.search.TransactionSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/pagseguro")
public class PagSeguroController {
	
	@Autowired
    private CheckoutService checkoutService;

    @Autowired
    private CheckoutTransparenteService checkoutTransparenteService;
    
    @Autowired
    private PreAprovadoService preAprovadoService;
    
    @Autowired
	private TokenRepository tokenRepository;
    
    @GetMapping(value = "/transacoes/datas")
    public ResponseEntity<DataList<? extends TransactionSummary>> buscarTransacoesPorDatas(
    			@RequestParam(value = "dataInicial", required = true) @DateTimeFormat(pattern="yyyy-MM-dd") Date dataInicial,
    			@RequestParam(value = "dataFinal", required = true) @DateTimeFormat(pattern="yyyy-MM-dd") Date dataFinal
    		) throws TransacaoAbortadaException {
    	
    	DataList<? extends TransactionSummary> listaTransacoes = 
    			checkoutTransparenteService.buscarTransacoesPorDatas(dataInicial, dataFinal);
    	return ResponseEntity.status(HttpStatus.OK).body(listaTransacoes);
    }
    
    @GetMapping(value = "/transacao/codigo/{codigo}")
    public ResponseEntity<TransactionDetail> buscarTransacaoPorCodigo(@PathVariable(value = "codigo") String codigo) throws TransacaoAbortadaException {
    	TransactionDetail transacao =  null;
    	if(codigo.equals("Plano gratuito")) {
    		transacao = new TransactionDetail() {
				
				@Override
				public List<? extends Parameter> getParameters() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public String getCode() {
					return "Plano Gratuito";
				}
				
				@Override
				public TransactionType getType() {
					return null;
				}
				
				@Override
				public TransactionStatus getStatus() {
					// TODO Auto-generated method stub
					return new TransactionStatus(TransactionStatus.Status.APPROVED.ordinal());
				}
				
				@Override
				public Shipping getShipping() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public Sender getSender() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public String getReference() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public TransactionPaymentMethod getPaymentMethod() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public String getPaymentLink() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public BigDecimal getNetAmount() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public String getMode() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public TransactionMethod getMethod() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public Date getLastEvent() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public List<? extends PaymentItem> getItems() {
					// TODO Auto-generated method stub
					PaymentItemXML paymento = new PaymentItemXML();
					paymento.setDescription("Plano Master");
					paymento.setId("1");
					paymento.setAmount(BigDecimal.ZERO);
					return Arrays.asList(paymento);
				}
				
				@Override
				public BigDecimal getGrossAmount() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public BigDecimal getFeeAmount() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public BigDecimal getExtraAmount() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public Date getEscrowEndDate() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public BigDecimal getDiscountAmount() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public Date getDate() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public CreditorFee getCreditorFees() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public String getCancellationSource() {
					// TODO Auto-generated method stub
					return null;
				}
			};
    	} else {
    		transacao = checkoutTransparenteService.buscarTransacaoPorCodigo(codigo.replace("-", ""));
    		
    	}
    	
    	return ResponseEntity.status(HttpStatus.OK).body(transacao);
    }
    
    @GetMapping(value = "/transacao/codigoNotificacao/{codigoNotificacao}")
    public ResponseEntity<TransactionDetail> buscarTransacaoPorCodigoNotificacao(@PathVariable("codigoNotificacao") String codigoNotificacao) throws TransacaoAbortadaException {
    	TransactionDetail transacao = checkoutTransparenteService.buscarTransacaoPorCodigoNotificacao(codigoNotificacao);
    	return ResponseEntity.status(HttpStatus.OK).body(transacao);
    }

    /*
     * Primeiro chama o register com as informações do pagamento 
     */
    @PostMapping(value = "/checkout")
	@AllUsersCanOnlyAccessResources
    public ResponseEntity<RegisteredCheckout> registrarCheckout(@RequestBody PagamentoDTO pagamento) throws TransacaoAbortadaException {        
        return ResponseEntity.ok(checkoutService.checkoutRegister(pagamento));
    }
    
    /*
     * Criado pagamento recorrente  
     */
    @PostMapping(value = "/checkout/pre-aprovado")
	@AllUsersCanOnlyAccessResources
    public ResponseEntity<?> preAprovado(Authentication authentication, @RequestBody PreAprovadoDTO preAprovadoDTO) throws TransacaoAbortadaException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		String referencia = UUID.randomUUID().toString();
		preAprovadoDTO.setReferencia(referencia);
		
		RegisteredPreApproval registeredPre = preAprovadoService.criarPreAprovado(preAprovadoDTO);
		//pagService.registrarSolicitacaoPagamento(tokenS.getWpUserTokens().getId(), registeredPre, preAprovadoDTO);
		
		RegistroPreAprovado registro = new RegistroPreAprovado();
		RegisterPreApprovalResponseXML r = (RegisterPreApprovalResponseXML) registeredPre;
		registro.setPreApprovalCode(r.getPreApprovalCode());
		registro.setDate(r.getDate());
		registro.setRedirectURL(r.getRedirectURL());
		registro.setCodeReference(referencia);
		
    	return ResponseEntity.ok(registro);
    }

    @PostMapping(value = "/transparente/boleto-bancario", produces = MediaType.APPLICATION_JSON_VALUE)
	@AllUsersCanOnlyAccessResources
    public ResponseEntity realizarTransacaoTransparenteBoletoBancario(@RequestBody PagamentoDTO pagamentoDTO) throws TransacaoAbortadaException {
        checkoutTransparenteService.criarTransacaoTransparentePorBoletoBancario(pagamentoDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @PostMapping(value = "/transparente/cartao-credito", produces = MediaType.APPLICATION_JSON_VALUE)
	@AllUsersCanOnlyAccessResources
    public ResponseEntity realizarTransacaoTransparenteCartaoCredito(@RequestBody PagamentoDTO pagamentoDTO) throws TransacaoAbortadaException {
        checkoutTransparenteService.criarTransacaoTransparentePorCartaoDeCredito(pagamentoDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @PostMapping(value = "/transparente/debito-online", produces = MediaType.APPLICATION_JSON_VALUE)
	@AllUsersCanOnlyAccessResources
    public ResponseEntity realizarTransacaoTransparenteDebitoOnline(@RequestBody PagamentoDTO pagamentoDTO) throws TransacaoAbortadaException {
        checkoutTransparenteService.criarTransacaoTransparentePorDebitoOnline(pagamentoDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }
    
    @PostMapping(value = "/notificacao", produces = MediaType.APPLICATION_JSON_VALUE)
	@AllUsersCanOnlyAccessResources
    public ResponseEntity receberNotificacao(NotificacaoPagSeguroDTO notificacaoPagSeguroDTO) throws TransacaoAbortadaException {

    	//pagService.registrarTransacaoPagamento(notificacaoPagSeguroDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }


}
