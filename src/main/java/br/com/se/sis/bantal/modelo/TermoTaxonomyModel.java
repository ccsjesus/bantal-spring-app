package br.com.se.sis.bantal.modelo;

import java.math.BigInteger;

public class TermoTaxonomyModel {
	
	private String termId;
	
	private BigInteger parent;
	
	private BigInteger count;
	
	private String taxonomy;

	
	public TermoTaxonomyModel(String termId, BigInteger parent, BigInteger count, String taxonomy) {
		super();
		this.termId = termId;
		this.parent = parent;
		this.count = count;
		this.taxonomy = taxonomy;
	}

	public String getTermId() {
		return termId;
	}

	public void setTermId(String termId) {
		this.termId = termId;
	}

	public BigInteger getParent() {
		return parent;
	}

	public void setParent(BigInteger parent) {
		this.parent = parent;
	}

	public BigInteger getCount() {
		return count;
	}

	public void setCount(BigInteger count) {
		this.count = count;
	}

	public String getTaxonomy() {
		return taxonomy;
	}

	public void setTaxonomy(String taxonomy) {
		this.taxonomy = taxonomy;
	}

	
	

}
