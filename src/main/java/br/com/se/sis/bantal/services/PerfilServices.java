package br.com.se.sis.bantal.services;

import br.com.se.sis.bantal.clients.ClientUsuarioOauthWp;
import br.com.se.sis.bantal.dto.WpPerfilDTO;
import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.exceptions.NotFoundException;
import br.com.se.sis.bantal.modelo.*;
import br.com.se.sis.bantal.repository.WpPerfilRepository;
import br.com.se.sis.bantal.repository.WpUserMetaRepository;
import br.com.se.sis.bantal.util.Constantes;
import br.com.se.sis.bantal.util.FuncaoSistema;
import br.com.se.sis.bantal.util.MetaKeys;
import br.com.se.sis.bantal.util.Util;
import br.com.se.sis.pagseguro.exception.TransacaoAbortadaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Configurable
public class PerfilServices {

	@Autowired
	private WpPerfilRepository perfilRepository;

	@Autowired
	private WpUserMetaRepository wpUserMetaRepository;
	
	@Autowired
	private ClientUsuarioOauthWp clienteUsuarioOuthWp;
	
	@Autowired
	private PlanoPagamentoService planoPagamentoService;

	public WpPerfilDTO cadastrarInformacoesIniciais(WpUser wpUser, Role funcaoSistema) {
		String id = wpUser.getId().toString();
		WpPerfilUsuario perfil = perfilRepository.obterInformacoesUsuarioPorId(id).orElse(null);
		String identificacao = "";
		String nome = "";
		WpPerfilDTO dto = null;

		for (WpUsermeta meta : wpUser.getWpUsermetas()) {
			if (Util.metaKeyExists(meta.getMetaKey(), Constantes.META_COMPANY_CNPJ) ||
				Util.metaKeyExists(meta.getMetaKey(), MetaKeys.META_BILLING_CNPJ)  ||
				Util.metaKeyExists(meta.getMetaKey(), MetaKeys.META_BILLING_CPF) ||
				Util.metaKeyExists(meta.getMetaKey(), MetaKeys.META_CADIDATO_CPF)) {
				identificacao = meta.getMetaValue();
			}
			if (Util.metaKeyExists(meta.getMetaKey(),  MetaKeys.META_FIRST_NAME)) {
				nome = meta.getMetaValue();
			}
			if(!identificacao.isEmpty() && !nome.isEmpty()) {
				break;
			}
		}
		if (perfil == null) {
			perfil = new WpPerfilUsuario();
			perfil.setUserId(id);
			perfil.setNome(nome);
			perfil.setDisplayName(wpUser.getDisplayName());
			perfil.setIdentificacao(identificacao);
			perfil.setEmail(wpUser.getUserEmail());
			perfil.setPerfil(funcaoSistema.name());
//			perfil.setFoto(usuario.getIm);
//			perfil.setPlanoPagamento(listaPlanoPagamento);
			perfilRepository.saveAndFlush(perfil);
			dto = this.convertToWpPerfilDTO(perfil);
		} else {
			dto = new WpPerfilDTO();
			dto.setId(perfil.getId());
			dto.setEmail(perfil.getEmail());
			dto.setIdentificacao(perfil.getIdentificacao());
			dto.setNome(perfil.getNome());
			dto.setDisplayNome(perfil.getDisplayName());
			dto.setUserId(perfil.getUserId());
			dto.setPerfil(perfil.getPerfil());
			dto.setPlanoPagamento(planoPagamentoService.convertFromListWpPlanoPagamentoDTO(perfil.getPlanoPagamento()));
		}
		
		planoPagamentoService.registrarPlanoInicialPagamento(id, funcaoSistema);
		return dto;
	}

	public Role obterFuncaoSistema(String idUsuarioLogado) {
		UserMeta userMeta = wpUserMetaRepository.obterDadosUsuario(new BigDecimal(idUsuarioLogado));

		return Util.getFuncaoSistema(userMeta);
	}
	
	public List<WpPerfilDTO> obterPerfisPorFuncaoSistema(Role funcaoSistema) {
		List<WpPerfilUsuario> listaPerfil = perfilRepository.obterPerfisPorFuncaoSistema(funcaoSistema.name());
		
		return listaPerfil.stream().map(perfilDTO -> this.convertToWpPerfilDTO(perfilDTO)).collect(Collectors.toList());
	}
	
	private WpPerfilDTO convertToWpPerfilDTO(WpPerfilUsuario perfilList) {
		WpPerfilDTO dto = new WpPerfilDTO();
		dto.setId(perfilList.getId());
		dto.setEmail(perfilList.getEmail());
		dto.setIdentificacao(perfilList.getIdentificacao());
		dto.setNome(perfilList.getNome());
		dto.setDisplayNome(perfilList.getDisplayName());
		dto.setUserId(perfilList.getUserId());
		dto.setPerfil(perfilList.getPerfil());
		dto.setFoto(perfilList.getFoto());
		dto.setPlanoPagamento(planoPagamentoService.convertFromListWpPlanoPagamentoDTO(perfilList.getPlanoPagamento()));
		return dto;
	}

	public WpPerfilDTO obterPerfilUsuario(String idUsuarioLogado) {
		WpPerfilUsuario perfilList = perfilRepository.obterInformacoesUsuarioPorId(idUsuarioLogado).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND));
		
		return convertToWpPerfilDTO(perfilList);
	}

	@Transactional
	public WpPerfilUsuario obterWpPerfilUsuario(String idUsuarioLogado) {
		return perfilRepository.obterInformacoesUsuarioPorId(idUsuarioLogado).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(),ErrorsMessages.USER_NOT_FOUND));
	}
	
	public void atualizarNomeUsuario(String idUsuarioLogado, String nome) {
		WpPerfilUsuario perfilList = perfilRepository.obterInformacoesUsuarioPorId(idUsuarioLogado).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(),ErrorsMessages.USER_NOT_FOUND));

		perfilList.setNome(nome);
	}
	
	public boolean isUsuarioLogadoCandidato(String idUsuarioLogado) {
		WpPerfilUsuario wpPerfilUsuario = perfilRepository.obterInformacoesUsuarioPorId(idUsuarioLogado).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(),ErrorsMessages.USER_NOT_FOUND));
		return wpPerfilUsuario.getPerfil().equalsIgnoreCase(FuncaoSistema.FUNCAO_CANDIDATO.getValor().toUpperCase());
	}
	
	public void atualizarFotoPerfil(String idLogado, MultipartFile file) {
		WpPerfilUsuario perfilList = perfilRepository.obterInformacoesUsuarioPorId(idLogado).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(),ErrorsMessages.USER_NOT_FOUND));
		
		try {
			perfilList.setFoto(Util.compressBytes(file.getBytes()));
			perfilRepository.save(perfilList);
		} catch (IOException e) {
			e.printStackTrace();
			throw new TransacaoAbortadaException("Erro ao realizar upload!");
		}
	}
	
	public void atualizarDisplayNomeUsuario(String idUsuarioLogado, String displayName) {
		WpPerfilUsuario perfilList = perfilRepository.obterInformacoesUsuarioPorId(idUsuarioLogado).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND));

		perfilList.setDisplayName(displayName);

	}
	
	public void atualizarEmail(String idUsuarioLogado, String email) {
		WpPerfilUsuario perfilList = perfilRepository.obterInformacoesUsuarioPorId(idUsuarioLogado).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND));

		perfilList.setEmail(email);

	}
	
	public void atualizarSenha(String idUsuarioLogado, Usuario usuario) throws TransacaoAbortadaException {

		//alterando senha 
		ClientUsuarioOauthWp oauth = null;
		if(usuario != null 
				&& !"".equals(usuario.getNewSenha())
				&& !"".equals(usuario.getUserPass())
		   ) {
			WpUser user = new WpUser();
			user.setId(new BigDecimal(idUsuarioLogado));
			user.setUserPass(usuario.getUserPass());
			user.setNewSenha(usuario.getNewSenha());
			oauth =  clienteUsuarioOuthWp.atualizarSenhaWp(user);

			if(oauth == null || oauth.getUser_pass() == null ) {
				throw new TransacaoAbortadaException("Senha inválida!");
			}
		}
	}

    public WpPerfilDTO findByIdentificacao(String identificacao) {
		return WpPerfilDTO.toDTO(perfilRepository.findByIdentificacao(identificacao).orElseThrow(() -> new NotFoundException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND)));
    }
}