package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the wp_term_taxonomy database table.
 * 
 */
@Entity
@Table(name="wp_term_taxonomy")
public class WpTermTaxonomy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="term_taxonomy_id")
	private String termTaxonomyId;

	private BigInteger count;

	@Lob
	private String description;

	private BigInteger parent;

	private String taxonomy;

	//bi-directional many-to-one association to WpTerm
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="term_id")
	@JsonBackReference
	private WpTerm wpTerm;

	//bi-directional many-to-one association to WpTermRelationship
	@OneToMany(mappedBy="wpTermTaxonomy", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpTermRelationship> wpTermRelationships;

	public WpTermTaxonomy() {
	}

	public String getTermTaxonomyId() {
		return this.termTaxonomyId;
	}

	public void setTermTaxonomyId(String termTaxonomyId) {
		this.termTaxonomyId = termTaxonomyId;
	}

	public BigInteger getCount() {
		return this.count;
	}

	public void setCount(BigInteger count) {
		this.count = count;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigInteger getParent() {
		return this.parent;
	}

	public void setParent(BigInteger parent) {
		this.parent = parent;
	}

	public String getTaxonomy() {
		return this.taxonomy;
	}

	public void setTaxonomy(String taxonomy) {
		this.taxonomy = taxonomy;
	}

	public WpTerm getWpTerm() {
		return this.wpTerm;
	}

	public void setWpTerm(WpTerm wpTerm) {
		this.wpTerm = wpTerm;
	}

	public List<WpTermRelationship> getWpTermRelationships() {
		return this.wpTermRelationships;
	}

	public void setWpTermRelationships(List<WpTermRelationship> wpTermRelationships) {
		this.wpTermRelationships = wpTermRelationships;
	}

	public WpTermRelationship addWpTermRelationship(WpTermRelationship wpTermRelationship) {
		getWpTermRelationships().add(wpTermRelationship);
		wpTermRelationship.setWpTermTaxonomy(this);

		return wpTermRelationship;
	}

	public WpTermRelationship removeWpTermRelationship(WpTermRelationship wpTermRelationship) {
		getWpTermRelationships().remove(wpTermRelationship);
		wpTermRelationship.setWpTermTaxonomy(null);

		return wpTermRelationship;
	}

}