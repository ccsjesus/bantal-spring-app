package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_inscricao")
public class WpInscricaoVaga implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private String id;
	
	@Column(name="user_id")
	private String userId;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_vaga")
	private WpVagas wpVagas;
	
	@Column(name="comentario")
	private String comentario;
	
	@Column(name="data_inscricao")
	private LocalDateTime dataInscricao;
	
}