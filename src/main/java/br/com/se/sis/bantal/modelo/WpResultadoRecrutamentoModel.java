package br.com.se.sis.bantal.modelo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class WpResultadoRecrutamentoModel {
	
	private int qtd_inscritos;

	private String titulo;

	private int codigo;
	
	private String situacao;
	
	private List<WpCandidatoModel> candidatos;
	
	private Vagas detalhesVaga;

	public int getQtd_inscritos() {
		return qtd_inscritos;
	}

	public void setQtd_inscritos(int qtd_inscritos) {
		this.qtd_inscritos = qtd_inscritos;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public List<WpCandidatoModel> getCandidatos() {
		if(this.candidatos == null ) {
			this.candidatos = new ArrayList<WpCandidatoModel>();
		}
		return candidatos;
	}

	public void setCandidatos(List<WpCandidatoModel> listaCandidatoModels) {
		this.candidatos = listaCandidatoModels;
	}

	public static List<WpResultadoRecrutamentoModel> retornarWpResultadoRecrutamento(List<Object[]> pVagas, List<Object[]> pCandidatos){
		List<WpResultadoRecrutamentoModel> lista = new ArrayList<>();
		WpResultadoRecrutamentoModel wpResult = null;
		for (Object[] obj : pVagas) {
			wpResult = new WpResultadoRecrutamentoModel();
			wpResult.setQtd_inscritos(new BigDecimal(obj[0].toString()).toBigInteger().intValue());
			wpResult.setTitulo(obj[1].toString());
			wpResult.setCodigo(new BigDecimal(obj[2].toString()).toBigInteger().intValue());
			wpResult.setSituacao(obj[3].toString());
			
			for (Object[] obj2 : pCandidatos) {
				int codigoVaga = new BigDecimal(obj2[0].toString()).toBigInteger().intValue();
				
				if(wpResult.getCodigo() == codigoVaga) {
					wpResult.getCandidatos().add(WpCandidatoModel.convetToWpCandidatoModel(obj2));
				}
			}
			lista.add(wpResult);
		}
		
		return lista;
	}

	public Vagas getDetalhesVaga() {
		return detalhesVaga;
	}

	public void setDetalhesVaga(Vagas detalhesVaga) {
		this.detalhesVaga = detalhesVaga;
	}
	
	

 }

