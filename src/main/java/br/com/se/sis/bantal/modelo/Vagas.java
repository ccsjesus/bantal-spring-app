package br.com.se.sis.bantal.modelo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.se.sis.bantal.util.Constantes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Vagas {
	
	private String  cdVaga;
    private String  titulo;
    private String  localizacao;  
    private int  idTipoVaga;
    private String  tipoVaga;  
    private String  area;  
    private String  descricao;  
    private String  email;  
    private String  dataLimite; 
    private String  vagas;
    private String  qtdVagasAbertas; 
    private String  totalInscritos; 
    private String  usuario;
    private String  situacao;
	private String salario;    
    private int idAreaAtuacao;
	private String nomeAreaAtuacao;
	private String slugAreaAtuacao;
	private String estado;
	private String cidade;
	private byte [] foto;
	private boolean inscrito;

	
	public Vagas addTipoVaga(Term t, Vagas v) {
		v.setIdTipoVaga(t.getTermId());
	    v.setTipoVaga(t.getName());  
		return v;
	}
	
	public Vagas addAreaAtuacaoVaga(Term t, Vagas v) {
		v.setIdAreaAtuacao(t.getTermId());
	    v.setNomeAreaAtuacao(t.getName());  
		return v;
	}
	
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Vagas)) {
			return false;
		}
		Vagas castOther = (Vagas)other;
		return 
			this.cdVaga.equals(castOther.cdVaga);
	}
	
	@Override
	public int hashCode() {
		return 31;
	}
}
	
