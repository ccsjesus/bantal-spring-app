package br.com.se.sis.bantal.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpPostmeta;

public interface PostMetasRepository extends JpaRepository<WpPostmeta, String>, JpaSpecificationExecutor<WpPostmeta> {

	@Modifying(clearAutomatically = true)
	@Query(value = "update wp_postmeta set wp_postmeta.meta_value = :novoTitulo where wp_postmeta.meta_key = '_job_applied_for' and wp_postmeta.post_id = :idPost", nativeQuery = true)
	void atualizarTituloVagaMeta(@Param("novoTitulo") String novoTitulo, @Param("idPost") BigInteger idEmpresa);

	@Query(value = "select * from wp_postmeta where wp_postmeta.meta_key = '_candidate_user_id' and wp_postmeta.meta_value = :idCandidato and wp_postmeta.post_id in (:lista)", nativeQuery = true)
	List<WpPostmeta> obterVagasPorListaIdPost(@Param("lista") List<String> lista, @Param("idCandidato") String idCandidato);
	
	@Query(value = "select * from wp_postmeta where wp_postmeta.meta_key = '_candidate_user_id' and wp_postmeta.meta_value = :idCandidato", nativeQuery = true)
	List<WpPostmeta> obterInscricoesCandidatoPorId(@Param("idCandidato") String idCandidato);
}
