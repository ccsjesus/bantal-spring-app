package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AreaAtuacaoModel {
	
	private String termId;
	
	private String name;
	
	private String slug;
	
	private int habilitada;
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof AreaAtuacaoModel))
			return false;
		return termId != null && termId.equals(((AreaAtuacaoModel) o).getTermId());
	}
	
	@Override
	public int hashCode() {
		return 31;
	}
	

}
