package br.com.se.sis.bantal.controller.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class TokenRefreshResponse {

    @NonNull
    private String accessToken;

    @NonNull
    private String refreshToken;

    private String tokenType = "Bearer";
}
