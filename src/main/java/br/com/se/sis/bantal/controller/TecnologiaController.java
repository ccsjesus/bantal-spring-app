package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.UserCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.CurriculoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/v1/tecnologia")
public class TecnologiaController {

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private CurriculoServices curriculoServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/adicionar-sem-experiencia")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> adicionarExperienciaCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		curriculoServices.adicionarTecnologiaSemExperiencia(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);

	}
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-sem-experiencia")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> updateExperienciaCandidato(Authentication authentication, @RequestBody CurriculoModel curriculo)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		curriculoServices.atualizarTecnologiaSemExperiencia(customDetails.getId(), curriculo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
}
