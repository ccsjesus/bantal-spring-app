package br.com.se.sis.bantal.util;

public enum Constantes {

	/*
	 * Constantes do Wordpress para inserir mensagem
	 */
	MSG_LIDA("1"), 
	MSG_NAO_LIDA("0"), 
	
	AUTHORIZATION("authorization"), 
	TOKEN("token"), 

	ERROR_MSG("error_msg"),
	SUCCESS_MSG("success_msg"),
	
	/*
	 * Constantes do Wordpress para inserir
	 */
	
	META_FILLED("_filled"),
	META_FEATURED("_featured"),
	META_JOB_TITLE("_job_title"),
	META_JOB_LOCATION("_job_location"),
	META_JOB_DESCRIPTON("_job_description"),
	META_APPLICATION("_application"),
	META_APPLICATION_DEADLINE("_application_deadline"),
	META_JOB_DEADLINE("_job_deadline"),
	META_JOB_SALARY("_job_salary"),
	META_COMPANY_NAME("_company_name"),
	META_COMPANY_WEBSITE("_company_website"),
	META_COMPANY_TAGLINE("_company_tagline"),
	META_COMPANY_VIDEO("_company_video"),
	META_THUMBNAIL_ID("_thumbnail_id"),
	META_COMPANY_NOME_FANTASIA("_company_nome_fantasia"),
	META_COMPANY_RAZAO_SOCIAL("_company_razao_social"),
	META_COMPANY_CNPJ("_company_cnpj"),
	META_COMPANY_LOGOMARCA("_company_logomarca"),
	META_JOB_AREA_ATUACAO("_job_area_de_atuacao"),
	META_EDIT_LOCK("_edit_lock"),
	META_SUBMITTING_KEY("_submitting_key"),
	META_JOB_EXPIRES("_job_expires"),
	META_TRACKED_SUBMITTED("_tracked_submitted"),
	META_EMPRESA_PERFIL_PHOTO("_company_perfil_photo"),
	META_ELEMENTOR_CONTROLS_USAGE("_elementor_controls_usage"),
	
	
	/*
	 * Constantes do Wordpress para atualizar dados pessoais
	 */
	
	META_CADIDATO_CARGO("_candidate_cargo"),
	META_CADIDATO_CELULAR("_candidate_celular"),
	META_CADIDATO_CIVIL("_candidate_civil"),
	META_CADIDATO_CNH("_candidate_cnh"),
	META_CADIDATO_CPF("_candidate_cpf"),
	META_CADIDATO_CURSOS("_candidate_cursos"),
	META_CADIDATO_EDUCATION("_candidate_education"),
	META_CADIDATO_EMAIL("_candidate_email"),
	META_CADIDATO_EXPERIENCE("_candidate_experience"),
	META_CADIDATO_FILHOS("_candidate_filhos"),
	META_CADIDATO_FORMACAO("_candidate_formacao"),
	META_CADIDATO_FUNCAO("_candidate_funcao"),
	META_CADIDATO_HABILIDADES("_candidate_habilidades"),
	META_CADIDATO_IDADE("_candidate_idade"),
	META_CADIDATO_IDIOMAS("_candidate_idiomas"),
	META_CADIDATO_LINKEDIN("_candidate_linkedin"),
	META_CADIDATO_LOCATION("_candidate_location"),
	META_CADIDATO_MAIL("_candidate_email"),
	META_CADIDATO_NACIONALIDADE("_candidate_nacionalidade"),
	META_CADIDATO_NAME("_candidate_name"),
	META_CADIDATO_NIVEL("_candidate_nivel"),
	META_CADIDATO_OBJETIVOS("_candidate_objetivos"),
	META_CADIDATO_PHONE("_candidate_phone"),
	META_CADIDATO_PHOTO("_candidate_photo"),
	META_PERFIL_PHOTO("_perfil_photo_informacoes"),
	META_CADIDATO_RECADOS("_candidate_recados"),
	META_CADIDATO_RG("_candidate_rg"),
	META_CADIDATO_SALARIO("_candidate_salario"),
	META_CADIDATO_SEM_EXP("_candidato_sem_exp"),
	META_CADIDATO_APRESENTACAO("_new_resume_content"),
	META_CADIDATO_USER_ID("_candidate_user_id"),
	META_JOB_APPLIED_FOR("_job_applied_for"),
	META_BILLING_NEIGHBORHOOD("billing_neighborhood"),
	META_BILLING_NUMBER("billing_number"),
	META_CANDIDATO_CIDADE("_candidate_cidade"),
	META_CANDIDATO_ESTADO("_candidate_estado"),
	META_BILLING_LAST_NAME("billing_last_name"),
	META_BILLING_POSTCODE("billing_postcode"),
	META_LAST_NAME("last_name"),
	META_DESCRIPTION("description"),
	META_LAST_ACTIVE("_last_active");
	
	

	private final String descricao;

	private Constantes(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false 
        return descricao.equals(otherName);
    }

    public String toString() {
       return this.descricao;
    }
}