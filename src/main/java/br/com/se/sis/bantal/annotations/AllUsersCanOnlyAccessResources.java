package br.com.se.sis.bantal.annotations;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@PreAuthorize("hasAnyRole('ROLE_CANDIDATE','ROLE_EMPLOYER','ROLE_ADMIN')")
public @interface AllUsersCanOnlyAccessResources {
}
