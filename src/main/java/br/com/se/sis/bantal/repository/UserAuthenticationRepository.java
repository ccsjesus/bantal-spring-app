package br.com.se.sis.bantal.repository;

import br.com.se.sis.bantal.modelo.WpUserAuthenticationEntity;
import br.com.se.sis.bantal.security.UserCustomDetails;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserAuthenticationRepository extends CrudRepository<WpUserAuthenticationEntity, Integer> {

    WpUserAuthenticationEntity findByNameContaining(String name);

    boolean existsByEmail(String email);

    WpUserAuthenticationEntity findByEmail(String email);

    Optional<WpUserAuthenticationEntity> findByIdentifier(String identifier);
}
