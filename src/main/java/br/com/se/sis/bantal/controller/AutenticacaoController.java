package br.com.se.sis.bantal.controller;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import br.com.se.sis.bantal.controller.requests.RegisterUserRequest;
import br.com.se.sis.bantal.controller.requests.ResponseToken;
import br.com.se.sis.bantal.controller.requests.TokenRefreshRequest;
import br.com.se.sis.bantal.controller.response.TokenRefreshResponse;
import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.exceptions.RegisterUserRequestException;
import br.com.se.sis.bantal.exceptions.TokenExpiredException;
import br.com.se.sis.bantal.modelo.*;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.PerfilServices;
import br.com.se.sis.bantal.services.RefreshTokenService;
import br.com.se.sis.bantal.services.UserDetailCustomService;
import io.jsonwebtoken.impl.DefaultClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import br.com.se.sis.bantal.clients.ClientUsuarioOauthWp;
import br.com.se.sis.bantal.repository.AcessoRepository;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.JWTUtil;
import br.com.se.sis.bantal.services.UsuarioService;

@RestController
@RequestMapping("/auth")
public class AutenticacaoController {

	@Autowired
	private AcessoRepository acessoRepository;
	
	@Autowired
	private TokenRepository tokenRepository;
	
	@Autowired
	private ClientUsuarioOauthWp clienteUsuarioOuthWp;
	
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UserDetailCustomService userDetailCustomService;

	@Autowired
	private PerfilServices perfilServices;

	@Autowired
	private JWTUtil jwtUtil;

	@Autowired
	private RefreshTokenService refreshTokenService;

	@PostMapping("/register")
	public ResponseEntity<Object> register(@RequestBody @Valid RegisterUserRequest postUserRequest) throws RegisterUserRequestException {
		userDetailCustomService.registerUser(WpUserAuthenticationEntity.toAuthenticationEntity(postUserRequest));
		return ResponseEntity.status(HttpStatus.CREATED).body("Ok");
	}

	@PostMapping("/refreshtoken")
	public ResponseEntity<?> refreshtoken(@Valid @RequestBody TokenRefreshRequest request) throws TokenExpiredException {
		String requestRefreshToken = request.getRefreshToken();

		return refreshTokenService.findByToken(requestRefreshToken)
				.map(refreshTokenService::verifyExpiration)
				.map(RefreshToken::getUser)
				.map(user -> {
					String token = jwtUtil.generateToken(user.getIdentifier());
					return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
				})
				.orElseThrow(() -> new TokenExpiredException(ErrorsMessages.REFRESH_TOKEN_EXPIRED.getDescription(),
						ErrorsMessages.REFRESH_TOKEN_EXPIRED));
	}

	@Transactional
	@Deprecated
	@RequestMapping(consumes="application/json", method = RequestMethod.POST , value = "/login")
	public ResponseEntity<Object> logIn(@RequestBody WpUser userAccess) throws IllegalArgumentException, UnsupportedEncodingException {

		ClientUsuarioOauthWp client = clienteUsuarioOuthWp.obterUsuarioWp(userAccess);

		if(client == null ) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cliente não encontrado ou Email não registrado.");
		}
		if(client.getUser_email() == null)
		{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cliente não encontrado ou Email não registrado.");	
		}
		
		WpUser acessoAutorizado = acessoRepository.consultarAcessoAutorizado(client.getUser_email(),client.getUser_pass());

		if(acessoAutorizado == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("CNPJ / Senha inválido(s)");
		}
		
		WpTokenSistema tokenSistemaGerado = null;
		if (acessoAutorizado.getWpUserTokens() != null && acessoAutorizado.getWpUserTokens().size() >= 0) {
			
			List<WpTokenSistema> tokens = tokenRepository.consultarTokenAutorizado(acessoAutorizado.getId(), 0);
			
			for(WpTokenSistema tks : tokens) {
				tks.setTokenStatus(1);
				acessoAutorizado.removeWpTokenSistema(tks);
				tokenRepository.save(tks);				
			}
			
			//Gero um novo token
			tokenSistemaGerado = new WpTokenSistema();
			tokenSistemaGerado.setToken(jwtUtil.generateToken(acessoAutorizado.getId().toString()));
			tokenSistemaGerado.setTokenRegistered(LocalDateTime.now());
			tokenSistemaGerado.setTokenStatus(0);
			tokenSistemaGerado.setWpUserTokens(acessoAutorizado);
			
			acessoAutorizado.addWpTokenSistema(tokenSistemaGerado);
			tokenRepository.save(tokenSistemaGerado);
			
		} else {
			//Gero um novo token
			tokenSistemaGerado = new WpTokenSistema();
			tokenSistemaGerado.setToken(jwtUtil.generateToken(acessoAutorizado.getId().toString()));
			tokenSistemaGerado.setTokenRegistered(LocalDateTime.now());
			tokenSistemaGerado.setTokenStatus(0);
			tokenSistemaGerado.setWpUserTokens(acessoAutorizado);
			
			acessoAutorizado.addWpTokenSistema(tokenSistemaGerado);
			tokenRepository.save(tokenSistemaGerado);
		}
		
		Usuario usuario = usuarioService.cadastrarInformacoesIniciais(acessoAutorizado.getId().toString(), acessoAutorizado);
		
		return ResponseEntity.ok(usuario);
	}
	
	
	
}
