package br.com.se.sis.bantal.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpUsermeta;

public interface UsuarioRepository extends JpaRepository<WpUsermeta, String>, JpaSpecificationExecutor<WpUsermeta> {


	@Query(value=" Select * from wp_usermeta wp_usermeta where wp_usermeta.user_id = ( SELECT m.user_id from wp_usermeta m where (m.meta_key = 'cpf' or m.meta_key = 'cnpj') and m.meta_value <> '' and m.user_id = :id)", nativeQuery = true)
	List<WpUsermeta> obterInformacoesUsuarioPorId(@Param("id") String id);
	
}
