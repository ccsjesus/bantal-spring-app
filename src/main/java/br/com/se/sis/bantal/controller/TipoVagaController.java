package br.com.se.sis.bantal.controller;

import javax.transaction.Transactional;

import br.com.se.sis.bantal.annotations.UserEmployerAndAdminCanOnlyAccessResources;
import br.com.se.sis.bantal.security.UserCustomDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.modelo.TipoVagaModel;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.TipoVagaServices;

@RestController
@RequestMapping("/v1/tipo-vaga")
public class TipoVagaController {


	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private TipoVagaServices tipoVagaServices;
	
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> getAllTipoVaga(Authentication authentication) throws IllegalArgumentException {

		return ResponseEntity.ok(tipoVagaServices.obterTodosTipoVaga());
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/habilitadas/")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> getTipoVaga() throws IllegalArgumentException {
		return ResponseEntity.ok(tipoVagaServices.obterTodosTipoVagaHabilitadas());
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/desabilitadas/")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> getTipoVagaHabilitada()
			throws IllegalArgumentException {

		return ResponseEntity.ok(tipoVagaServices.obterTodosTipoVagaDesabilitadas());
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/incluir")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> incluirTipoVaga(Authentication authentication, @RequestBody TipoVagaModel tipo) throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		tipoVagaServices.incluirNovoTipoVaga(customDetails.getId(), tipo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/desabilitar/{id}")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> excluirVaga(Authentication authentication, @PathVariable("id") String idTipoVaga)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		tipoVagaServices.excluirNovoTipoVaga(customDetails.getId(), idTipoVaga);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/habilitar/{id}")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> habilitarTipoVaga(Authentication authentication, @PathVariable("id") String idTipoVaga)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

			tipoVagaServices.habilitarNovoTipoVaga(customDetails.getId(), idTipoVaga);
			return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
}
