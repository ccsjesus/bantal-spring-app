package br.com.se.sis.bantal.modelo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_area_atuacao")
public class WpAreaAtuacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private String id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="id_curriculo", insertable = false, updatable = false)
	@JsonBackReference
	private WpCurriculos wpCurriculosAreaAtuacao;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="id_area_atuacao", insertable = false, updatable = false)
	@JsonBackReference
	private WpAreaAtuacaoParametro wpAreaAtuacaoParametro;

	public WpAreaAtuacao(WpAreaAtuacaoParametro wpAreaAtuacaoParametro, WpCurriculos wpCurriculos) {
		super();
		this.wpAreaAtuacaoParametro = wpAreaAtuacaoParametro;
		this.wpCurriculosAreaAtuacao = wpCurriculos;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WpAreaAtuacao))
			return false;
		return id != null && id.equals(((WpAreaAtuacao) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
	
}