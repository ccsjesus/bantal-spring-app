package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Entity
@Table(name="wp_users")
@Builder(setterPrefix = "with")
@AllArgsConstructor
public class WpUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="display_name")
	private String displayName;

	@Column(name="user_activation_key")
	private String userActivationKey;

	@Column(name="user_email")
	private String userEmail;

	@Column(name="user_login")
	private String userLogin;

	@Column(name="user_nicename")
	private String userNicename;

	@Column(name="user_pass")
	private String userPass;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="user_registered")
	private Date userRegistered;

	@Column(name="user_status")
	private int userStatus;

	@Column(name="user_url")
	private String userUrl;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private BigDecimal id;

	//bi-directional many-to-one association to WpUsermeta
	@OneToMany(mappedBy="wpUser", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpUsermeta> wpUsermetas;
	
	//bi-directional many-to-one association to WpUsermeta
	@OneToMany(mappedBy="wpUserTokens", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Where(clause = "token_status = 0")
	@JsonManagedReference
	private Set<WpTokenSistema> wpUserTokens;
	
	@Transient
	private String newSenha;
	
	public WpUser() {
	}

	
	
	public String getNewSenha() {
		return newSenha;
	}



	public void setNewSenha(String newSenha) {
		this.newSenha = newSenha;
	}



	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getUserActivationKey() {
		return this.userActivationKey;
	}

	public void setUserActivationKey(String userActivationKey) {
		this.userActivationKey = userActivationKey;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserLogin() {
		return this.userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public String getUserNicename() {
		return this.userNicename;
	}

	public void setUserNicename(String userNicename) {
		this.userNicename = userNicename;
	}

	public String getUserPass() {
		return this.userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}

	public Date getUserRegistered() {
		return this.userRegistered;
	}

	public void setUserRegistered(Date userRegistered) {
		this.userRegistered = userRegistered;
	}

	public int getUserStatus() {
		return this.userStatus;
	}

	public void setUserStatus(int userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserUrl() {
		return this.userUrl;
	}

	public void setUserUrl(String userUrl) {
		this.userUrl = userUrl;
	}

	public List<WpUsermeta> getWpUsermetas() {
		return this.wpUsermetas;
	}

	public void setWpUsermetas(List<WpUsermeta> wpUsermetas) {
		this.wpUsermetas = wpUsermetas;
	}

	public WpUsermeta addWpUsermeta(WpUsermeta wpUsermeta) {
		getWpUsermetas().add(wpUsermeta);
		wpUsermeta.setWpUser(this);

		return wpUsermeta;
	}

	public WpUsermeta removeWpUsermeta(WpUsermeta wpUsermeta) {
		getWpUsermetas().remove(wpUsermeta);
		wpUsermeta.setWpUser(null);

		return wpUsermeta;
	}
	
	public Set<WpTokenSistema> getWpUserTokens() {
		return wpUserTokens;
	}

	public void setWpUserTokens(Set<WpTokenSistema> wpUserTokens) {
		this.wpUserTokens = wpUserTokens;
	}
	
	public WpTokenSistema addWpTokenSistema(WpTokenSistema wpTokenUsermeta) {
		getWpUserTokens().add(wpTokenUsermeta);
		wpTokenUsermeta.setWpUserTokens(this);

		return wpTokenUsermeta;
	}

	public WpTokenSistema removeWpTokenSistema(WpTokenSistema wpTokenUsermeta) {
		getWpUserTokens().remove(wpTokenUsermeta);
		wpTokenUsermeta.setWpUserTokens(null);

		return wpTokenUsermeta;
	}

	public BigDecimal getId() {
		return id;
	}
	
	public void setId(BigDecimal id) {
		this.id = id;
	}

}