package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_idiomas")
public class WpIdioma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_idioma")
	private String id;

	@Column(name="idioma")
	private String idioma;

	@Column(name="nivel_escrita")
	private String nivelEscrita;
	
	@Column(name="nivel_conversacao")
	private String nivelConversacao;
	
	@Column(name="nivel_leitura")
	private String nivelLeitura;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="id_curriculo", insertable = false, updatable = false)
	@JsonBackReference
	private WpCurriculos wpCurriculosIdiomas;

	public WpIdioma(String idioma, String nivelEscrita, String nivelConversacao, String nivelLeitura,
			WpCurriculos wpCurriculosIdiomas) {
		super();
		this.idioma = idioma;
		this.nivelEscrita = nivelEscrita;
		this.nivelConversacao = nivelConversacao;
		this.nivelLeitura = nivelLeitura;
		this.wpCurriculosIdiomas = wpCurriculosIdiomas;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WpIdioma))
			return false;
		return id != null && id.equals(((WpIdioma) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
	
	public static List<IdiomaModel> fromToModel(List<WpIdioma> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new IdiomaModel(wpFormacao.getId(), wpFormacao.getIdioma(), wpFormacao.getNivelEscrita(), wpFormacao.getNivelConversacao(), wpFormacao.getNivelLeitura()))
		        .collect(Collectors.toList());
	}
	
	public static List<WpIdioma> fromToWpEntity(List<IdiomaModel> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new WpIdioma(wpFormacao.getId(), wpFormacao.getIdioma(), wpFormacao.getEscrita(), wpFormacao.getConversacao(), wpFormacao.getLeitura(), null))
		        .collect(Collectors.toList());
	}


}