package br.com.se.sis.bantal.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpOption;

public interface OptionsRepository extends JpaRepository<WpOption, String>, JpaSpecificationExecutor<WpOption> {

	@Query(value="select * from wp_options WHERE wp_options.option_value like :idPost limit 1", 
			  nativeQuery = true)
	WpOption obterOptionsPorPost(@Param("idPost") String idPost);
	
}
