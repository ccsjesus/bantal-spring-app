package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the wp_users database table.
 * 
 */
/**
 * @author ccsjesus
 *
 *
 */

@Entity
@Table(name="wp_tokens")
public class WpTokenSistema implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="token_registered")
	private LocalDateTime tokenRegistered;

	@Column(name="token_status")
	private int tokenStatus;
	
	@Column(name="token")
	private String token;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private BigDecimal id;

	//bi-directional many-to-one association to WpUsermeta
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "ID")
    @JsonBackReference
	private WpUser wpUserTokens;

	public WpTokenSistema() {
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}

	public LocalDateTime getTokenRegistered() {
		return tokenRegistered;
	}

	public void setTokenRegistered(LocalDateTime tokenRegistered) {
		this.tokenRegistered = tokenRegistered;
	}

	public int getTokenStatus() {
		return tokenStatus;
	}

	public void setTokenStatus(int tokenStatus) {
		this.tokenStatus = tokenStatus;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public WpUser getWpUserTokens() {
		return wpUserTokens;
	}

	public void setWpUserTokens(WpUser wpUserTokens) {
		this.wpUserTokens = wpUserTokens;
	}
	 

}