package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VagasDisponiveisDTO {
	
	private String userId;
	private String nomeEmpresa;
	private String idVaga;
	private String tituloVaga;
	private String dataVaga;
	private String localizacaoVaga;
	private String sigla;
	
}