package br.com.se.sis.bantal.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpPostmeta;

public interface CurriculoRepository extends JpaRepository<WpPostmeta, String>, JpaSpecificationExecutor<WpPostmeta> {

	@Query(value="SELECT distinct meta_key, meta_value, meta_id, post_id FROM wp_postmeta WHERE post_id in (select wp_posts.ID from wp_posts JOIN wp_usermeta on wp_usermeta.user_id = wp_posts.post_author WHERE wp_posts.post_author = :idCandidato and wp_usermeta.meta_value = :cpfCandidato) and meta_key in (:listaMetaKey) ORDER BY meta_key,meta_id", 
			  nativeQuery = true)
	List<WpPostmeta> obterCurriculoCandidato(@Param("idCandidato") String idCandidato, @Param("cpfCandidato") String cpfCandidato, @Param("listaMetaKey") List<String> listaMetaKey);
	
	@Query(value="select * from wp_postmeta where wp_postmeta.meta_key = 'options'and wp_postmeta.post_id = (select ID from wp_posts where wp_posts.post_title = :idFuncao)", 
			  nativeQuery = true)
	String obterPostsCurriculo(@Param("idFuncao") String idFuncao);
	
	/*
	 * Querie para obter as View dos curriculos
	 *  query para view SELECT SUM(count) AS views _views WHERE id IN (2157) AND type = 4
	 */
	
	/*
	 * Query para obter as skill por usuario
	 * SELECT t.*, tt.* FROM wp_terms AS t INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id INNER JOIN wp_term_relationships AS tr ON tr.term_taxonomy_id = tt.term_taxonomy_id WHERE tt.taxonomy IN ('resume_skill') AND tr.object_id IN (2157) ORDER BY t.name ASC
	 */
	
	/*
	 * Query parar obter as habilidades
	 * SELECT t.*, tt.* FROM wp_terms AS t INNER JOIN wp_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tt.taxonomy IN ('habilidade') AND tt.count > 0 ORDER BY tt.count DESC LIMIT 10
	 */
	
}
