package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IdiomaModel {
	
	private String id;
	
	private String idioma;
	
	private String leitura;
	
	private String escrita;
	
	private String conversacao;
	

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof IdiomaModel))
			return false;
		return id != null && id.equals(((IdiomaModel) o).getId());
	}
	
	@Override
	public int hashCode() {
		return 31;
	}

	
}
