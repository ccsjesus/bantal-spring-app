package br.com.se.sis.bantal.modelo;

import lombok.Data;

@Data
public class ExpiracaoPlano {
	
	private String dataPlano;
	
	private String expiracaoPlano;
	
	private long diasExpirar;
	
	private boolean exibirAlertaExpiracao = false;
	
}
