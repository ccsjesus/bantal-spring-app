package br.com.se.sis.bantal.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.UserMeta;
import br.com.se.sis.bantal.modelo.WpUsermeta;

public interface WpUserMetaRepository extends JpaRepository<WpUsermeta, String>, JpaSpecificationExecutor<WpUsermeta> {

//	@Query(value="SELECT user_id, meta_key, meta_value FROM wp_usermeta WHERE user_id = :idUsuario ORDER BY umeta_id ASC", 
//			  nativeQuery = true)
//	List<Object[]> obetrDadosUsuario(@Param("idUsuario") String idUsuario);
	
	@Query(value="select id, meta_key, meta_value from (SELECT wp_users.ID, wp_usermeta.meta_key, wp_usermeta.meta_value FROM `wp_users` \n" + 
			"join wp_usermeta on wp_users.ID = wp_usermeta.user_id where wp_users.ID in :idUsuarios\n" + 
			"union \n" + 
			"select wp_posts.post_author, wp_postmeta.meta_key, wp_postmeta.meta_value from wp_posts\n" + 
			"join wp_postmeta on wp_postmeta.post_id = wp_posts.ID where wp_posts.post_author in :idUsuarios ) resultado order by id asc", 
			  nativeQuery = true)
	List<Object[]> obetrDadosUsuario(@Param("idUsuarios") List<String> idUsuarios);
	
	
	@Modifying
	@Query(value="update wp_usermeta wp set wp.meta_value = :firstName where wp.user_id = :idUsuario and wp.meta_key = 'first_name'",
			nativeQuery = true)
	void atualizarFirstName(@Param("idUsuario") String idUsuario, @Param("firstName") String firstName);
	
	
	@Modifying
	@Query(value="update wp_usermeta wp set wp.meta_value = :valor where wp.user_id = :idUsuario and wp.meta_key = 'last_name'",
			nativeQuery = true)
	void atualizarLastName(@Param("idUsuario") String idUsuario, @Param("valor") String valor);
	
	@Modifying
	@Query(value="update wp_users set display_name =:valor where id =:idUsuario",
			nativeQuery = true)
	void atualizarDisplayName(@Param("idUsuario") String idUsuario, @Param("valor") String valor);
	
	@Modifying
	@Query(value="update wp_users set user_email =:valor where id =:idUsuario",
			nativeQuery = true)
	void atualizarEmail(@Param("idUsuario") String idUsuario, @Param("valor") String valor);
	
	@Query(value="SELECT new br.com.se.sis.bantal.modelo.UserMeta(wp_usermeta.wpUser.id, wp_usermeta.metaKey, wp_usermeta.metaValue) FROM WpUsermeta wp_usermeta WHERE wp_usermeta.wpUser.id =:idUsuario and wp_usermeta.metaKey = 'wp_capabilities' ORDER BY wp_usermeta.umetaId ASC")
	UserMeta obterDadosUsuario(@Param("idUsuario") BigDecimal idUsuario);	
	
	
	
	
}
