package br.com.se.sis.bantal.modelo;

public class CNHModel {

	private String name;
	private String value;
	private boolean selected = false;
	
	public CNHModel() {
	}
	

	public CNHModel(String name, String value, boolean selected) {
		super();
		this.name = name;
		this.value = value;
		this.selected = selected;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
