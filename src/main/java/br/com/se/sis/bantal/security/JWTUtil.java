package br.com.se.sis.bantal.security;

import br.com.se.sis.bantal.controller.requests.ResponseTokenJWT;
import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.exceptions.TokenExpiredException;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class JWTUtil {

	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.expiration}")
	private  Long expiration;

    public String generateToken(String idEmpresa) throws IllegalArgumentException, JWTCreationException {
    	return Jwts.builder()
				.setSubject(idEmpresa)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date((System.currentTimeMillis() + expiration)))
				.signWith(SignatureAlgorithm.HS512, secret.getBytes())
				.compact();
    }

	public String generateTokenWithPayload(String identifier, UserCustomDetails userCustomDetails) throws IllegalArgumentException, JWTCreationException {
		System.out.println(new Date(System.currentTimeMillis()));
		Date expiration = getExpirationTime();
		System.out.println(expiration);


		String authorities = getAuthorities(userCustomDetails);

		ResponseTokenJWT responseTokenJWT = buildClaims(userCustomDetails, authorities);

		return Jwts.builder()
				.setClaims(responseTokenJWT.getAttributes())
				.setSubject(identifier)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(expiration)
				.signWith(SignatureAlgorithm.HS512, secret.getBytes())
				.compact();
	}

	private Date getExpirationTime() {
		Date expiration = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(expiration);
		calendar.add(Calendar.HOUR_OF_DAY, 1);
		return calendar.getTime();
	}

	public String doGenerateRefreshToken(Map<String, Object> claims, String subject) {
		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 30000))
				.signWith(SignatureAlgorithm.HS512, secret.getBytes()).compact();

	}

	private ResponseTokenJWT buildClaims(UserCustomDetails userCustomDetails, String authorities) {
		Map<String, Object> claims = new HashMap<>();
		claims.put("roles", authorities);
		claims.put("name", userCustomDetails.getCustomer().getName());
		claims.put("status", userCustomDetails.getCustomer().getStatus());
		claims.put("cellphone", userCustomDetails.getCustomer().getCellphone());
		claims.put("email", userCustomDetails.getCustomer().getEmail());

		ResponseTokenJWT responseTokenJWT = ResponseTokenJWT.builder()
						.withAttributes(claims).build();
		return responseTokenJWT;
	}

	private String getAuthorities(UserCustomDetails userCustomDetails) {
		String authorities = userCustomDetails.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.joining(","));
		return authorities;
	}

	public boolean isValidToken(String token) {
		Claims claim = getClaims(token);

		if(claim.getSubject() == null || claim.getExpiration() == null || new Date().after(claim.getExpiration())) {
			return false;
		}
		return true;
	}

	public Claims getClaims(String token) {
		try {
			return Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token).getBody();
		} catch (Exception ex ){
			throw new TokenExpiredException(ErrorsMessages.TOKEN_EXPIRED.getDescription(), ErrorsMessages.TOKEN_EXPIRED);

		}
	}

	public Long getExpiration() {
		return expiration;
	}

	public String getSubject(String token) {
		return getClaims(token).getSubject();
	}
}