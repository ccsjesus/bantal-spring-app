package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DetalhesVagasDisponiveisDTO {
	
	private String userId;
	private String nomeEmpresa;
	private String idVaga;
	private String tituloVaga;
	private String dataVaga;
	private String localizacaoVaga;
	private String areaAtuacao;
	private String descricaoVaga;
	private String tipoVaga;
	private String descricaoTipoVaga;
	
}