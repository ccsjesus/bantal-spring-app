package br.com.se.sis.bantal.enums;

import java.util.HashMap;
import java.util.Map;

import br.com.se.sis.bantal.util.FuncaoSistema;

public enum Role {

    ADMIN("ROLE_ADMIN"),
    CANDIDATE("ROLE_CANDIDATE"),
    EMPLOYER("ROLE_EMPLOYER"),
    UNDEFINED("UNDEFINED");

    private static final Map<String, Role> lookup = new HashMap<>();
    private static final Map<String, Role> lookupToRole = new HashMap<>();

    static {
        for (Role d : Role.values()) {
            lookup.put(d.name(), d);
        }
        lookupToRole.put(FuncaoSistema.FUNCAO_EMPRESA.getValor(), Role.EMPLOYER);
        lookupToRole.put(FuncaoSistema.FUNCAO_CANDIDATO.getValor(), Role.CANDIDATE);
        lookupToRole.put(FuncaoSistema.FUNCAO_CUSTOMER.getValor(), Role.CANDIDATE);
        lookupToRole.put(FuncaoSistema.FUNCAO_ADMINISTRATOR.getValor(), Role.ADMIN);
    }

    private String description;

    Role(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static Role get(String abbreviation) {
        return lookup.get(abbreviation);
    }

    public static Role getToRoleFromFunctionSystem(String abbreviation) {
        return lookupToRole.get(abbreviation) != null ? lookupToRole.get(abbreviation) : Role.get(abbreviation);
    }
}
