package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the wp_posts database table.
 * 
 */
public class Term implements Serializable, Comparable<Term> {

	private int termId;

	private String name;

	private String slug;

	private BigInteger termGroup;
	

	public int getTermId() {
		return termId;
	}

	public void setTermId(int termId) {
		this.termId = termId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public BigInteger getTermGroup() {
		return termGroup;
	}

	public void setTermGroup(BigInteger termGroup) {
		this.termGroup = termGroup;
	}
	
	public static List<Term> converterToList(List<WpTerm> lista) {
		List<Term> listaG = new ArrayList<Term>();

		for (WpTerm wp : lista) {
			listaG.add(getInstance(wp));

		}
		return listaG;
	}

	public static Term getInstance(WpTerm wt) {
		Term t = new Term();
		t.setTermId(Integer.parseInt(wt.getTermId()));
		t.setName(wt.getName());
		t.setSlug(wt.getSlug());
		t.setTermGroup(wt.getTermGroup());
		
		return t;
	}
	
	public int compareTo(Term posts) {
		if (this.termId < posts.getTermId()) {
			return 1;
		}
		if (this.termId > posts.getTermId()) {
			return -1;
		}
		return 0;
	}

}