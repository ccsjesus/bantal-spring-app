package br.com.se.sis.bantal.util;

public @interface StatusVaga {	
	
	String APROVADA = "approved";
	
	String NOVA = "new";
	
	String EXPIRADA = "expired";
	
	String EXCLUIDA = "excluded";

}

