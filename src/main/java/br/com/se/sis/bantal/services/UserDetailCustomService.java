package br.com.se.sis.bantal.services;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.exceptions.AuthenticationException;
import br.com.se.sis.bantal.exceptions.RegisterUserRequestException;
import br.com.se.sis.bantal.modelo.WpUserAuthenticationEntity;
import br.com.se.sis.bantal.repository.UserAuthenticationRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UserDetailCustomService implements UserDetailsService {

    @Autowired
    private UserAuthenticationRepository userAuthenticationRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
        WpUserAuthenticationEntity customer = userAuthenticationRepository.findByIdentifier(id).orElseThrow(() -> new AuthenticationException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND));
        return new UserCustomDetails(customer);
    }

    public Optional<WpUserAuthenticationEntity> findByIdentifier(String id) throws UsernameNotFoundException {
        WpUserAuthenticationEntity customer = userAuthenticationRepository.findByIdentifier(id).orElseThrow(() -> new AuthenticationException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND));
        return Optional.of((customer));
    }

    public Optional<UserCustomDetails> findById(String id) throws UsernameNotFoundException {
        WpUserAuthenticationEntity customer = userAuthenticationRepository.findById(Integer.parseInt(id)).orElseThrow(() -> new AuthenticationException(ErrorsMessages.USER_NOT_FOUND.getDescription(), ErrorsMessages.USER_NOT_FOUND));
        return Optional.of(new UserCustomDetails(customer));
    }

    @Transactional
    public WpUserAuthenticationEntity registerUser(WpUserAuthenticationEntity customer) throws RegisterUserRequestException {
        String passwordEncoded = passwordEncoder.encode(customer.getPassword());
        customer.setPassword(passwordEncoded);
        try {
            return userAuthenticationRepository.save(customer);
        } catch (DataAccessException ex ) {
            throw new RegisterUserRequestException("Can't register user: username and email must be unique" );
        }
    }
}
