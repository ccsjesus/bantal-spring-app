package br.com.se.sis.bantal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.ContagemVagasDisponiveisDTO;
import br.com.se.sis.bantal.modelo.DetalhesVagasDisponiveisDTO;
import br.com.se.sis.bantal.modelo.EmpresasDTO;
import br.com.se.sis.bantal.modelo.VagasDisponiveisDTO;
import br.com.se.sis.bantal.modelo.WpVagas;

public interface VagasDisponiveisRepository extends JpaRepository<WpVagas, String>, JpaSpecificationExecutor<WpVagas> {

	@Query(value = " SELECT new br.com.se.sis.bantal.modelo.VagasDisponiveisDTO(v.userId, pe.displayName, v.id, v.titulo, v.dataLimite , v.localizacao, em.sigla) "
			+ " FROM WpVagas v "
			+ " join WpPerfilUsuario pe on pe.userId = v.userId "
			+ " left join WpEmpresas em on em.userId = pe.userId"
			+ " where pe.perfil = 'EMPLOYER' and CURDATE() <= STR_TO_DATE(v.dataLimite,'%d/%m/%Y')")
	List<VagasDisponiveisDTO> recuperarVagasDisponiveis();

	@Query(value = " SELECT new br.com.se.sis.bantal.modelo.VagasDisponiveisDTO(v.userId, pe.displayName, v.id, v.titulo, v.dataLimite , v.localizacao, em.sigla) "
			+ " FROM WpVagas v "
			+ " join WpPerfilUsuario pe on pe.userId = v.userId "
			+ " left join WpEmpresas em on em.userId = pe.userId"
			+ " where pe.perfil = 'EMPLOYER' and v.userId = :idEmpresa and CURDATE() <= STR_TO_DATE(v.dataLimite,'%d/%m/%Y')")
	List<VagasDisponiveisDTO> recuperarVagasDisponiveisPorIdEmpr(@Param("idEmpresa") String idEmpresa);

	
	@Query(value = " SELECT new br.com.se.sis.bantal.modelo.DetalhesVagasDisponiveisDTO(v.userId, pe.displayName, v.id, v.titulo, v.dataLimite , v.localizacao, aa.nome , v.descricao,  tv.id, tv.nome) "
			+ " FROM WpVagas v " + " join WpAreaAtuacaoParametro aa on aa.id = v.wpAreaAtuacaoParametro.id"
			+ " join WpTipoVaga tv on tv.id = v.wpTipoVaga.id"
			+ " join WpPerfilUsuario pe on pe.userId = v.userId where pe.perfil = 'EMPLOYER' and v.id = :idVaga")
	List<DetalhesVagasDisponiveisDTO> recuperarDetalhementoVagaDisponivelPorID(@Param("idVaga") String idVaga);
	
	@Query(value = " SELECT new br.com.se.sis.bantal.modelo.EmpresasDTO(pe.userId, pe.displayName, em.sigla, pe.foto) "
			+ " FROM WpPerfilUsuario pe "
			+ " left join WpEmpresas em on em.userId = pe.userId"
			+ " where pe.perfil = 'EMPLOYER'")
	List<EmpresasDTO> recuperarEmpresas();


	@Query(value = " SELECT new br.com.se.sis.bantal.modelo.ContagemVagasDisponiveisDTO(pe.userId, pe.displayName,  em.objetivo, em.sigla, count(pe.displayName), pe.foto) "
			+ " FROM WpPerfilUsuario pe "
			+ " left join WpEmpresas em on em.userId = pe.userId"
			+ " left join WpVagas v on pe.userId = v.userId where pe.perfil = 'EMPLOYER' and pe.userId = :idEmpresa "
//			+ "and CURDATE() <= STR_TO_DATE(v.dataLimite,'%d/%m/%Y') "
			+ "group by pe.userId, pe.displayName, pe.foto, em.objetivo, em.sigla")
	List<ContagemVagasDisponiveisDTO> recuperarDetalhamentoEmpresaPorId(@Param("idEmpresa") String idEmpresa);
	
	@Query(value = "select count(1) from wp_vagas v where v.user_id = :idEmpresa and curdate()<=STR_TO_DATE(v.data_limite, '%d/%m/%Y')", nativeQuery = true)
	Long contagemVagasPorIdEmpresa(@Param("idEmpresa") String idEmpresa);
	
	@Query(value = " SELECT new br.com.se.sis.bantal.modelo.ContagemVagasDisponiveisDTO(pe.userId, pe.displayName,  em.objetivo, em.sigla, count(pe.displayName), pe.foto) "
			+ " FROM WpPerfilUsuario pe "
			+ " left join WpEmpresas em on em.userId = pe.userId"
			+ " left join WpVagas v on pe.userId = v.userId where pe.perfil = 'EMPLOYER' and em.sigla = :sigla "
//			+ "and CURDATE() <= STR_TO_DATE(v.dataLimite,'%d/%m/%Y') "
			+ "group by pe.userId, pe.displayName, pe.foto, em.objetivo, em.sigla")
	List<ContagemVagasDisponiveisDTO> contagemVagasDisponiveisPorSigla(@Param("sigla") String sigla);
	
}
