package br.com.se.sis.bantal.modelo;

import java.util.ArrayList;
import java.util.List;

public class CandidatoReport {
	
	private List<String> array;

	public CandidatoReport(List<String> array) {
		this.array = array;
	}
	
	public List<String> getArray() {
		return array;
	}

	public void setArray(List<String> array) {
		this.array = array;
	}
	
	
}
