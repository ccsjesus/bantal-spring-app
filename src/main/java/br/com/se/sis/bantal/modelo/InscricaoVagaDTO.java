package br.com.se.sis.bantal.modelo;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class InscricaoVagaDTO {
	
	private String id;
	
	private BigDecimal cdUsuario;
	
	private String nomeCandidato;
	
	private String codigoVaga;
	
	private String comentarioInscricao;


	public String getComentarioInscricao() {
		if(comentarioInscricao == null) {
			comentarioInscricao = "Desejo participar deste processo seletivo.";
		}
		return comentarioInscricao;
	}

}
