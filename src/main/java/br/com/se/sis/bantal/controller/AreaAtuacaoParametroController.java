package br.com.se.sis.bantal.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.transaction.Transactional;

import br.com.se.sis.bantal.annotations.UserEmployerCanOnlyAccessTheirOwnResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.modelo.AreaAtuacaoModel;
import br.com.se.sis.bantal.modelo.WpTokenSistema;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.AreaAtuacaoParametroServices;

@RestController
@RequestMapping("/v1/area-atuacao-parametro")
public class AreaAtuacaoParametroController {


	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private AreaAtuacaoParametroServices areaAtuacaoParametroServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserEmployerCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getAreaAtuacao() throws IllegalArgumentException{
		List<AreaAtuacaoModel> listaAtuacao = areaAtuacaoParametroServices.obterTodasAreasAtuacao();

		
		return ResponseEntity.ok(listaAtuacao);
	}
	
}
