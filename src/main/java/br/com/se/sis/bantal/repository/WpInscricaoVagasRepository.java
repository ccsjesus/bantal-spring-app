package br.com.se.sis.bantal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import br.com.se.sis.bantal.modelo.WpInscricaoVaga;

public interface WpInscricaoVagasRepository extends JpaRepository<WpInscricaoVaga, String>, JpaSpecificationExecutor<WpInscricaoVaga> {

	@Query(value="select * from wp_inscricao i join wp_vagas v on v.id = i.id_vaga where v.user_id = :idLogado", nativeQuery = true)
	public List<WpInscricaoVaga> obterInscricaoVagasPorIdUsuario(String idLogado);
	
	@Query(value="select * from wp_inscricao i join wp_vagas v on v.id = i.id_vaga where i.user_id = :idLogado and v.id = :cdVaga", nativeQuery = true)
	public WpInscricaoVaga obterInscricaoVagaPorCdVagaIdUsuario(String cdVaga, String idLogado);
}
