package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.dto.WpMensagemDTO;
import br.com.se.sis.bantal.dto.WpPerfilDTO;
import br.com.se.sis.bantal.repository.AcessoRepository;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.MensagemService;
import br.com.se.sis.bantal.services.PerfilServices;
import br.com.se.sis.bantal.util.TipoMensagem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/mensagem")
public class MensagemController {

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private MensagemService mensagemService;

	@Autowired
	private AcessoRepository acessoRepository;

	@Autowired
	private PerfilServices perfilServices;


	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/{idVaga}")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> obterMensagensCandidato(Authentication authentication, @PathVariable(value = "idVaga") String idVaga)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(mensagemService.obterMensagensCandidato(idVaga, customDetails.getId()));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/obter-mensagens-empresa/{idVaga}")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> getMensagemEmpresa(Authentication authentication, @PathVariable(value = "idVaga") String idVaga)
			throws IllegalArgumentException, UnsupportedEncodingException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(mensagemService.obterMensagensEmpresa(idVaga));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/obter-mensagens-candidato-empresa")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> getMensagemCandidatoEmpresa(Authentication authentication, @RequestBody WpMensagemDTO mensagem)
			throws IllegalArgumentException, UnsupportedEncodingException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(mensagemService.obterMensagensCandidatoEmpresa(mensagem, customDetails.getId()));
	}
	
	@Transactional
	@RequestMapping(consumes = "application/json", method = RequestMethod.POST, value = "/inserir-mensagem")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> enviarMenssagem(Authentication authentication, @RequestBody WpMensagemDTO mensagem) {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		WpPerfilDTO profileUser = perfilServices.obterPerfilUsuario(customDetails.getId());
		return ResponseEntity.ok(mensagemService.inserirMensagem(mensagem.toWpMensagem(), customDetails.getId(), profileUser.getPerfil()));

	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/obter-mensagens-nao-lidas/{idVaga}")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> getMensagensNaoLidasEmpresa(Authentication authentication, @PathVariable(value = "idVaga") String idVaga)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(mensagemService.obterMsgEmpresa(idVaga, customDetails.getId(), TipoMensagem.MENSAGEM_NAO_LIDA));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/obter-mensagens-lidas/{idVaga}")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> getMensagensLidasEmpresa(Authentication authentication, @PathVariable(value = "idVaga") String idVaga)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(mensagemService.obterMsgEmpresa(idVaga, customDetails.getId(), TipoMensagem.MENSAGEM_LIDA));
	}
	
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/obter-mensagens-codigo-vagas")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> getMensagensLidasEmpresa(Authentication authentication, @RequestBody ArrayList<String> listaCdVagas)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(mensagemService.obterMensagensCandidatoPorCodigo(listaCdVagas, customDetails.getId()));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-situacao-mensagem-lida")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> atualkizarStiacaoMsg(Authentication authentication, @RequestBody List<WpMensagemDTO> listaMsg)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(mensagemService.atualizarStatusMsg(listaMsg, customDetails.getId(), TipoMensagem.MENSAGEM_LIDA));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/obter-mensagem-nao-lida-usuarios")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> obterMsgVariosUsersuario(Authentication authentication, @RequestBody Map<Integer,List<String>> listaMsg)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		return ResponseEntity.ok(mensagemService.obterMsgVariosUsersuario(listaMsg, customDetails.getId()));
	}
	

}
