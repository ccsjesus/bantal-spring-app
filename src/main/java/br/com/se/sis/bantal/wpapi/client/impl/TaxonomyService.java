/**
 * Copyright 2016 Kamran Zafar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package br.com.se.sis.bantal.wpapi.client.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.wpapi.Taxonomy;
import br.com.se.sis.bantal.wpapi.client.BaseService;
import br.com.se.sis.bantal.wpapi.client.WpApiException;

/**
 * Created by kamran on 07/08/16.
 */
@Service
public class TaxonomyService extends BaseService<Taxonomy> {
    @Override
    public Taxonomy[] getAll() {
        return restTemplate.getForObject(buildUri(), Taxonomy[].class);
    }

    @Override
    public Taxonomy[] getAll(Map<String, String> params) {
        return restTemplate.getForObject(buildUri(params), Taxonomy[].class);
    }

    @Override
    public String getUrlPostfix() {
        return wpApiConfig.getApiVersionBase() + "/taxonomies";
    }

    @Override
    public void create(Taxonomy obj) {
        throw new WpApiException("Not supported");
    }

    @Override
    public void update(Taxonomy obj) {
        throw new WpApiException("Not supported");
    }

    @Override
    public void delete(int id) {
        throw new WpApiException("Not supported");
    }

    @Override
    public Class<Taxonomy> getType() {
        return Taxonomy.class;
    }
}
