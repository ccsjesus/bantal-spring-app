package br.com.se.sis.bantal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpTecnologias;

public interface TecnologiasRepository extends JpaRepository<WpTecnologias, String>, JpaSpecificationExecutor<WpTecnologias> {

	@Query(value = "select * from wp_tecnologias f where f.id_curriculo = :idCurriculo",
			nativeQuery = true)
	List<WpTecnologias> obterTecnologiasByIdLogado(@Param("idCurriculo") String idCurriculo);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM wp_tecnologias WHERE wp_tecnologias.id_tecnologia = :id",
			nativeQuery = true)
	void removerTecnologiaById(@Param("id") String id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "INSERT INTO wp_tecnologias (tecnologia, nivel_tecnologia, id_curriculo, desc_atribuicao, experiencia)"
			+		" VALUES"
			+ 		" (:#{#tecnologia.tecnologia}, :#{#tecnologia.nivel}, :#{#tecnologia.wpCurriculosTecnologias.id}, :#{#tecnologia.descAtribuicao}, :#{#tecnologia.experiencia})",
			nativeQuery = true)
	void inserirTecnologias(@Param("tecnologia") WpTecnologias tecnologia);
}
