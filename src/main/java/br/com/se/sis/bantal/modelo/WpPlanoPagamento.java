package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The persistent class for the wp_posts database table.
 * 
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
@Table(name = "wp_plano_pagamento")
public class WpPlanoPagamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private String id;
	
	@Column(name = "cd_usuario")
	private String cdUsuario;

	// bi-directional many-to-one association to WpPost
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cd_plano")
	@JsonBackReference
	private WpPlanos wpPlano;

	@Column(name = "cd_transacao")
	private String cdTransacao;
	
	@Column(name = "cd_solicitacao_transacao")
	private String cdSolicitacaoTransacao;
	
	@Column(name = "cd_referencia_transacao")
	private String cdReferenciaTransacao;
	
	@Column(name="data_solicitacao_pagamento")
	@Basic
	private LocalDateTime dataSolicitacaoPagamento;
	
	@Column(name="data_confrmacao_pagamento", columnDefinition = "TIMESTAMP")
	@Basic
	private LocalDateTime dataConfimacaoPagamento;

	@Column(name="data_cancelamento_plano", columnDefinition = "TIMESTAMP")
	@Basic
	private LocalDateTime dataCancelamentoPlano;
	
	@Column(name="status_pagamento")
	@Basic
	private String statusPagamento;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_perfil")
	@JsonBackReference
	private WpPerfilUsuario wpPerfilUsuario;
	
}