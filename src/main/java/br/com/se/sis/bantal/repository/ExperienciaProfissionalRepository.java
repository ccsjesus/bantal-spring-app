package br.com.se.sis.bantal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpExperienciaProfissional;

public interface ExperienciaProfissionalRepository extends JpaRepository<WpExperienciaProfissional, String>, JpaSpecificationExecutor<WpExperienciaProfissional> {

	@Query(value = "select * from wp_experiencia_profissional f where f.id_curriculo = :idCurriculo",
			nativeQuery = true)
	List<WpExperienciaProfissional> obterExperienciaProfissionalByIdLogado(@Param("idCurriculo") String idCurriculo);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM wp_experiencia_profissional WHERE wp_experiencia_profissional.ID = :id",
			nativeQuery = true)
	void removerXpProfissionalById(@Param("id") String id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "INSERT INTO wp_experiencia_profissional (empregador, cargo_ocupado, dt_inicio, dt_fim, notas, desc_atribuicao, experiencia, id_curriculo, id_profissao)"
			+		" VALUES"
			+ 		" (:#{#experiencia.empregador}, :#{#experiencia.cargoOcupado}, :#{#experiencia.dtInicio}, :#{#experiencia.dtFim}, :#{#experiencia.notas}, :#{#experiencia.descAtribuicao}, :#{#experiencia.experiencia}, :#{#experiencia.wpCurriculosExperiencia.id}, :#{#experiencia.idProfissao})",
			nativeQuery = true)
	void inserirXpProfissional(@Param("experiencia") WpExperienciaProfissional experiencia);
}
