package br.com.se.sis.bantal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.AreaAtuacaoDTO;
import br.com.se.sis.bantal.modelo.WpAreaAtuacaoParametro;

public interface AreaAtuacaoParametroRepository extends JpaRepository<WpAreaAtuacaoParametro, String>, JpaSpecificationExecutor<WpAreaAtuacaoParametro> {
	
	
	@Query(value = "select * from wp_area_atuacao_parametro f where f.id_curriculo = :idCurriculo  order by t.nome asc ", nativeQuery = true)
	List<WpAreaAtuacaoParametro> obterAreaAtuacaoParametroByIdLogado(@Param("idCurriculo") String idCurriculo);
	
	@Query(value = "select * from wp_area_atuacao_parametro f where f.id_area_atuacao = :id", nativeQuery = true)
	WpAreaAtuacaoParametro obterAreaAtuacaoParametroById(@Param("id") String id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM wp_area_atuacao_parametro WHERE wp_area_atuacao.id_area_atuacao = :id",nativeQuery = true)
	void removerAreaAtuacaoParametroById(@Param("id") String id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "INSERT INTO wp_area_atuacao_parametro (nome, valor, id_curriculo)"
			+		" VALUES"
			+ 		" (:#{#atuacao.nome}, :#{#atuacao.valor}, :#{#atuacao.wpCurriculosAreaAtuacao.id})",
			nativeQuery = true)
	void inserirAreaAtuacaoParametro(@Param("atuacao") WpAreaAtuacaoParametro atuacao);
	
	@Query(value = "select * from wp_area_atuacao_parametro t order by t.nome asc ", nativeQuery = true)
	List<WpAreaAtuacaoParametro> obterTodasAreasAtuacao();
	
	@Query(value = "select new br.com.se.sis.bantal.modelo.AreaAtuacaoDTO(watp.id, watp.nome, watf.foto,"
			+ "count(wat.wpCurriculosAreaAtuacao.id)) "
			+ "FROM WpAreaAtuacaoParametro watp "
			+ "left join WpAreaAtuacaoFoto watf on watp.id = watf.idareaatuacao "
			+ "left join WpAreaAtuacao wat on watp.id = wat.wpAreaAtuacaoParametro.id group by watp.id, watp.nome, watf.foto")
	List<AreaAtuacaoDTO> obterTodasAreasAtuacaoAgrupadaPorCurriculo();
	
	@Query(value = "select * from wp_area_atuacao_parametro t where t.habilitada = 1 order by t.nome asc ", nativeQuery = true)
	List<WpAreaAtuacaoParametro> obterTodasAreasAtuacaoHabilitadas();
	
	@Query(value = "select * from wp_area_atuacao_parametro t where t.habilitada = 0 order by t.nome asc ", nativeQuery = true)
	List<WpAreaAtuacaoParametro> obterTodasAreasAtuacaoDesabilitadas();
	
}
