package br.com.se.sis.bantal.security;

import br.com.se.sis.bantal.responses.ErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationEntrypoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        ErrorResponse errorResponse =  new ErrorResponse(HttpStatus.UNAUTHORIZED.value(), "Unauthorized", "000", null);
        response.getOutputStream().print(new ObjectMapper().writerWithDefaultPrettyPrinter()
                .writeValueAsString(errorResponse));
    }
}
