package br.com.se.sis.bantal.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.CurriculoModel;
import br.com.se.sis.bantal.modelo.IdiomaModel;
import br.com.se.sis.bantal.modelo.WpCurriculos;
import br.com.se.sis.bantal.modelo.WpIdioma;
import br.com.se.sis.bantal.repository.IdiomaRepository;


@Service
@Configurable
public class IdiomaServices {
	
	@Autowired
	private IdiomaRepository idiomaRepository;
	
	
	public void atualizarIdioma(CurriculoModel curriculoModel, WpCurriculos wpCurriculos) {
		
		List<WpIdioma> wpFor = idiomaRepository.obterIdiomasByIdLogado(wpCurriculos.getId());
		List<IdiomaModel> listFormUsuarioConsulta = WpIdioma.fromToModel(wpFor);
		List<IdiomaModel> listFormUsuario = curriculoModel.getIdiomas();
		int tam = curriculoModel.getIdiomas().size();


		if(tam == wpFor.size()) {
			//alterar o que ja existe
			for(IdiomaModel arg : curriculoModel.getIdiomas()) {
				WpIdioma wp = idiomaRepository.findById(arg.getId()).get();
				wp.setIdioma(arg.getIdioma());
				wp.setNivelConversacao(arg.getConversacao());
				wp.setNivelEscrita(arg.getEscrita());
				wp.setNivelLeitura(arg.getLeitura());
			}		
		} else if (tam < wpFor.size()) {

			listFormUsuarioConsulta.stream().filter(a -> !listFormUsuario.contains(a)).map(b -> b.getId()).collect(Collectors.toList()).forEach(c -> idiomaRepository.removerIdiomaById(c));
			
		} else if (tam > wpFor.size()) {
			
			listFormUsuario.stream().filter(a -> !listFormUsuarioConsulta.contains(a)).map(wpFormacao -> new WpIdioma(wpFormacao.getIdioma(), wpFormacao.getEscrita(), wpFormacao.getConversacao(), wpFormacao.getLeitura(), wpCurriculos))
			.collect(Collectors.toList()).forEach(c -> idiomaRepository.inserirIdiomas(c));
			
		}
		
	}

}
