package br.com.se.sis.bantal.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImagemModel {

	private Long id;

	private String name;

	private String type;
	
	private byte[] picByte;

}
