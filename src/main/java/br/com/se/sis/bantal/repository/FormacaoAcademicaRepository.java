package br.com.se.sis.bantal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpCurriculos;
import br.com.se.sis.bantal.modelo.WpFormacao;

public interface FormacaoAcademicaRepository extends JpaRepository<WpFormacao, String>, JpaSpecificationExecutor<WpFormacao> {

	@Query(value = "select * from wp_formacao f where f.id_curriculo = :idCurriculo",
			nativeQuery = true)
	List<WpFormacao> obterFormacaoByIdLogado(@Param("idCurriculo") String idCurriculo);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM wp_formacao WHERE wp_formacao.ID = :id",
			nativeQuery = true)
	void removerFormacaoById(@Param("id") String id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "INSERT INTO wp_formacao (dt_inicio, dt_termino, nota, qualificacao, universidade, id_curriculo)"
			+		" VALUES"
			+ 		" (:#{#formacao.dtInicio}, :#{#formacao.dtTermino}, :#{#formacao.nota}, :#{#formacao.qualificacao}, :#{#formacao.universidade}, :#{#formacao.wpCurriculos.id})",
			nativeQuery = true)
	void inserirFormacaoAcademica(@Param("formacao") WpFormacao formacao);
}
