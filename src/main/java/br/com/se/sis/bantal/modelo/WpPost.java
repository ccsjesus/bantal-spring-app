package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the wp_posts database table.
 * 
 */
@Entity
@Table(name="wp_posts")
public class WpPost implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private String id;

	@Column(name="comment_count")
	private BigInteger commentCount;

	@Column(name="comment_status")
	private String commentStatus;

	private String guid;

	@Column(name="menu_order")
	private int menuOrder;

	@Column(name="ping_status")
	private String pingStatus;

	@Lob
	private String pinged;

	@Column(name="post_author")
	private BigDecimal postAuthor;

	@Lob
	@Column(name="post_content")
	private String postContent;

	@Lob
	@Column(name="post_content_filtered")
	private String postContentFiltered;

	@Basic
	@Column(name="post_date")
	private LocalDateTime postDate;

	@Basic
	@Column(name="post_date_gmt")
	private LocalDateTime postDateGmt;

	@Lob
	@Column(name="post_excerpt")
	private String postExcerpt;

	@Column(name="post_mime_type")
	private String postMimeType;

	@Basic
	@Column(name="post_modified")
	private LocalDateTime postModified;

	@Basic
	@Column(name="post_modified_gmt")
	private LocalDateTime postModifiedGmt;

	@Column(name="post_name")
	private String postName;

	@Column(name="post_parent")
	private BigInteger postParent;

	@Column(name="post_password")
	private String postPassword;

	@Column(name="post_status")
	private String postStatus;

	@Lob
	@Column(name="post_title")
	private String postTitle;

	@Column(name="post_type")
	private String postType;

	@Lob
	@Column(name="to_ping")
	private String toPing;

	//bi-directional many-to-one association to WpPostmeta
	@OneToMany(mappedBy="wpPost", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpPostmeta> wpPostmetas;

	public WpPost() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigInteger getCommentCount() {
		return this.commentCount;
	}

	public void setCommentCount(BigInteger commentCount) {
		this.commentCount = commentCount;
	}

	public String getCommentStatus() {
		return this.commentStatus;
	}

	public void setCommentStatus(String commentStatus) {
		this.commentStatus = commentStatus;
	}

	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public int getMenuOrder() {
		return this.menuOrder;
	}

	public void setMenuOrder(int menuOrder) {
		this.menuOrder = menuOrder;
	}

	public String getPingStatus() {
		return this.pingStatus;
	}

	public void setPingStatus(String pingStatus) {
		this.pingStatus = pingStatus;
	}

	public String getPinged() {
		return this.pinged;
	}

	public void setPinged(String pinged) {
		this.pinged = pinged;
	}

	public BigDecimal getPostAuthor() {
		return this.postAuthor;
	}

	public void setPostAuthor(BigDecimal postAuthor) {
		this.postAuthor = postAuthor;
	}

	public String getPostContent() {
		return this.postContent;
	}

	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}

	public String getPostContentFiltered() {
		return this.postContentFiltered;
	}

	public void setPostContentFiltered(String postContentFiltered) {
		this.postContentFiltered = postContentFiltered;
	}

	

	public String getPostExcerpt() {
		return this.postExcerpt;
	}

	public void setPostExcerpt(String postExcerpt) {
		this.postExcerpt = postExcerpt;
	}

	public String getPostMimeType() {
		return this.postMimeType;
	}

	public void setPostMimeType(String postMimeType) {
		this.postMimeType = postMimeType;
	}

	public LocalDateTime getPostModified() {
		return postModified;
	}

	public void setPostModified(LocalDateTime postModified) {
		this.postModified = postModified;
	}

	public LocalDateTime getPostDate() {
		return postDate;
	}

	public void setPostDate(LocalDateTime postDate) {
		this.postDate = postDate;
	}

	public LocalDateTime getPostDateGmt() {
		return postDateGmt;
	}

	public void setPostDateGmt(LocalDateTime postDateGmt) {
		this.postDateGmt = postDateGmt;
	}

	public LocalDateTime getPostModifiedGmt() {
		return postModifiedGmt;
	}

	public void setPostModifiedGmt(LocalDateTime postModifiedGmt) {
		this.postModifiedGmt = postModifiedGmt;
	}

	public String getPostName() {
		return this.postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

	public BigInteger getPostParent() {
		return this.postParent;
	}

	public void setPostParent(BigInteger postParent) {
		this.postParent = postParent;
	}

	public String getPostPassword() {
		return this.postPassword;
	}

	public void setPostPassword(String postPassword) {
		this.postPassword = postPassword;
	}

	public String getPostStatus() {
		return this.postStatus;
	}

	public void setPostStatus(String postStatus) {
		this.postStatus = postStatus;
	}

	public String getPostTitle() {
		return this.postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getPostType() {
		return this.postType;
	}

	public void setPostType(String postType) {
		this.postType = postType;
	}

	public String getToPing() {
		return this.toPing;
	}

	public void setToPing(String toPing) {
		this.toPing = toPing;
	}

	public List<WpPostmeta> getWpPostmetas() {
		return this.wpPostmetas;
	}

	public void setWpPostmetas(List<WpPostmeta> wpPostmetas) {
		this.wpPostmetas = wpPostmetas;
	}

	public WpPostmeta addWpPostmeta(WpPostmeta wpPostmeta) {
		getWpPostmetas().add(wpPostmeta);
		wpPostmeta.setWpPost(this);

		return wpPostmeta;
	}

	public WpPostmeta removeWpPostmeta(WpPostmeta wpPostmeta) {
		getWpPostmetas().remove(wpPostmeta);
		wpPostmeta.setWpPost(null);

		return wpPostmeta;
	}

}