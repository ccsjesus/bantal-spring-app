package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.UserCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.dto.WpMensagemDTO;
import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.repository.AcessoRepository;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.MensagemService;
import br.com.se.sis.bantal.services.PerfilServices;
import br.com.se.sis.bantal.util.TipoMensagem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/candidato/mensagem")
public class MensagemCandidatoController {

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private MensagemService mensagemService;

	@Autowired
	private AcessoRepository acessoRepository;

	@Autowired
	private PerfilServices perfilServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/{idVaga}")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> obterMensagensCandidato(Authentication authentication, @PathVariable(value = "idVaga") String idVaga)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(mensagemService.obterMensagensCandidato(idVaga, customDetails.getId()));

	}

	@Transactional
	@RequestMapping(consumes = "application/json", method = RequestMethod.POST, value = "/inserir-mensagem")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> enviarMenssagem(Authentication authentication, @RequestBody WpMensagemDTO mensagem) {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(mensagemService.inserirMensagem(mensagem.toWpMensagem(), customDetails.getId(), Role.CANDIDATE.name()));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/atualizar-situacao-mensagem-lida")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> atualizarSituacaoMsg(Authentication authentication, @RequestBody List<WpMensagemDTO> listaMsg) throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		return ResponseEntity.ok(mensagemService.atualizarStatusMsg(listaMsg, customDetails.getId(), TipoMensagem.MENSAGEM_LIDA));
	}

}
