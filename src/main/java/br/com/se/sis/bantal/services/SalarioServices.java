package br.com.se.sis.bantal.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.ParametroModel;
import br.com.se.sis.bantal.modelo.WpParametros;
import br.com.se.sis.bantal.repository.ParametrosRepository;
import br.com.se.sis.bantal.util.IParametros;
import br.com.se.sis.bantal.util.Util;


@Service
@Configurable
public class SalarioServices {
	
	@Autowired
	private ParametrosRepository parametrosServices;
	
	
	public List<ParametroModel> obterTodosSalarios() {
		WpParametros wpParametros = parametrosServices.obterValorPorCategoria(IParametros.PRETENSAO_SALARIAL);
		String[] valor = Util.splitEspecial(wpParametros.getValor());
		
		List<ParametroModel> listAreaAtuacao = new ArrayList<>();
		ParametroModel areaAtuacao;
		for(String str : valor) {
			areaAtuacao = new ParametroModel();
			areaAtuacao.setTermId(wpParametros.getCdParametro());
			areaAtuacao.setName(str);
			areaAtuacao.setSlug(str);
			listAreaAtuacao.add(areaAtuacao);
		}
		
		return listAreaAtuacao;
		
	}

}
