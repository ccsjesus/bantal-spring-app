package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_tecnologias")
public class WpTecnologias implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_tecnologia")
	private String id;

	@Column(name="tecnologia")
	private String tecnologia;

	@Column(name="nivel_tecnologia")
	private String nivel;
	
	@Column(name="desc_atribuicao")
	private String descAtribuicao;
	
	@Column(name = "experiencia", nullable = false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean experiencia;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="id_curriculo", insertable = false, updatable = false)
	@JsonBackReference
	private WpCurriculos wpCurriculosTecnologias;

	public WpTecnologias(String id, String tecnologia, String nivel, WpCurriculos wpCurriculos, String descAtribuicao, boolean experiencia) {
		super();
		this.id = id;
		this.tecnologia = tecnologia;
		this.nivel = nivel;
		this.wpCurriculosTecnologias = wpCurriculos;
		this.descAtribuicao = descAtribuicao;
		this.experiencia = experiencia;
	}
	

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WpTecnologias))
			return false;
		return id != null && id.equals(((WpTecnologias) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
	
	public static List<InformaticaModel> fromToModel(List<WpTecnologias> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new InformaticaModel(wpFormacao.getId(), wpFormacao.getNivel(), wpFormacao.getTecnologia(), wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia()))
		        .collect(Collectors.toList());
	}
	
	public static List<WpTecnologias> fromToWpEntity(List<InformaticaModel> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new WpTecnologias(wpFormacao.getId(), wpFormacao.getTecnologia(), wpFormacao.getNivel(), null, wpFormacao.getDescAtribuicao(), wpFormacao.isExperiencia()))
		        .collect(Collectors.toList());
	}
	

}