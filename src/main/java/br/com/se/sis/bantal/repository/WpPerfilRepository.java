package br.com.se.sis.bantal.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpPerfilUsuario;

public interface WpPerfilRepository extends JpaRepository<WpPerfilUsuario, String>, JpaSpecificationExecutor<WpPerfilUsuario> {


	@Query(value=" Select * from wp_perfil_usuario users where users.user_id = :id", nativeQuery = true)
	Optional<WpPerfilUsuario> obterInformacoesUsuarioPorId(@Param("id") String id);
	
	@Query(value=" Select * from wp_perfil_usuario perfil where perfil.perfil = :funcaoSistema", nativeQuery = true)
	List<WpPerfilUsuario> obterPerfisPorFuncaoSistema(@Param("funcaoSistema") String funcaoSistema);

	Optional<WpPerfilUsuario> findByIdentificacao(String identificacao);

	@Modifying
	@Query(value = "update wp_perfil_usuario u set u.user_id = :userId, u.perfil = :profile where u.identificacao = :identifier", nativeQuery = true)
	void updateUserIdAndProfileByIdentifier(@Param(value = "userId") String userId, @Param(value = "profile") String profile, @Param(value = "identifier") String identifier);
}
