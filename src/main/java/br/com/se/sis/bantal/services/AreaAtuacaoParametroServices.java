package br.com.se.sis.bantal.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.AreaAtuacaoModel;
import br.com.se.sis.bantal.modelo.WpAreaAtuacaoParametro;
import br.com.se.sis.bantal.repository.AreaAtuacaoParametroRepository;


@Service
@Configurable
public class AreaAtuacaoParametroServices {
	
	@Autowired
	private AreaAtuacaoParametroRepository areaAtuacaoParametroRepository;
	
	
	public WpAreaAtuacaoParametro obterAreaAtuacaoPorId(String id) {
		return areaAtuacaoParametroRepository.findById(id).get();
	}
	
	public List<AreaAtuacaoModel> obterTodasAreasAtuacao() {
		List<WpAreaAtuacaoParametro> wpParametros = areaAtuacaoParametroRepository.findAll();
		
		List<AreaAtuacaoModel> listAreaAtuacao = new ArrayList<>();
		AreaAtuacaoModel areaAtuacao;
		for(WpAreaAtuacaoParametro str : wpParametros) {
			areaAtuacao = new AreaAtuacaoModel();
			areaAtuacao.setTermId(str.getId());
			areaAtuacao.setName(str.getNome());
			areaAtuacao.setSlug(str.getValor());
			listAreaAtuacao.add(areaAtuacao);
		}
		
		return listAreaAtuacao;
	}
	
	public void atualizarAreaAtuacao(AreaAtuacaoModel areaAtuacaoModel) {
		
		Optional<WpAreaAtuacaoParametro> wpFor = areaAtuacaoParametroRepository.findById(areaAtuacaoModel.getTermId());
		WpAreaAtuacaoParametro optional = wpFor.get();
		
		if(optional != null) {
			optional.setNome(areaAtuacaoModel.getName());
			optional.setValor(areaAtuacaoModel.getSlug());
		} else {
			optional = new WpAreaAtuacaoParametro();
			optional.setNome(areaAtuacaoModel.getName());
			optional.setValor(areaAtuacaoModel.getSlug());
			areaAtuacaoParametroRepository.save(optional);
		}
		
	}
	
}
