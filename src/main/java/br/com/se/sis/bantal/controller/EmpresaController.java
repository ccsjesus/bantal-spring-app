package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.UserEmployerAndCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.services.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@RestController()
public class EmpresaController {

   @Autowired
    private EmpresaService empresaService;

    @Transactional
    @RequestMapping(method = RequestMethod.GET, value = "/dados-empresa/{idEmpresa}")
    @UserEmployerAndCandidateCanOnlyAccessTheirOwnResource
    public ResponseEntity<Object> obterInformacoesEmpresaPorId(@PathVariable Long idEmpresa) throws IllegalArgumentException{
        return ResponseEntity.ok(empresaService.obterInformacoesEmpresaPorId(idEmpresa));
    }

}
