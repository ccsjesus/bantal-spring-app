package br.com.se.sis.bantal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.WpPlanos;
import br.com.se.sis.bantal.repository.PlanosRepository;
import br.com.se.sis.bantal.util.IPerfilSistema;

@Service
@Configurable
public class PlanoService {
	
	@Autowired
	private PlanosRepository planosRepository;
	
	
	public List<WpPlanos> obterPlanoEmpresa() {
		return planosRepository.obterPlanoPorPerfil(IPerfilSistema.FUNCAO_EMPRESA);
	}
	
	public List<WpPlanos> obterPlanoCandidato() {
		return planosRepository.obterPlanoPorPerfil(IPerfilSistema.FUNCAO_CANDIDATO);
	}
	

}
