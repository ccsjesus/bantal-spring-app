package br.com.se.sis.bantal.controller;

import javax.transaction.Transactional;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.annotations.UserEmployerAndAdminCanOnlyAccessResources;
import br.com.se.sis.bantal.security.UserCustomDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.modelo.AreaAtuacaoModel;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.AreaAtuacaoServices;

@RestController
@RequestMapping("/v1/area-atuacao")
public class AreaAtuacaoController {


	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private AreaAtuacaoServices areaAtuacaoServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> obterListaAreaAtuacao() throws IllegalArgumentException {

		return ResponseEntity.ok(areaAtuacaoServices.obterTodasAreasAtuacao());
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/agrupadas")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> obterTodasAreasAtuacaoAgrupadaPorCurriculo() throws IllegalArgumentException {

		return ResponseEntity.ok(areaAtuacaoServices.obterTodasAreasAtuacaoAgrupadaPorCurriculo());
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/habilitadas/")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> getTipoVaga() throws IllegalArgumentException {

		return ResponseEntity.ok(areaAtuacaoServices.obterTodosTipoVagaHabilitadas());
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/desabilitadas/")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> getTipoVagaHabilitada() throws IllegalArgumentException {

		return ResponseEntity.ok(areaAtuacaoServices.obterTodosTipoVagaDesabilitadas());
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/incluir")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> incluirTipoVaga(Authentication authentication, @RequestBody AreaAtuacaoModel tipo)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		areaAtuacaoServices.incluirNovoTipoVaga(customDetails.getId(), tipo);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);

	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/desabilitar/{id}")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> excluirVaga(Authentication authentication, @PathVariable("id") String idTipoVaga)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		areaAtuacaoServices.excluirNovoTipoVaga(customDetails.getId(), idTipoVaga);
		return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/habilitar/{id}")
	@UserEmployerAndAdminCanOnlyAccessResources
	public ResponseEntity<Object> habilitarTipoVaga(Authentication authentication, @PathVariable("id") String idTipoVaga)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

			areaAtuacaoServices.habilitarNovoTipoVaga(customDetails.getId(), idTipoVaga);
			return ResponseEntity.status(HttpStatus.CREATED).body(null);
	}
	
}
