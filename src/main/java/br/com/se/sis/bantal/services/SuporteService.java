package br.com.se.sis.bantal.services;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.se.sis.pagseguro.domain.vo.SuporteModel;

@Service
@Configurable
public class SuporteService {

	private String protocolo = "smtp";

	@Value(value = "${bantal.mail.smtp.host}")
	private String servidor;

	@Value(value = "${bantal.mail.username}")
	private String username;

	@Value(value = "${bantal.mail.password}")
	private String senha;

	private String porta = "587";

	public boolean sendEmail(SuporteModel suporteModel) {
		{
			Properties props = new Properties();
			props.put("mail.transport.protocol", protocolo);
			props.put("mail.smtp.host", servidor);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", porta);

			Session session = Session.getDefaultInstance(props, null);
			session.setDebug(false);

			try {
				String destinatario = suporteModel.getDestinatario().split(" ")[0];
				InternetAddress iaFrom = new InternetAddress(username, suporteModel.getNome());
				InternetAddress[] iaTo = new InternetAddress[1];
				InternetAddress[] iaReplyTo = new InternetAddress[1];

				//iaReplyTo[0] = new InternetAddress(suporteModel.getDestinatario(), suporteModel.getDestinatario());
				iaTo[0] = new InternetAddress(username, username);

				MimeMessage msg = new MimeMessage(session);

				if (iaReplyTo != null)
					//msg.setReplyTo(iaReplyTo);
				if (iaFrom != null)
					msg.setFrom(iaFrom);
				if (iaTo.length > 0)
					msg.setRecipients(Message.RecipientType.TO, iaTo);
				msg.setSubject(suporteModel.getAssunto() + " - <" + destinatario + ">");
				msg.setSentDate(new Date());

				msg.setContent( suporteModel.getMensagem(), "text/html");

				Transport tr = session.getTransport(protocolo);
				tr.connect(servidor, username, senha);

				msg.saveChanges();

				tr.sendMessage(msg, msg.getAllRecipients());
				tr.close();
				return true;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return false;
			} catch (MessagingException e) {
				e.printStackTrace();
				return false;
			}
		}
	}
	
}