package br.com.se.sis.bantal.services;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.modelo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.dto.WpMensagemDTO;
import br.com.se.sis.bantal.repository.MensagemRepository;
import br.com.se.sis.bantal.repository.WpVagasRepository;
import br.com.se.sis.bantal.util.Constantes;
import br.com.se.sis.bantal.util.TipoMensagem;
import br.com.se.sis.pagseguro.exception.TransacaoAbortadaException;

@Service
@Configurable
public class MensagemService {
	

	@Autowired
	private MensagemRepository mensagemRepository;
	
	@Autowired
	private CandidatoService candidatoService;
	
	@Autowired
	private PlanoPagamentoService planosService;
	
	@Autowired
	private WpVagasRepository vagaRepository;
	
	
	public List<WpMensagemDTO> obterMensagensCandidato(String idVaga, String idUsuarioLogado) {
		List<WpMensagem> listaMinhasMsg = mensagemRepository.obterMensagensPorCodigoVagaCandidato(idVaga, idUsuarioLogado);
		List<WpMensagemDTO> listaMinhasMensagemDTO = WpMensagem.converterListMensagem(listaMinhasMsg);

		listaMinhasMensagemDTO.forEach( c2 -> {
			WpPerfilUsuario c = candidatoService.obterDadosUsuario(c2.getCodigoAutorMensagem());
			c2.setNomeAutorMensagem(c.getDisplayName());
		});
		
		
		List<WpMensagem> listaMsgDestino = mensagemRepository.obterMensagensPorCodigoVagaDestino(idVaga, idUsuarioLogado);
		List<WpMensagemDTO> listaMsgDestinoDTO = WpMensagem.converterListMensagem(listaMsgDestino);

		listaMsgDestinoDTO.forEach( c2 -> {
			WpPerfilUsuario c = candidatoService.obterDadosUsuario(c2.getCodigoAutorMensagem());
			c2.setNomeAutorMensagem(c.getDisplayName());
		});
		
		List<WpMensagemDTO> union = Stream.concat(listaMinhasMensagemDTO.stream(), listaMsgDestinoDTO.stream())
			    .distinct()
			    .collect(Collectors.toList());
        Collections.sort(union);

		
		return union;
	}
	
	public Map<Integer,List<WpMensagemDTO>> obterMensagensCandidatoPorCodigo(List<String> listaCdVaga, String idUsuarioLogado) {
		
		Map<Integer,List<WpMensagemDTO>> listUnion = new HashMap<Integer,List<WpMensagemDTO>>();
		
		for(String str : listaCdVaga) {
			List<WpMensagem> listaMinhasMsg = mensagemRepository.obterMsgsPorIdTipoMensagem(str, idUsuarioLogado, TipoMensagem.MENSAGEM_NAO_LIDA);
			List<WpMensagemDTO> listaMinhasMensagemDTO = WpMensagem.converterListMensagem(listaMinhasMsg);
			
			listaMinhasMensagemDTO.forEach( c2 -> {
				WpPerfilUsuario c = candidatoService.obterDadosUsuario(c2.getCodigoAutorMensagem());
				c2.setNomeAutorMensagem(c.getDisplayName());
			});
			
			listUnion.put(Integer.valueOf(str), listaMinhasMensagemDTO);
			
		};
		
		return listUnion;
	}
	
	public List<WpMensagemDTO> obterMensagensEmpresa(String idVaga) {
		List<WpMensagem> listaMsgVaga = mensagemRepository.obterMensagensPorCodigoVaga(idVaga);
		List<WpMensagemDTO> listaMsgVagaDTO = WpMensagem.converterListMensagem(listaMsgVaga);

		listaMsgVagaDTO.forEach( c2 -> {
			WpPerfilUsuario c = candidatoService.obterDadosUsuario(c2.getCodigoAutorMensagem());
			c2.setNomeAutorMensagem(c.getDisplayName());
		});
		
        Collections.sort(listaMsgVagaDTO);

		
		return listaMsgVagaDTO;
	}
	
	public List<WpMensagemDTO> obterMensagensCandidatoEmpresa(WpMensagemDTO mensagem, String idUsuarioLogado) {
		List<WpMensagem> listaMinhasMsg = mensagemRepository.obterMsgCandidatoEmpresa(mensagem.getIdVaga(), idUsuarioLogado, mensagem.getCodigoAutorMensagem()).orElseGet(Collections::emptyList);
		List<WpMensagemDTO> listMyMessagesDTO = Collections.emptyList();
		List<WpMensagemDTO> listMessagesEmployerDTO = Collections.emptyList();

		if(!listaMinhasMsg.isEmpty()){
			listMyMessagesDTO = WpMensagem.converterListMensagem(listaMinhasMsg);
			Collections.sort(listMyMessagesDTO);

			listMyMessagesDTO.forEach( c2 -> {
				WpPerfilUsuario c = candidatoService.obterDadosUsuario(c2.getCodigoAutorMensagem());
				c2.setNomeAutorMensagem(c.getDisplayName());
			});
		}

        List<WpMensagem> listaMsgDestino = mensagemRepository.getSendsMessagesToCandidateByEmployer(mensagem.getIdVaga(), idUsuarioLogado, mensagem.getCodigoAutorMensagem()).orElseGet(Collections::emptyList);
		if(!listaMsgDestino.isEmpty()){
			listMessagesEmployerDTO = WpMensagem.converterListMensagem(listaMsgDestino);
			listMessagesEmployerDTO.forEach( c2 -> {
				WpPerfilUsuario c = candidatoService.obterDadosUsuario(c2.getCodigoAutorMensagem());
				c2.setNomeAutorMensagem(c.getDisplayName());
			});
		}

		List<WpMensagemDTO> union = Stream.concat(listMyMessagesDTO.stream(), listMessagesEmployerDTO.stream())
			    .distinct()
			    .collect(Collectors.toList());
        Collections.sort(union);
		
		return union;
	}
	
	public List<WpMensagemDTO> obterMsgEmpresa(String idVaga, String idUsuarioLogado, int tipoMsg) {
		
		List<WpMensagemDTO> listaMsgVagaDTO = null;
				
		if(tipoMsg != TipoMensagem.MENSAGEM_TODAS) {
			listaMsgVagaDTO = WpMensagem.converterListMensagem(mensagemRepository.obterMsgsPorIdTipoMensagem(idVaga, idUsuarioLogado, tipoMsg));
		} else {
			listaMsgVagaDTO = WpMensagem.converterListMensagem(mensagemRepository.obterMsgsPorIdTipoMensagem(idVaga, idUsuarioLogado, tipoMsg));

		}

		
        Collections.sort(listaMsgVagaDTO);

		
		return listaMsgVagaDTO;
	}
	
	
	
	public boolean inserirMensagem(WpMensagem mensagem, String idUsuarioLogado, String profileUser) throws TransacaoAbortadaException {
		WpVagas vaga = vagaRepository.obterVagaPorId(mensagem.getIdVaga());
		
		Role role = Role.get(profileUser);
		
		 switch(role) {
			case CANDIDATE:
			return inserirMensagemGenerics(mensagem, idUsuarioLogado, vaga.getUserId());
			
			case EMPLOYER:
			case ADMIN:
			return inserirMensagemGenerics(mensagem, idUsuarioLogado, null);
		default:
			return false;
		}
		
	}
	
	public boolean inserirMensagemGenerics(WpMensagem mensagem, String idUsuarioLogado, String cdReferencia) {
		mensagem.setCodigoAutorMensagem(idUsuarioLogado);
		if(cdReferencia != null)
			mensagem.setCodigoReferenciaMensagem(cdReferencia);
		mensagem.setLida(Constantes.MSG_NAO_LIDA.getDescricao());
	    mensagemRepository.save(mensagem);
		return true;
		
	}
	
	public boolean atualizarStatusMsg(List<WpMensagemDTO> listaMensagem,  String idUsuarioLogado, int tipoMsg) {
		
		listaMensagem.forEach(str -> {
			WpMensagem msg = mensagemRepository.obterMensagensPorId(str.getID());
			msg.setLida(tipoMsg + "");
			mensagemRepository.save(msg);
		});

		return true;
	}
	
	public Map<Integer,List<WpMensagemDTO>> obterMsgVariosUsersuario(Map<Integer, List<String>> listaCdVaga, String idUser) {
		
		Map<Integer,List<WpMensagemDTO>> listUnion = new HashMap<Integer,List<WpMensagemDTO>>();
		
		listaCdVaga.forEach((k, v) -> {
			v.forEach(str -> {
				if(str != null) {
					List<WpMensagem> listaMinhasMsg = mensagemRepository.obterMsgNaoLidaUsuarios(k.toString(), new BigInteger(idUser), new BigInteger(str));
					List<WpMensagemDTO> listaMinhasMensagemDTO = WpMensagem.converterListMensagem(listaMinhasMsg);
					Collections.sort(listaMinhasMensagemDTO);
					
					listUnion.put(Integer.parseInt(str), listaMinhasMensagemDTO);
					
				}
			});
			
		});
		
		return listUnion;
	}
	
	

}
