package br.com.se.sis.bantal.security;

import br.com.se.sis.bantal.clients.ClientUsuarioOauthWp;
import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.repository.AcessoRepository;
import br.com.se.sis.bantal.repository.UserAuthenticationRepository;
import br.com.se.sis.bantal.repository.WpPerfilRepository;
import br.com.se.sis.bantal.services.RefreshTokenService;
import br.com.se.sis.bantal.services.UserDetailCustomService;
import br.com.se.sis.bantal.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true )
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private CustomAuthenticationEntrypoint customAuthenticationEntrypoint;

    @Autowired
    private UserAuthenticationRepository userAuthenticationRepository;

    @Autowired
    private UserDetailCustomService userCustomDetails;

    @Autowired
    private ClientUsuarioOauthWp clientUserOauthWp;

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private RefreshTokenService refreshTokenService;

    @Autowired
    private WpPerfilRepository profileRepository;

    @Autowired
    private UserDetailCustomService userDetailCustomService;

    @Autowired
    private UsuarioService userService;


    private String[] PUBLIC_MATCHERS = {
            "/**"
    };

    private String[]  PUBLIC_POST_MATCHERS = {
            "/login",
            "/auth/register",
            "/auth/login",
            "/auth/refreshtoken"
    };

    private String[]  ADMIN_MATCHERS = {
            "/admin/**"
    };

    private String[]  BOTH_MATCHERS = {
            "/dados/**",
            "/v1/area-atuacao/**",
            "/v1/tipo-vaga/**",
            "/v1/salario/**",
            "/vagas/**",
            "/v1/planos/**",
            "/candidato/mensagem/**",
            "/mensagem/**",
            "/v1/vagas-admin/**",
            "/vagas-candidato/**",
            "/v1/tipo-vaga/**",
            "/vagas-empresa/**",
            "/curriculo/**",
            "/cnh/**",
            "/funcao-cargo/**",
            "/nivel-cargo/**",
            "/habilidade/**",
            "/suporte/**",
            "/v1/tecnologia/**",
            "/v1/experiencia-profissional/**",
            "/imagem/**",
            "/pagamento/**",
            "/v2/img-upload/**"
    };


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable();
        http.authorizeRequests()
                .antMatchers(PUBLIC_MATCHERS).permitAll()
                .antMatchers(HttpMethod.POST, PUBLIC_POST_MATCHERS).permitAll()
                .antMatchers(ADMIN_MATCHERS).hasAuthority(Role.ADMIN.getDescription())
                .antMatchers(BOTH_MATCHERS).hasAnyAuthority(Role.CANDIDATE.getDescription(), Role.EMPLOYER.getDescription(), Role.ADMIN.getDescription())
                .anyRequest().authenticated();
        http.addFilter(new AuthenticationFilter(authenticationManager(), userAuthenticationRepository, jwtUtil, refreshTokenService, profileRepository, userDetailCustomService, clientUserOauthWp, userService));
        http.addFilter(new AuthorizationFilter(authenticationManager(), userCustomDetails, jwtUtil));
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling().authenticationEntryPoint(customAuthenticationEntrypoint);
        http.headers().frameOptions().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userCustomDetails).passwordEncoder(bCryptPasswordEncoder());
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(false);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    @Bean
    public HttpFirewall defaultHttpFirewall() {
        return new DefaultHttpFirewall();
    }

}
