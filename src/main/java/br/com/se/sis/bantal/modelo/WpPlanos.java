package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

/**
 * The persistent class for the wp_posts database table.
 * 
 */
@Data
@Entity
@Table(name = "wp_planos")
public class WpPlanos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_plano")
	private String cdPlano;

	@Column(name = "nome")
	private String nome;

	@Column(name = "slug")
	private String slug;

	@Column(name = "desc_tempo")
	private String descTempo;
	
	@Column(name = "valor")
	private String valor;
	
	@Column(name = "parcelas")
	private String parcelas;
	
	@Column(name = "perfil")
	private String perfil;
	
	@Column(name = "tipo")
	private String tipo;
	
	@Column(name = "duracao")
	private int duracao;
	
	@Column(name = "tempo_alerta")
	private int tempoAlerta;

	// bi-directional many-to-one association to wpPlanoPagamento
	@OneToMany(mappedBy = "wpPlano", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpPlanoPagamento> wpPlanoPagamento;

	public WpPlanos() {
	}

	public WpPlanoPagamento addWpPlanoPagamento(WpPlanoPagamento wpPlanoPagamento) {
		getWpPlanoPagamento().add(wpPlanoPagamento);
		wpPlanoPagamento.setWpPlano(this);

		return wpPlanoPagamento;
	}

	public WpPlanoPagamento removeWpPlanoPagamento(WpPlanoPagamento wpPlanoPagamento) {
		getWpPlanoPagamento().remove(wpPlanoPagamento);
		wpPlanoPagamento.setWpPlano(null);

		return wpPlanoPagamento;
	}

}