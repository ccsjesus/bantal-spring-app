package br.com.se.sis.bantal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpTermTaxonomy;

public interface SalarioRepository extends JpaRepository<WpTermTaxonomy, String>, JpaSpecificationExecutor<WpTermTaxonomy> {
	
	@Query(value="select wp_posts.ID from wp_posts WHERE  wp_posts.post_title = 'job_salary' and wp_posts.post_status = 'publish'", 
			  nativeQuery = true)
	String obterIDSalario();
	
	@Query(value="select * from wp_postmeta where wp_postmeta.meta_key = 'options'and wp_postmeta.post_id = (select wp_posts.ID from wp_posts WHERE  wp_posts.post_title = 'job_salary' and wp_posts.post_status = 'publish')", 
			  nativeQuery = true)
	String obterTodosSalarios();
	
	@Query(value="SELECT meta_key, meta_value, meta_id, post_id FROM wp_postmeta WHERE post_id = (select wp_posts.ID from wp_posts JOIN wp_usermeta on wp_usermeta.user_id = wp_posts.post_author WHERE wp_posts.post_author = :idCandidato and wp_usermeta.meta_value = :cpfCandidato and wp_posts.post_status = 'publish' and wp_posts.post_type = 'resume') and wp_postmeta.meta_key = '_candidate_salario' ORDER BY meta_key,meta_id", 
			  nativeQuery = true)
	String obterSalarioCandidato(@Param("idCandidato") String idCandidato, @Param("cpfCandidato") String cpfCandidato);
	
}
