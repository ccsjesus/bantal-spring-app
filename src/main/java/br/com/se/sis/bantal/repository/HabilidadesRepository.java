package br.com.se.sis.bantal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import br.com.se.sis.bantal.modelo.WpTermTaxonomy;

public interface HabilidadesRepository extends JpaRepository<WpTermTaxonomy, String>, JpaSpecificationExecutor<WpTermTaxonomy> {
	
	
}
