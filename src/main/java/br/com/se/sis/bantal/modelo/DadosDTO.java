package br.com.se.sis.bantal.modelo;

import lombok.Data;

@Data
public class DadosDTO {

    private String perfil;

    public DadosDTO(String perfil) {

        this.perfil = perfil;
    }

}