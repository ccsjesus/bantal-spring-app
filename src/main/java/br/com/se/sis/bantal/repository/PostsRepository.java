package br.com.se.sis.bantal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpPost;

public interface PostsRepository extends JpaRepository<WpPost, String>, JpaSpecificationExecutor<WpPost> {
	
	@Query(value="SELECT * from wp_posts WHERE wp_posts.post_type = 'job_listing' and wp_posts.post_status = 'publish'", 
			  nativeQuery = true)
	List<WpPost> obterVagasDisponiveis();
	
//	@Query(value = "SELECT * FROM wp_posts WHERE 1=1 AND wp_posts.post_type = 'job_listing' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'hidden' OR wp_posts.post_status = 'expired' OR wp_posts.post_status = 'new' OR wp_posts.post_status = 'interviewed' OR wp_posts.post_status = 'offer' OR wp_posts.post_status = 'hired' OR wp_posts.post_status = 'rejected' OR wp_posts.post_status = 'archived' OR wp_posts.post_status = 'future' OR wp_posts.post_status = 'draft' OR wp_posts.post_status = 'expired' OR wp_posts.post_status = 'private') ORDER BY wp_posts.post_date DESC",
//			nativeQuery = true)
	@Query(value = "SELECT * FROM wp_posts WHERE 1=1 AND wp_posts.post_type = 'job_listing' AND (wp_posts.post_status = 'publish') ORDER BY wp_posts.post_date DESC",
			nativeQuery = true)
	List<WpPost> obterTodosIdsVagasCadastradas();
	
	@Query(value = "select * from wp_posts where wp_posts.post_parent in (:lista)  and post_status <> 'trash' and post_type = 'job_application' ", nativeQuery = true)
	List<WpPost> obterVagasPorListaIdCadastradas(@Param("lista") List<String> lista);
	
	@Query(value = "select * from wp_posts where wp_posts.id in (:lista)  and post_status <> 'trash' and post_type = 'job_application' ", nativeQuery = true)
	List<WpPost> obterVagasPorId(@Param("lista") List<String> lista);
	
	@Query(value="select * from wp_posts where wp_posts.ID = :idPost", 
			  nativeQuery = true)
	WpPost obterCodAutorPorCodPost(String idPost);
	
}
