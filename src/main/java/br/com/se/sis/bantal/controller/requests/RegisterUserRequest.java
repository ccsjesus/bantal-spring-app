package br.com.se.sis.bantal.controller.requests;

import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.modelo.WpPerfilUsuario;
import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class RegisterUserRequest {

    @NotEmpty(message = "Nome é obrigatório")
    private String name;

    @NotEmpty(message = "Email é obrigatório")
    private String email;

    @NotEmpty(message = "Identificação é obrigatório")
    private String identifier;

    @NotEmpty(message = "Senha é obrigatório")
    private String password;

    @NotEmpty(message = "Telefone é obrigatório")
    private String cellphone;

    @NotEmpty(message = "Perfil do Sistema é obrigatório")
    @JsonAlias(value = "profile")
    private String profileSystem;


    public static RegisterUserRequest toRequest(WpPerfilUsuario profileUser){
        return RegisterUserRequest.builder()
                .withName(profileUser.getNome())
                .withEmail(profileUser.getEmail())
                .withIdentifier(profileUser.getIdentificacao())
                .withProfileSystem(Role.get(profileUser.getPerfil()).name())
                .build();
    }
}
