package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.AllUsersCanOnlyAccessResources;
import br.com.se.sis.bantal.modelo.Usuario;
import br.com.se.sis.bantal.repository.CandidatosRepository;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.util.MetaKeys;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.*;

@RestController
@RequestMapping("/relatorio")
public class RelatorioController {

	@Autowired
	private CandidatosRepository candidatosRepository;
	
	@Autowired
	private TokenRepository tokenRepository;
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/candidato")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> imprimir(Authentication authentication, @RequestBody List<String> array, HttpServletResponse response)
			throws JRException, SQLException, IOException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		// Pega o arquivo .jasper localizado em resources
		InputStream jasperStream = this.getClass().getResourceAsStream("/relatorios/livros.jasper");

		// Cria o objeto JaperReport com o Stream do arquivo jasper
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
		// Passa para o JasperPrint o relatório, os parâmetros e a fonte dos dados, no
		// caso uma conexão ao banco de dados
		
		
		List<Object[]> candidatos = candidatosRepository.consultarCandidatosId(array, MetaKeys.listaMetaKeys);
		List<Usuario> user = null; //usuarioService.retornarWpResultadoFromObject(candidatos);
		
		JRDataSource jrDataSource = new JRBeanCollectionDataSource(user);
		JRBeanCollectionDataSource reports = new JRBeanCollectionDataSource(user, false);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ItemDataSource",reports);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrDataSource);

		// Configura a respota para o tipo PDF
		response.setContentType("application/pdf");
		// Define que o arquivo pode ser visualizado no navegador e também nome final do
		// arquivo
		// para fazer download do relatório troque 'inline' por 'attachment'
		response.setHeader("Content-Disposition", "inline; filename=bantal_relatorio.pdf");

		// Faz a exportação do relatório para o HttpServletResponse
		final OutputStream outStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		return ResponseEntity.ok("Impressão realizada com suceso!");
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/candidato/{idUsuario}")
	@AllUsersCanOnlyAccessResources
	public ResponseEntity<Object> imprimirRelatorio(Authentication authentication, @PathVariable(value = "idUsuario") String idUsuario, HttpServletResponse response)
			throws JRException, SQLException, IOException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		// Pega o arquivo .jasper localizado em resources
		InputStream jasperStream = this.getClass().getResourceAsStream("/relatorios/livros.jasper");

		// Cria o objeto JaperReport com o Stream do arquivo jasper
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
		// Passa para o JasperPrint o relatório, os parâmetros e a fonte dos dados, no
		// caso uma conexão ao banco de dados
		
		
		List<Object[]> candidatos = candidatosRepository.consultarCandidatosId(Arrays.asList(idUsuario),MetaKeys.listaMetaKeys);
		List<Usuario> user = null; //usuarioService.retornarWpResultadoFromObject(candidatos);
		
		JRDataSource jrDataSource = new JRBeanCollectionDataSource(user);
		JRBeanCollectionDataSource reports = new JRBeanCollectionDataSource(user, false);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ItemDataSource",reports);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrDataSource);

		// Configura a respota para o tipo PDF
		response.setContentType("application/pdf");
		// Define que o arquivo pode ser visualizado no navegador e também nome final do
		// arquivo
		// para fazer download do relatório troque 'inline' por 'attachment'
		response.setHeader("Content-Disposition", "inline; filename=bantal_relatorio.pdf");

		// Faz a exportação do relatório para o HttpServletResponse
		final OutputStream outStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		return ResponseEntity.ok("Impressão realizada com suceso!");
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/candidato")
	public ResponseEntity<Object>  imprimirGET(Authentication authentication, HttpServletResponse response, String cdU)
			throws JRException, SQLException, IOException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		// Pega o arquivo .jasper localizado em resources
		InputStream jasperStream = this.getClass().getResourceAsStream("/relatorios/livros.jasper");

		// Cria o objeto JaperReport com o Stream do arquivo jasper
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
		// Passa para o JasperPrint o relatório, os parâmetros e a fonte dos dados, no
		// caso uma conexão ao banco de dados
		
		List<String> array = new ArrayList<String>();
		array.add("15");
		array.add("41");
		List<Object[]> candidatos = candidatosRepository.consultarCandidatosId(array, MetaKeys.listaMetaKeys);
		List<Usuario> user = null;//usuarioService.retornarWpResultadoFromObject(candidatos);
		
		JRDataSource jrDataSource = new JRBeanCollectionDataSource(user);
		JRBeanCollectionDataSource reports = new JRBeanCollectionDataSource(user, false);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ItemDataSource",reports);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrDataSource);

		// Configura a respota para o tipo PDF
		response.setContentType("application/pdf");
		// Define que o arquivo pode ser visualizado no navegador e também nome final do
		// arquivo
		// para fazer download do relatório troque 'inline' por 'attachment'
		response.setHeader("Content-Disposition", "inline; filename=livros.pdf");

		// Faz a exportação do relatório para o HttpServletResponse
		final OutputStream outStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		return null;
	}

}
