package br.com.se.sis.bantal.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.transaction.Transactional;

import br.com.se.sis.bantal.annotations.UserCandidateCanOnlyAccessTheirOwnResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.modelo.HabilidadeModel;
import br.com.se.sis.bantal.modelo.ParametroModel;
import br.com.se.sis.bantal.modelo.WpTokenSistema;
import br.com.se.sis.bantal.repository.HabilidadesRepository;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.services.HabilidadesServices;

@RestController
@RequestMapping("/v1/habilidade")
public class HabilidadesController {


	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private HabilidadesServices habilidadesServices;
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getAreaAtuacao() throws IllegalArgumentException{

		return ResponseEntity.ok(habilidadesServices.obterTodasHabilidade());
	}
	
}
