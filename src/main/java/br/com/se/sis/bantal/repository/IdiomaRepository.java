package br.com.se.sis.bantal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpIdioma;

public interface IdiomaRepository extends JpaRepository<WpIdioma, String>, JpaSpecificationExecutor<WpIdioma> {

	@Query(value = "select * from wp_idiomas f where f.id_curriculo = :idCurriculo",
			nativeQuery = true)
	List<WpIdioma> obterIdiomasByIdLogado(@Param("idCurriculo") String idCurriculo);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM wp_idiomas WHERE wp_idiomas.id_idioma = :id",
			nativeQuery = true)
	void removerIdiomaById(@Param("id") String id);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "INSERT INTO wp_idiomas (idioma, nivel_leitura, nivel_conversacao, nivel_escrita, id_curriculo)"
			+		" VALUES"
			+ 		" (:#{#idioma.idioma}, :#{#idioma.nivelLeitura}, :#{#idioma.nivelConversacao}, :#{#idioma.nivelEscrita}, :#{#idioma.wpCurriculosIdiomas.id})",
			nativeQuery = true)
	void inserirIdiomas(@Param("idioma") WpIdioma idioma);
}
