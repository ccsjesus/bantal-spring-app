package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.math.BigInteger;
import java.util.List;


/**
 * The persistent class for the wp_terms database table.
 * 
 */
@Entity
@Table(name="wp_terms")
public class WpTerm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="term_id")
	private String termId;

	private String name;

	private String slug;

	@Column(name="term_group")
	private BigInteger termGroup;

	//bi-directional many-to-one association to WpTermTaxonomy
	@OneToMany(mappedBy="wpTerm", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpTermTaxonomy> wpTermTaxonomies;

	public WpTerm() {
	}

	public String getTermId() {
		return this.termId;
	}

	public void setTermId(String termId) {
		this.termId = termId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return this.slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public BigInteger getTermGroup() {
		return this.termGroup;
	}

	public void setTermGroup(BigInteger termGroup) {
		this.termGroup = termGroup;
	}

	public List<WpTermTaxonomy> getWpTermTaxonomies() {
		return this.wpTermTaxonomies;
	}

	public void setWpTermTaxonomies(List<WpTermTaxonomy> wpTermTaxonomies) {
		this.wpTermTaxonomies = wpTermTaxonomies;
	}

	public WpTermTaxonomy addWpTermTaxonomy(WpTermTaxonomy wpTermTaxonomy) {
		getWpTermTaxonomies().add(wpTermTaxonomy);
		wpTermTaxonomy.setWpTerm(this);

		return wpTermTaxonomy;
	}

	public WpTermTaxonomy removeWpTermTaxonomy(WpTermTaxonomy wpTermTaxonomy) {
		getWpTermTaxonomies().remove(wpTermTaxonomy);
		wpTermTaxonomy.setWpTerm(null);

		return wpTermTaxonomy;
	}

}