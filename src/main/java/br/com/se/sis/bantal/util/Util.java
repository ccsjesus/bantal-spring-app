package br.com.se.sis.bantal.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import br.com.se.sis.bantal.enums.Role;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.se.sis.bantal.modelo.UserMeta;
import br.com.se.sis.bantal.modelo.WpPostmeta;
import br.com.se.sis.bantal.services.UsuarioService;

public class Util {

	
	    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
	    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
	    private static final String NUMBER = "0123456789";

	    private static final String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
	    private static SecureRandom random = new SecureRandom();
	    
	    @Autowired
	    private static UsuarioService usuarioService;

	    public static void main(String[] args) {

	        System.out.println("String : " + DATA_FOR_RANDOM_STRING);

	        for (int i = 0; i < 5; i++) {
	            System.out.println("result : " + generateRandomString(8));
	            System.out.println("\n");
	        }

	    }

	    public static String generateRandomString(int length) {
	        if (length < 1) throw new IllegalArgumentException();

	        StringBuilder sb = new StringBuilder(length);
	        for (int i = 0; i < length; i++) {

				// 0-62 (exclusive), random returns 0-61
	            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
	            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

	            // debug
	            System.out.format("%d\t:\t%c%n", rndCharAt, rndChar);

	            sb.append(rndChar);

	        }

	        return sb.toString();

	    }

	public static String obterNacionalidade(String sigla) {
		if(sigla != null && sigla.equals("BR")) {
			return "Brasileira";
		} else {
			return "";
		}
	}
	
	public static String removerAcentos(String str) {
	    return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
	
	public static List<String> montarListStringFromWordPress(String valor) {
		String r = "\\\"([^\\\\].*)\\\"";
		List<String> listaFAcademica =  new ArrayList<String>();
		if(valor != null) {
			final Pattern pat = Pattern.compile(r);
			Matcher mat = pat.matcher(valor);
			String str = "";
			if (mat.find()) {
				str = mat.group();
			}
			
			String[] valores = str.split(";}");
			List<String[]> listaFormacaoAcademica = new ArrayList<String[]>();
			for(String strV : valores) {
				listaFormacaoAcademica.add(strV.split("\""));				
			}
			
			for(String[] lista : listaFormacaoAcademica) {
				int i = 3;
				listaFAcademica.add(lista[i]);
				i+=4;
				
				do {
					listaFAcademica.add(lista[i]);
					i+=4;
				} while (lista.length > i);
				
			}		
			
		}
		return listaFAcademica;
	}
	
	public static Role getFuncaoSistema(UserMeta userMeta) {

		if(userMeta.getMetaValue().contains(FuncaoSistema.FUNCAO_CANDIDATO.toString())) {
			return Role.CANDIDATE;
		} else if(userMeta.getMetaValue().contains(FuncaoSistema.FUNCAO_EMPRESA.toString())) {
			return Role.EMPLOYER;
		} else if(userMeta.getMetaValue().contains(FuncaoSistema.FUNCAO_ADMINISTRATOR.toString())) {
			return Role.ADMIN;
		} if(userMeta.getMetaValue().contains(FuncaoSistema.FUNCAO_CUSTOMER.toString())) {
			return Role.CANDIDATE;
		}

		return Role.UNDEFINED;
	}
	
	public static boolean metaKeyExists(WpPostmeta meta, Constantes keyMeta) {
		return meta.getMetaKey().equals(keyMeta.toString()) ? true : false;
		
	}
	
	public static boolean metaKeyExists(Object meta, Constantes keyMeta) {
		return meta.toString().equals(keyMeta.toString()) ? true : false;
	}
	
	public static boolean metaKeyExists(Object meta, String keyMeta) {
		return meta.toString().equals(keyMeta.toString()) ? true : false;
	}
	
	public static boolean metaKeyExistsValid(Object meta, String keyMeta, Object value) {
		return meta.toString().equals(keyMeta.toString()) && (value != null && !"".equals(value.toString())) ? true : false;
	}
	
	public static String[] splitEspecial(String line) {
		String[] result = line.split("[" + Pattern.quote(IUtils.CURINGA) + "]");
		ArrayList<String> lista = new ArrayList<String>();
		for(String str : result) {
			if(!"".equals(str)) {
				lista.add(str);
			}
		}
		String[] newArr = new String[lista.size()];
		return lista.toArray(newArr);
	}
	
	public static byte[] decompressBytes(byte[] data) {
		
        Inflater inflater = new Inflater();

        inflater.setInput(data);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

        byte[] buffer = new byte[2048];

        try {

            while (!inflater.finished()) {

                int count = inflater.inflate(buffer);

                outputStream.write(buffer, 0, count);

            }

            outputStream.close();

        } catch (IOException ioe) {

        } catch (DataFormatException e) {

        }
        return outputStream.toByteArray();

    }
	
	public static byte[] compressBytes(byte[] data) {
			
	        Deflater deflater = new Deflater();
	
	        deflater.setInput(data);
	
	        deflater.finish();
	
	        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
	
	        byte[] buffer = new byte[2048];
	
	        while (!deflater.finished()) {
	
	            int count = deflater.deflate(buffer);
	
	            outputStream.write(buffer, 0, count);
	
	        }
		        try {
	
	            outputStream.close();
	
	        } catch (IOException e) {
	
	        }
	
	        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
	
	        return outputStream.toByteArray();
	
	    }


}
