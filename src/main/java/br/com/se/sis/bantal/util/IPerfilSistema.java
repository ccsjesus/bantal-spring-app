package br.com.se.sis.bantal.util;

public @interface IPerfilSistema {

	String FUNCAO_EMPRESA = "employer";
	String FUNCAO_CANDIDATO = "candidate";
	String FUNCAO_CUSTOMER = "customer";
	String FUNCAO_ADMINISTRATOR = "administrator";
	String FUNCAO_UNDEFINED = "indefinido";

}