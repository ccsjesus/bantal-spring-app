package br.com.se.sis.bantal.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpUser;

public interface AcessoRepository extends JpaRepository<WpUser, String>, JpaSpecificationExecutor<WpUser> {

	@Query("SELECT distinct user FROM WpUser user where user.userEmail = :email and user.userPass = :senha")
	WpUser consultarAcessoAutorizado(@Param("email") String email, @Param("senha") String senha);
	
	@Query("SELECT distinct user FROM WpUser user where user.id = :idUsuario")
	WpUser consultarUsuarioPorId(@Param("idUsuario") BigDecimal idUsuario);

	@Query(value="select DISTINCT * from wp_users join wp_usermeta on wp_users.ID = wp_usermeta.user_id where wp_usermeta.meta_key = 'billing_cnpj' and wp_usermeta.meta_value <> '' and wp_usermeta.user_id = :id ", 
			nativeQuery = true)
	List<WpUser> consultarDadosUsuarioPorId(@Param("id") String id);
	
	@Query(value="select DISTINCT * from wp_users join wp_usermeta on wp_users.ID = wp_usermeta.user_id where (wp_usermeta.meta_key = 'billing_cnpj' and wp_usermeta.meta_value <> '' or wp_usermeta.meta_key = 'billing_cpf' and wp_usermeta.meta_value <> '') and wp_usermeta.user_id = :id ", 
			nativeQuery = true)
	WpUser consultarDadosUsuario(@Param("id") String id);
	
}
