package br.com.se.sis.bantal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.Profissao;
import br.com.se.sis.bantal.modelo.ProfissaoDTO;

public interface ProfissaoRepository extends JpaRepository<Profissao, String>, JpaSpecificationExecutor<Profissao> {

	@Query(value = "SELECT * FROM wp_profissao;", nativeQuery = true)
	List<Profissao> recuperarProfissoes();
	
	@Query(value = "SELECT * FROM wp_profissao where id_profissao = :idProfissao", nativeQuery = true)
	Profissao recuperarProfissaoPorId(@Param("idProfissao") Long idProfissao);
	
	@Query(value = " SELECT new br.com.se.sis.bantal.modelo.ProfissaoDTO(p.idProfissao,p.nome, p.idAreaAtuacao, p.habilitada, count(e.id)) "
			+ " FROM Profissao p "
			+ " join WpExperienciaProfissional e on p.idProfissao = e.idProfissao "
			+ " where p.idAreaAtuacao = :idAreaAtuacao group by  p.idProfissao,p.nome, p.idAreaAtuacao, p.habilitada")
	List<ProfissaoDTO> recuperarProfissoesPorIdAreaAtuacao(@Param("idAreaAtuacao") Long idAreaAtuacao);

}
