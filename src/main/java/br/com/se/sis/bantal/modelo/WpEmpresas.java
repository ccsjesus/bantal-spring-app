package br.com.se.sis.bantal.modelo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_empresas")
@Builder(setterPrefix = "with")
public class WpEmpresas implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="nome")
    private String nome;

    @Column(name="objetivo")
    private String objetivo;

    @Column(name="sigla")
    private String sigla;

    @Column(name="user_id")
    private String userId;


}
