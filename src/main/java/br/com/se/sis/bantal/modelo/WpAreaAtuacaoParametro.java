package br.com.se.sis.bantal.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the wp_users database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="wp_area_atuacao_parametro")
public class WpAreaAtuacaoParametro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_area_atuacao")
	private String id;

	@Column(name="nome")
	private String nome;

	@Column(name="valor")
	private String valor;
	
	@Column(name="habilitada")
	private int habilitada;
	
	@OneToMany(mappedBy="wpAreaAtuacaoParametro", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<WpAreaAtuacao> listAreaAtuacao;
	
	@Transient
	@OneToOne(mappedBy = "wpAreaAtuacaoParametro", cascade = CascadeType.ALL,fetch = FetchType.LAZY, optional = false)
	private WpVagas wpVagasAreaAtuacaoParametro;
	

	public WpAreaAtuacaoParametro(String nome, String valor, int habilitada) {
		super();
		this.nome = nome;
		this.valor = valor;
		this.habilitada = habilitada;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof WpAreaAtuacaoParametro))
			return false;
		return id != null && id.equals(((WpAreaAtuacaoParametro) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
	
	
	public static List<AreaAtuacaoModel> fromToModel(List<WpAreaAtuacaoParametro> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new AreaAtuacaoModel(wpFormacao.getId(), wpFormacao.getNome(), wpFormacao.getValor(), wpFormacao.getHabilitada()))
		        .collect(Collectors.toList());
	}
	
	public static List<WpAreaAtuacaoParametro> fromToWpEntity(List<AreaAtuacaoModel> wpFor) {
		return wpFor.stream()
		        .map(wpFormacao -> new WpAreaAtuacaoParametro(wpFormacao.getTermId(), wpFormacao.getName(), wpFormacao.getHabilitada()))
		        .collect(Collectors.toList());
	}

	

}