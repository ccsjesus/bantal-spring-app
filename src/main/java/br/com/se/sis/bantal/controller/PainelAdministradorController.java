package br.com.se.sis.bantal.controller;

import br.com.se.sis.bantal.annotations.UserAdminCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.repository.TokenRepository;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.PainelAdministradorServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/v1/painel-admin")
public class PainelAdministradorController {

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private PainelAdministradorServices painelAdmin;

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserAdminCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getPainelAdmin(Authentication authentication)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(painelAdmin.obterDadosPainelAdministrador(customDetails.getId()));
			
	}
	
	

}
