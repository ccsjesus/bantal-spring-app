package br.com.se.sis.bantal.services;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.exceptions.AuthenticationException;
import br.com.se.sis.bantal.exceptions.TokenExpiredException;
import br.com.se.sis.bantal.modelo.RefreshToken;
import br.com.se.sis.bantal.modelo.WpUserAuthenticationEntity;
import br.com.se.sis.bantal.repository.RefreshTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
public class RefreshTokenService {

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private UserDetailCustomService userRepository;

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    public RefreshToken createRefreshToken(String userId) throws AuthenticationException {
        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setUser(userRepository.findById(userId).get().getCustomer());
        refreshToken.setExpiryDate(Instant.now().plusMillis(300000));
        refreshToken.setToken(UUID.randomUUID().toString());

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public RefreshToken verifyExpiration(RefreshToken token) throws TokenExpiredException {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new TokenExpiredException(ErrorsMessages.TOKEN_EXPIRED.getDescription(), ErrorsMessages.TOKEN_EXPIRED);
        }

        return token;
    }

    @Transactional
    public int deleteByUserId(Long userId) throws AuthenticationException {
        return refreshTokenRepository.deleteByUser(userRepository.findByIdentifier(userId.toString()).get());
    }
}