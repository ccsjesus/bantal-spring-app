package br.com.se.sis.bantal.controller;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.se.sis.bantal.annotations.UserCandidateCanOnlyAccessTheirOwnResource;
import br.com.se.sis.bantal.modelo.InscricaoVagaDTO;
import br.com.se.sis.bantal.security.UserCustomDetails;
import br.com.se.sis.bantal.services.VagasService;

@RestController
@RequestMapping("/vagas-candidato")
public class VagasCandidatoController {

	@Autowired
	private VagasService vagasGerenciador;

	@Transactional
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> getVagasCadidato(Authentication authentication)
			throws IllegalArgumentException {
		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());

		return ResponseEntity.ok(vagasGerenciador.obterVagasInscritasCandidato(customDetails.getId()));
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "/realizar-inscricao-vaga")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> realizarIncricaoVaga(Authentication authentication, @RequestBody InscricaoVagaDTO inscricaoVaga)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		inscricaoVaga.setCdUsuario(new BigDecimal(customDetails.getId()));
		vagasGerenciador.realizarNovaInscricaoVaga(inscricaoVaga,customDetails.getId());
		return ResponseEntity.ok(null);
	}
	
	@Transactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/{cdVaga}/remover-inscricao-vaga")
	@UserCandidateCanOnlyAccessTheirOwnResource
	public ResponseEntity<Object> removerIncricaoVaga(Authentication authentication, @PathVariable(value = "cdVaga") String cdVaga)
			throws IllegalArgumentException {

		UserCustomDetails customDetails = ((UserCustomDetails)authentication.getPrincipal());
		
		vagasGerenciador.removerInscricaoVaga(cdVaga, customDetails.getId());
		return ResponseEntity.ok(null);
	}
	
	
}
