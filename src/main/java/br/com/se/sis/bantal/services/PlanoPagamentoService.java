package br.com.se.sis.bantal.services;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.se.sis.bantal.dto.WpPlanoPagamentoDTO;
import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.enums.Role;
import br.com.se.sis.bantal.exceptions.PlanNotFoundException;
import br.com.se.sis.bantal.modelo.ExpiracaoPlano;
import br.com.se.sis.bantal.modelo.Usuario;
import br.com.se.sis.bantal.modelo.WpPerfilUsuario;
import br.com.se.sis.bantal.modelo.WpPlanoPagamento;
import br.com.se.sis.bantal.modelo.WpPlanos;
import br.com.se.sis.bantal.repository.PlanoPagamentoRepository;
import br.com.se.sis.bantal.repository.PlanosRepository;
import br.com.se.sis.bantal.util.IPlanos;

@Service
public class PlanoPagamentoService {
	
	@Autowired
	private PlanosRepository planosRepository;
	
	@Autowired
	private PlanoPagamentoRepository planoPagamentoRepository;
	
	@Autowired
	private DateService dataService;
	
	@Autowired
	private PerfilServices perfilServices;
	
	@Autowired
	private ControleVagaAvulsaService controleVagaAvulsaService;
	
	public WpPlanos obterPlanoPorCodigo(BigInteger cdPlano) {
		return planosRepository.obterPlanoPorCodigo(cdPlano);
	}
	
	public boolean isPlanoUsuarioValido(String idUsuarioLogado) {
		boolean usuarioCandidato = perfilServices.isUsuarioLogadoCandidato(idUsuarioLogado);
		
		if (usuarioCandidato)
			return false;
		
		WpPlanoPagamento planoAtivoUsuario = planoPagamentoRepository.obterPlanoAtivoPorIdUsuario(idUsuarioLogado);
		
		if (planoAtivoUsuario == null ) {
			throw new PlanNotFoundException(ErrorsMessages.PLAN_NOT_FOUND.getDescription(),ErrorsMessages.PLAN_NOT_FOUND);
		}
		
		WpPlanos planoId = this.obterPlanoPorCodigo(new BigInteger(planoAtivoUsuario.getWpPlano().getCdPlano()));
		
		if (controleVagaAvulsaService.isEmpresaComPlanoAvulsoPermitePublicarVaga(planoAtivoUsuario, true)) {
			return true;
		}
		
		LocalDateTime localConfirmacao = planoAtivoUsuario.getDataConfimacaoPagamento();
		
		if (localConfirmacao == null || localConfirmacao.plusMonths(planoId.getDuracao()).isAfter(dataService.now())) {
			return false;
		} else {
			return true;
		}
	}
	
	public ExpiracaoPlano obterPeriodoVingenciaPlano(String idUsuarioLogado) {
		WpPlanoPagamento planoAtivoUsuario = planoPagamentoRepository.obterPlanoAtivoPorIdUsuario(idUsuarioLogado);
		ExpiracaoPlano expiracaoPlano = new ExpiracaoPlano();
		
		if (planoAtivoUsuario == null || planoAtivoUsuario.getDataConfimacaoPagamento() == null) {
			
			expiracaoPlano.setDiasExpirar(0);
			expiracaoPlano.setExibirAlertaExpiracao(true);
		} if (controleVagaAvulsaService.isTipoPlanoAvulso(planoAtivoUsuario.getWpPlano())) {
			
			boolean isValidarPlanoAvulso = controleVagaAvulsaService.isEmpresaComPlanoAvulsoPermitePublicarVaga(planoAtivoUsuario, false);
			
			//TODO verificar qual o prazo para planos avulsos - 180
			LocalDateTime localConfirmacao = planoAtivoUsuario.getDataConfimacaoPagamento();
			LocalDateTime locallocalDatePrevisao = localConfirmacao.plusMonths(180);

			expiracaoPlano.setDataPlano(dataService.formatLocalDateTimeToString(localConfirmacao));
			expiracaoPlano.setExpiracaoPlano(dataService.formatLocalDateTimeToString(locallocalDatePrevisao));
			
			long diferencaEmDias = ChronoUnit.DAYS.between(dataService.now(), locallocalDatePrevisao);
			
			if (diferencaEmDias <= 180 || !isValidarPlanoAvulso) {
				expiracaoPlano.setExibirAlertaExpiracao(true);
			}			
			
			expiracaoPlano.setDiasExpirar(diferencaEmDias);
			
			if (!isValidarPlanoAvulso)
				expiracaoPlano.setDiasExpirar(-1);
		} else {
			
			WpPlanos planoId = this.obterPlanoPorCodigo(new BigInteger(planoAtivoUsuario.getWpPlano().getCdPlano()));
			
			LocalDateTime localConfirmacao = planoAtivoUsuario.getDataConfimacaoPagamento();
			LocalDateTime locallocalDatePrevisao = localConfirmacao.plusMonths(planoId.getDuracao());
			
			expiracaoPlano.setDataPlano(dataService.formatLocalDateTimeToString(localConfirmacao));
			expiracaoPlano.setExpiracaoPlano(dataService.formatLocalDateTimeToString(locallocalDatePrevisao));
			
			long diferencaEmDias = ChronoUnit.DAYS.between(dataService.now(), locallocalDatePrevisao);
			
			if (diferencaEmDias <= planoId.getTempoAlerta()) {
				expiracaoPlano.setExibirAlertaExpiracao(true);
			}
			
			expiracaoPlano.setDiasExpirar(diferencaEmDias);
		}
		 
		if (perfilServices.isUsuarioLogadoCandidato(idUsuarioLogado)) {
			expiracaoPlano.setExibirAlertaExpiracao(false);
		}
		
		return expiracaoPlano;

	}
	
	
	
	@Transactional
	public List<WpPlanoPagamento> registrarPlanoInicialPagamento(String idUsuarioLogado, Role role) {
		WpPerfilUsuario profileUser = perfilServices.obterWpPerfilUsuario(idUsuarioLogado);
		WpPlanoPagamento wpPlanoPagamento;
		WpPlanos plans;

		switch (role) {
			case EMPLOYER:
				plans  =  planosRepository.getPlanBySlugAndProfile(IPlanos.INICIAL, role.name());
				wpPlanoPagamento = WpPlanoPagamento.builder()
						.withCdSolicitacaoTransacao(IPlanos.INICIAL)
						.withCdTransacao(IPlanos.INICIAL)
						.withCdUsuario(idUsuarioLogado)
						.withDataSolicitacaoPagamento(dataService.now())
						.withDataConfimacaoPagamento(dataService.now())
						.withWpPlano(plans)
						.withWpPerfilUsuario(profileUser).build();

			break;
			case CANDIDATE:
			case ADMIN:
				plans  =  planosRepository.getPlanBySlugAndProfile(IPlanos.UNLIMITED, role.name());
				wpPlanoPagamento = WpPlanoPagamento.builder()
						.withCdSolicitacaoTransacao(IPlanos.UNLIMITED)
						.withCdTransacao(IPlanos.UNLIMITED)
						.withCdUsuario(idUsuarioLogado)
						.withDataSolicitacaoPagamento(dataService.now())
						.withDataConfimacaoPagamento(dataService.now())
						.withWpPlano(plans)
						.withWpPerfilUsuario(profileUser).build();
				break;
		default:
			return Arrays.asList(new WpPlanoPagamento());
		}

		planoPagamentoRepository.saveAndFlush(wpPlanoPagamento);
		return Arrays.asList(wpPlanoPagamento);
	}
	
	public void registrarSolicitacaoPagamento(WpPlanoPagamentoDTO solicitacaoPlanoPagamento) {
		WpPlanoPagamento wpPlanoPagamento = new WpPlanoPagamento();
		wpPlanoPagamento.setCdSolicitacaoTransacao(solicitacaoPlanoPagamento.getCdSolicitacaoTransacao());
		wpPlanoPagamento.setCdUsuario(solicitacaoPlanoPagamento.getCdUsuario());
		wpPlanoPagamento.setDataSolicitacaoPagamento(dataService.now());
		WpPlanos wpPlano = obterPlanoPorCodigo(BigInteger.valueOf(Integer.parseInt(solicitacaoPlanoPagamento.getCdPlano())));
		wpPlano.setCdPlano(BigInteger.valueOf(Integer.parseInt(solicitacaoPlanoPagamento.getCdPlano())).toString());
		wpPlanoPagamento.setWpPlano(wpPlano);
		
		planoPagamentoRepository.save(wpPlanoPagamento);
	}
	
	public void registrarTransacaoPagamento(WpPlanoPagamentoDTO registroPlanoPagamento, String idUser) {
		WpPlanoPagamento wpPlanoPagamento = obterPlanoPagamentoPorCodigoSolicitacao(registroPlanoPagamento.getCdSolicitacaoTransacao(), new BigInteger(idUser));
		
		wpPlanoPagamento.setCdTransacao(registroPlanoPagamento.getCdTransacao());
		wpPlanoPagamento.setDataConfimacaoPagamento(dataService.now());
		wpPlanoPagamento.setStatusPagamento(registroPlanoPagamento.getStatusPagamento());
		
		planoPagamentoRepository.save(wpPlanoPagamento);
		
		boolean isPlanoAvulso = controleVagaAvulsaService.isTipoPlanoAvulso(wpPlanoPagamento.getWpPlano());
		
		if (isPlanoAvulso) {
			controleVagaAvulsaService.registrarInicioControleVagaAvulsa(wpPlanoPagamento);
		}
	
	}
	
	public WpPlanoPagamento obterPlanoPagamentoPorCodigoSolicitacao(String cdSolicitacaoPagamento, BigInteger idUsuario) {
		return planoPagamentoRepository.obterPlanoPagamentoPorCodigoSolicitacao(cdSolicitacaoPagamento, idUsuario);
	}
	
	public WpPlanoPagamento obterPlanoPagamentoPorIdUsuario(String idUsuarioLogado) {
		return planoPagamentoRepository.obterPlanoAtivoPorIdUsuario(idUsuarioLogado);
		
	}
	
	public WpPlanoPagamentoDTO convertFromWpPlanoPagamentoDTO(WpPlanoPagamento planoPagamento) {
		return new WpPlanoPagamentoDTO(planoPagamento.getWpPlano().getCdPlano(), 
										planoPagamento.getCdUsuario(),
										planoPagamento.getCdTransacao(), 
										planoPagamento.getCdSolicitacaoTransacao(),
										planoPagamento.getDataConfimacaoPagamento(), 
										planoPagamento.getDataSolicitacaoPagamento(),
										planoPagamento.getStatusPagamento());
	}
	
	public List<WpPlanoPagamentoDTO> convertFromListWpPlanoPagamentoDTO(List<WpPlanoPagamento> listaPlanoPagamento) {
		List<WpPlanoPagamentoDTO> listaDTO = new ArrayList<>();
		if(listaPlanoPagamento != null) {
			listaDTO.addAll(listaPlanoPagamento.stream().map(planoPagamentoDTO -> this.convertFromWpPlanoPagamentoDTO(planoPagamentoDTO)).collect(Collectors.toList()));
		}
		return listaDTO;
	}

	public Optional<WpPlanoPagamento> findPlanPaymentByIdProfile(String idProfile) {
		WpPerfilUsuario wpPerfilUsuario = WpPerfilUsuario.builder().withId(idProfile).build();
		return planoPagamentoRepository.findByWpPerfilUsuario(wpPerfilUsuario);
	}

	public WpPlanoPagamento updateCdUserPlanPaymentByProfileUser(WpPerfilUsuario profileUser, String userId) {
		WpPlanoPagamento wpPlanoPagamento = findPlanPaymentByIdProfile(profileUser.getId()).orElse(null);
		if(wpPlanoPagamento == null ) {
			wpPlanoPagamento = registrarPlanoInicialPagamento(userId, Role.get(profileUser.getPerfil())).get(0);
		}
		return getWpPlanoPagamento(userId, wpPlanoPagamento);
	}


	public WpPlanoPagamento updateCdUserPlanPaymentByProfileUser(Usuario user, String userId) {
		WpPlanoPagamento wpPlanoPagamento = obterPlanoPagamentoPorIdUsuario(user.getId().toString());
		if( wpPlanoPagamento == null ){
			wpPlanoPagamento = obterPlanoPagamentoPorIdUsuario(userId);
		}
		return getWpPlanoPagamento(userId, wpPlanoPagamento);
	}

	private WpPlanoPagamento getWpPlanoPagamento(String userId, WpPlanoPagamento wpPlanoPagamento) {
		if (!wpPlanoPagamento.getCdUsuario().equals(userId)) {
			wpPlanoPagamento.setCdUsuario(userId);
			return planoPagamentoRepository.save(wpPlanoPagamento);
		}
		return wpPlanoPagamento;
	}
}
