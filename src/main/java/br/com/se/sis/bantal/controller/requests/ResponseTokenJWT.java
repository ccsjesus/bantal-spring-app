package br.com.se.sis.bantal.controller.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class ResponseTokenJWT {

    @JsonProperty(value = "attributes")
    private Map<String, Object> attributes;

}
