package br.com.se.sis.bantal.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpPlanos;

public interface PlanosRepository extends JpaRepository<WpPlanos, String>, JpaSpecificationExecutor<WpPlanos> {

	
	@Query(value = "select * from wp_planos where wp_planos.cd_plano = :cdPlano",
			nativeQuery = true)
	WpPlanos obterPlanoPorCodigo(@Param("cdPlano") BigInteger cdPlano);
	
	@Query(value = "select * from wp_planos where wp_planos.slug = :slug and perfil= :profile",
			nativeQuery = true)
	WpPlanos getPlanBySlugAndProfile(@Param("slug") String slug, @Param("profile") String profile);
	
	@Query(value = "select * from wp_planos where wp_planos.perfil = :perfil and wp_planos.slug != 'inicial' order by sequencial_apresentacao",
			nativeQuery = true)
	List<WpPlanos> obterPlanoPorPerfil(@Param("perfil") String perfil);
	
}
