package br.com.se.sis.bantal.modelo;

import java.util.List;

public class PostsParent {
	
	private List<Posts> posts;
	
	private int found_posts;
	
	private int max_num_pages;

	public List<Posts> getPosts() {
		return posts;
	}

	public void setPosts(List<Posts> posts) {
		this.posts = posts;
	}

	public int getFound_posts() {
		return found_posts;
	}

	public void setFound_posts(int found_posts) {
		this.found_posts = found_posts;
	}

	public int getMax_num_pages() {
		return max_num_pages;
	}

	public void setMax_num_pages(int max_num_pages) {
		this.max_num_pages = max_num_pages;
	}
	

}
