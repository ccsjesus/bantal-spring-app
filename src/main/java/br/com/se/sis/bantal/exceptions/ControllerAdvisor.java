package br.com.se.sis.bantal.exceptions;

import br.com.se.sis.bantal.enums.ErrorsMessages;
import br.com.se.sis.bantal.responses.ErrorResponse;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.UnexpectedTypeException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

        @ExceptionHandler(TokenExpiredException.class)
        public ResponseEntity<ErrorResponse> handleNotFoundException(WebRequest request, TokenExpiredException exception) {
            ErrorResponse error = ErrorResponse.builder().withHttpCode(HttpStatus.FORBIDDEN.value())
                    .withMessage(exception.getMessage())
                    .withInternalCode(exception.getCode().name())
                    .withErros(null).build();
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(error);
        }

        @ExceptionHandler(AuthenticationException.class)
        public ResponseEntity<ErrorResponse> responseNotFoundException(WebRequest request, AuthenticationException authentication) {
            ErrorResponse error = ErrorResponse.builder().withHttpCode(HttpStatus.NOT_FOUND.value())
                    .withMessage(authentication.getMessage())
                    .withInternalCode(HttpStatus.NOT_FOUND.toString())
                    .withErros(null).build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
        }

        @ExceptionHandler(UnexpectedTypeException.class)
        public ResponseEntity<ErrorResponse> responseUnexpectedTypeException(WebRequest request, UnexpectedTypeException exception) {
            ErrorResponse error = ErrorResponse.builder().withHttpCode(HttpStatus.BAD_REQUEST.value())
                    .withMessage(exception.getMessage())
                    .withInternalCode(HttpStatus.BAD_REQUEST.toString())
                    .withErros(null).build();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
        }

        @ExceptionHandler(NullPointerException.class)
        public ResponseEntity<ErrorResponse> responseNullPointerException(WebRequest request, NullPointerException exception) {
            ErrorResponse error = ErrorResponse.builder().withHttpCode(HttpStatus.BAD_REQUEST.value())
                    .withMessage(HttpStatus.BAD_REQUEST.name())
                    .withInternalCode(HttpStatus.BAD_REQUEST.toString())
                    .withErros(null).build();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
        }

        @ExceptionHandler(NotFoundException.class)
        public ResponseEntity<ErrorResponse> responseException(WebRequest request, NotFoundException exception) {
            ErrorResponse error = ErrorResponse.builder().withHttpCode(HttpStatus.NOT_FOUND.value())
                    .withMessage(exception.getMessage())
                    .withInternalCode(exception.getCode().name())
                    .withErros(null).build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
        }

        @ExceptionHandler(DataAccessResourceFailureException.class)
        public ResponseEntity<ErrorResponse> responseDataAccessException(WebRequest request, DataAccessResourceFailureException exception) {
            ErrorResponse error = ErrorResponse.builder().withHttpCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .withMessage(exception.getMessage())
                    .withInternalCode(ErrorsMessages.CONNECTION_DATABASE_ERROR.name())
                    .withErros(null).build();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
        }
    }