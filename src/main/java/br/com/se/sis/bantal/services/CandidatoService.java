package br.com.se.sis.bantal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import br.com.se.sis.bantal.modelo.WpPerfilUsuario;

@Service
@Configurable
public class CandidatoService {
	
	@Autowired
	private PerfilServices profileService;
	

	public WpPerfilUsuario obterDadosUsuario(String idUsuario) {
		return profileService.obterWpPerfilUsuario(idUsuario) != null ? profileService.obterWpPerfilUsuario(idUsuario) : null;
	}
		
}
