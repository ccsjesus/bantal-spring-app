package br.com.se.sis.bantal.util;

public enum FuncaoSistema {	
	
	FUNCAO_EMPRESA("employer"),
	FUNCAO_CANDIDATO("candidate"),
	FUNCAO_CUSTOMER("customer"),
	FUNCAO_ADMINISTRATOR("administrator"),
	FUNCAO_UNDEFINED("indefinido");
	
	private String valor;
	
	FuncaoSistema(String valor) {
		this.valor = valor;
	}
	
	public String getValor() {
		return valor;
	}
	
	@Override
	public String toString() {
		return this.valor;
	}
	
	public static FuncaoSistema valueOfLabel(String label) {
	    for (FuncaoSistema e : values()) {
	        if (e.getValor().equals(label)) {
	            return e;
	        }
	    }
	    return null;
	}

}

