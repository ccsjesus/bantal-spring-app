package br.com.se.sis.bantal.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.se.sis.bantal.modelo.WpCurriculos;

public interface CurriculoRepositoryV2 extends JpaRepository<WpCurriculos, String>, JpaSpecificationExecutor<WpCurriculos> {


	@Query(value=" SELECT * FROM wp_curriculos c WHERE c.user_id = :idCandidato", nativeQuery = true)
	WpCurriculos obterCurriculoCandidato(@Param("idCandidato") String idCandidato);
	
	@Query(value=" SELECT * FROM wp_curriculos c WHERE c.user_id in :listIdCandidato", nativeQuery = true)
	List<WpCurriculos> obterCurriculoCandidato(@Param("listIdCandidato") List<String> listIdCandidato);
	
	@Query(value=" select * from wp_curriculos c \r\n"
			+ "join wp_experiencia_profissional ep on c.id = ep.id_curriculo\r\n"
			+ "where ep.id_profissao = :idProfissao ", nativeQuery = true)
	List<WpCurriculos> obterCurriculoPorProfissao(@Param("idProfissao") String idProfissao);

    Optional<WpCurriculos> findByUserId(String userId);
}
